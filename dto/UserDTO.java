package com.base245.apeevent.dto;

import com.base245.apeevent.service.util.enum_types.UserTypes;

/**
 * @author Dilini Peiris
 */
public class UserDTO extends SuperDTO {
    private String name;
    private String email;

    private UserTypes userType;

    private int id;
    private String district;

    private String occupation;
    private String organization;
    private String contactNo;
    private String profileImage;
    private String image;

    public UserDTO() {
    }

    public UserDTO(String name, String email, UserTypes userType, int id, String district, String profileImage) {
        this.name = name;
        this.email = email;
        this.userType = userType;
        this.id = id;
        this.district = district;
        this.profileImage = profileImage;
    }

//    public UserDTO(String name, String email, UserTypes userType, int id, String district) {
//        this.name = name;
//        this.email = email;
//        this.userType = userType;
//        this.id = id;
//        this.district = district;
//    }

    public UserDTO(String name, String email, UserTypes userType, int id, String occupation, String organization, String contactNo, String profileImage) {
        this.name = name;
        this.email = email;
        this.userType = userType;
        this.id = id;
        this.occupation = occupation;
        this.organization = organization;
        this.contactNo = contactNo;
        this.profileImage = profileImage;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public UserTypes getUserType() {
        return userType;
    }

    public void setUserType(UserTypes userType) {
        this.userType = userType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
