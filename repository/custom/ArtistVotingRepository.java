package com.base245.apeevent.repository.custom;

import com.base245.apeevent.entity.ArtistVoting;
import com.base245.apeevent.entity.ArtistVotingID;
import com.base245.apeevent.repository.CrudRepository;


public interface ArtistVotingRepository extends CrudRepository<ArtistVoting, ArtistVotingID> {

    ArtistVoting getArtistVotingByID(int eventID,int artistID,String username);

}
