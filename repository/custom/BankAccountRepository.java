package com.base245.apeevent.repository.custom;

import com.base245.apeevent.entity.BankAccount;
import com.base245.apeevent.repository.CrudRepository;

/**
 * @author Dilini Peiris on 3/29/2019
 */


public interface BankAccountRepository extends CrudRepository<BankAccount,String> {
}
