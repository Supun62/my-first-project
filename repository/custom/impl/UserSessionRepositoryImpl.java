package com.base245.apeevent.repository.custom.impl;

import com.base245.apeevent.entity.UserSession;
import com.base245.apeevent.repository.CrudRepositoryImpl;
import com.base245.apeevent.repository.custom.UserSessionRepository;
import com.base245.apeevent.repository.session.SessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public class UserSessionRepositoryImpl extends CrudRepositoryImpl<UserSession, Integer> implements UserSessionRepository {

    @Autowired
    private SessionManager sessionManager;

    @Override
    public boolean updateLogOutTime(String username) {
        boolean state = false;
        try {
            int s = sessionManager.getSession().createNativeQuery("update user_session set LOG_OUT_TIME='" + LocalDateTime.now().toString() + "' where USERNAME='" + username + "';").executeUpdate();
            if (s > 0) state = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return state;
    }

    @Override
    public boolean updateLogInTime(String username) {
        boolean state = false;
        try {
            int s = sessionManager.getSession().createNativeQuery("update user_session set LOG_IN_TIME='" + LocalDateTime.now().toString() + "' , LOG_OUT_TIME='" + "LOGGED" + "' where USERNAME='" + username + "';").executeUpdate();
            if (s > 0) state = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return state;
    }
}
