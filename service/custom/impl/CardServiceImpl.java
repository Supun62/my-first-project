package com.base245.apeevent.service.custom.impl;

import com.base245.apeevent.dto.CardDTO;
import com.base245.apeevent.entity.Card;
import com.base245.apeevent.repository.custom.CardRepository;
import com.base245.apeevent.service.custom.CardService;
import com.base245.apeevent.service.util.convertor.EntityConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Dilini Peiris on 3/29/2019
 */
@Service
public class CardServiceImpl implements CardService {

    @Autowired
    EntityConvertor entityConvertor;

    @Autowired
    CardRepository cardRepository;
    @Override
    public boolean save(CardDTO dto) {
        Card card = (Card) entityConvertor.convertToEntity(dto);
        return cardRepository.save(card);
    }

    @Override
    public boolean update(CardDTO dto) {
        return false;
    }

    @Override
    public boolean delete(String key) {
        return false;
    }

    @Override
    public CardDTO findByID(String key) {
        return null;
    }

    @Override
    public CardDTO findByName(String name) {
        return null;
    }

    @Override
    public List<CardDTO> findAll() {
        return null;
    }
}
