package com.base245.apeevent.controller;

import com.base245.apeevent.dto.ArtistDTO;
import com.base245.apeevent.service.custom.ArtistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("api/v1/artist")
public class ArtistController {

    @Autowired
    private ArtistService artistService;
// return all artist details
    @GetMapping("/getAllArtists")
    public List<ArtistDTO> getAllArtists(){
        return artistService.findAll();
    }

    @PostMapping("/add")
    public boolean addArtist(@RequestBody ArtistDTO artistDTO) {
        return artistService.save(artistDTO);
    }

}
