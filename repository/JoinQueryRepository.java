package com.base245.apeevent.repository;

import com.base245.apeevent.entity.*;
import com.base245.apeevent.repository.custom.EventRepository;
import com.base245.apeevent.repository.custom.LoginRepository;
import com.base245.apeevent.repository.session.SessionManager;
import com.base245.apeevent.service.util.enum_types.UserTypes;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dilini Peiris on 6/7/2019
 */
@Repository
public class JoinQueryRepository {

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    LoginRepository loginRepository;

    @Autowired
    EventRepository eventRepository;

    public String getEmailFromUsername(String username) {
        UserTypes userType = getUserTypeFromUsername(username);
        String email = null;
        switch (userType) {
            case FAN: {
                Query query = sessionManager.getSession().createSQLQuery("select email from Login l, Fan where fk_fan=fid AND username='" + username + "';");
                email = (String) query.list().get(0);
                break;
            }
            case EVENT_ORGANIZER: {
                Query query = sessionManager.getSession().createSQLQuery("select email from Login l, EVENT_ORGANIZER where fk_eventOrganizer=id AND username='" + username + "';");
                email = (String) query.list().get(0);
                break;
            }
            case FACILITATOR: {
                Query query = sessionManager.getSession().createSQLQuery("select email from Login l, FACILITATOR where fk_facilitator=id AND username='" + username + "';");
                email = (String) query.list().get(0);
            }
        }

        return email;
    }

    public UserTypes getUserTypeFromUsername(String username) {
        UserTypes type = null;
        Login byID = loginRepository.findByID(username);
        if (null != byID.getEventOrganizer())
            return UserTypes.EVENT_ORGANIZER;
        else if (null != byID.getFan())
            return UserTypes.FAN;
        else if (null != byID.getFacilitator())
            return UserTypes.FACILITATOR;
        return type;
    }

    public TicketPackage findPackageByEventAndName(String eventName, String packageName) {
        Query query = sessionManager.getSession().createSQLQuery("select id from event where name='" + eventName + "';");
        Integer id = (Integer) (query.list().get(0));
        System.out.println(id);

        NativeQuery<TicketPackage> nativeQuery = sessionManager.getSession().createNativeQuery("select * from ticket_package where fk_event=" + id +
                " AND name='" + packageName + "';", TicketPackage.class);
        List<TicketPackage> data = nativeQuery.getResultList();

        System.out.println(data);
        if (null != data) {
            return data.get(0);
        }
        return null;
    }

    public List<String> findEventByOrganizer(String username) {
        Query q = sessionManager.getSession().createSQLQuery("select fk_eventOrganizer from login where username='" + username + "'");
        Integer eventOrganizer = (Integer) (q.list().get(0));
        Query query = sessionManager.getSession().createSQLQuery("select name from event where eventOrganizer='" + eventOrganizer + "';");
        List<String> names = query.getResultList();
        return names;
    }

    public List<String> findFanEmailsByEvent(String eventName) {
        Query query = sessionManager.getSession().createSQLQuery("select id from event where name='" + eventName + "';");
        Integer id = (Integer) (query.list().get(0));

        Query q = sessionManager.getSession().createSQLQuery("select email from fan f inner join (select fan_id from ticket t " +
                "inner join (select pack_id from ticket_package where fk_event='" + id + "') a on t.TICKET_PACKAGE_ID=a.pack_id) x on f.fid=x.fan_id");
//recheck the query with optimization
        List<String> emails = q.getResultList();

        return emails;

    }

    public String getConfirmedLocation(int eventID) {
        NativeQuery nativeQuery = sessionManager.getSession().createNativeQuery("select ");
        int data = (int) nativeQuery.getSingleResult();
        return null;
    }

    public List<LocationVoteCount> getLocationVoting(int eventID) {
//        Query q = sessionManager.getSession().createNativeQuery(
//                "SELECT l.name, count(v.vote) from LocationVoting v,Location l where v.eventID=" + eventID + " and l.ID=v.locationID group by v.locationID",
//                "LocationVoteResults");
//        return q.list();

        NativeQuery sqlQuery = sessionManager.getSession().createSQLQuery("SELECT l.name, count(v.vote) from location_voting v,location l where v.eventID=" + eventID + " and l.ID=v.locationID group by v.locationID");
        List<Object[]> rows = sqlQuery.list();
        if (rows.size() == 0)
            return null;
        List<LocationVoteCount> list = new ArrayList<>();
        for (Object[] row : rows) {
            LocationVoteCount count = new LocationVoteCount(row[0].toString(), Integer.parseInt(row[1].toString()));
            System.out.println(count);
            list.add(count);
        }

        return list;
    }

    public List<ArtistVoteCount> getArtistVoting(int eventID) {
//        Query q = sessionManager.getSession().createNativeQuery(
//                "SELECT a.name, count(v.vote) from ArtistVoting v,Artist a where v.eventID=" + eventID + " and a.ID=v.artistID group by v.artistID",
//                "ArtistVoteResults");
//        return q.list();

        NativeQuery sqlQuery = sessionManager.getSession().createSQLQuery("SELECT a.name, count(v.vote) from artist_voting v,artist a where v.eventID=" + eventID + " and a.ID=v.artistID group by v.artistID");
        List<Object[]> rows = sqlQuery.list();
        if (rows.size() == 0)
            return null;
        List<ArtistVoteCount> list = new ArrayList<>();
        for (Object[] row : rows) {
            ArtistVoteCount count = new ArtistVoteCount(row[0].toString(), Integer.parseInt(row[1].toString()));
            System.out.println(count);
            list.add(count);
        }

        return list;
    }

    public List<TicketPurchaseCount> getTicketPurchaseCount(int eventID) {
//        Query q = sessionManager.getSession().createNativeQuery(
//                "SELECT l.name, count(v.vote) from Ticket v,TicketPackage l where v.eventID=" + eventID + " and l.PACK_ID=v.TICKET_PACKAGE_ID group by v.TICKET_PACKAGE_ID",
//                "TicketPurchaseResults");
//        return q.list();

        NativeQuery sqlQuery = sessionManager.getSession().createSQLQuery("SELECT l.name, count(v.TICKET_NO) from ticket v,ticket_package l where l.fk_event=" + eventID + " and l.PACK_ID=v.TICKET_PACKAGE_ID group by v.TICKET_PACKAGE_ID");
        List<Object[]> rows = sqlQuery.list();
        if (rows.size() == 0)
            return null;
        List<TicketPurchaseCount> list = new ArrayList<>();
        for (Object[] row : rows) {
            TicketPurchaseCount count = new TicketPurchaseCount(row[0].toString(), Integer.parseInt(row[1].toString()));
            System.out.println(count);
            list.add(count);
        }

        return list;
    }

}
