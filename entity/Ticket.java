package com.base245.apeevent.entity;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * @author Amesh M Jayaweera as 1/26/2019
 * edited by Dilini Peiris
 */
@Entity
@Table(name = "TICKET",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"TICKET_NO"})}
)
@AssociationOverrides({
        @AssociationOverride(name = "primaryKey.fan",
                joinColumns = @JoinColumn(name = "FAN_ID")),
        @AssociationOverride(name = "primaryKey.ticketPackage",
                joinColumns = @JoinColumn(name = "TICKET_PACKAGE_ID"))
})
public class Ticket extends SuperEntity {

    @EmbeddedId
    private TicketID primaryKey;

    @Column(name = "TICKET_NO", nullable = false)
    private String ticketNo;

    @Column(name = "PURCHASE_DATE", nullable = false)
    private LocalDate purchaseDate;


    public Ticket(String ticketNo, LocalDate purchaseDate) {
        this.ticketNo = ticketNo;
        this.purchaseDate = purchaseDate;
    }

    public Ticket() {
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public LocalDate getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(LocalDate purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public TicketID getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(TicketID primaryKey) {
        this.primaryKey = primaryKey;
    }
}
