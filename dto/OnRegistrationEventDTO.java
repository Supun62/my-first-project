package com.base245.apeevent.dto;

import org.springframework.context.ApplicationEvent;

/**
 * @author Hkp Maheshika
 */
public class OnRegistrationEventDTO extends ApplicationEvent {

    private String appUrl;
    private UserRegisterDTO userRegisterDTO;

    public OnRegistrationEventDTO(String appUrl, UserRegisterDTO userRegisterDTO) {
        super(new Object());
        this.appUrl = appUrl;
        this.userRegisterDTO = userRegisterDTO;
    }

    public OnRegistrationEventDTO(Object source, String appUrl, UserRegisterDTO userRegisterDTO) {
        super(source);
        this.appUrl = appUrl;
        this.userRegisterDTO = userRegisterDTO;
    }

    public String getAppUrl() {
        return appUrl;
    }

    public void setAppUrl(String appUrl) {
        this.appUrl = appUrl;
    }

    public UserRegisterDTO getUserRegisterDTO() {
        return userRegisterDTO;
    }

    public void setUserRegisterDTO(UserRegisterDTO userRegisterDTO) {
        this.userRegisterDTO = userRegisterDTO;
    }
}
