package com.base245.apeevent.service.custom;


import com.base245.apeevent.dto.CustomDTO;
import com.base245.apeevent.dto.EventDTO;
import com.base245.apeevent.dto.TicketPackageDTO;
import com.base245.apeevent.service.SuperService;

import java.util.List;

/**
 * @author Supun Rangana as 2/18/2019
 */
public interface EventService extends SuperService<EventDTO, Integer> {
    boolean compareEventBudget(int id);

    int updatePostVote(int vote, int eventID);

    int updatePostArtistVote(int vote, int eventID, int artistID);

    int updatePostLocationVote(int vote, int eventID, int locationID);

    List<EventDTO> getEventsByEventOrganizer(int eventOrganizerID);

    List<EventDTO> getOnGoingEventsByEventOrganizer(int eventOrganizerID);

    List<EventDTO> getPreviousEventsByEventOrganizer(int eventOrganizerID);

    EventDTO getEventByEventID(int eventID);

    boolean removeTicketPackage(int ticketPackageID);

    boolean removeArtist(int eventID, int artistID);

    boolean updateTicketPackage(TicketPackageDTO ticketPackageDTO);

    EventDTO addNewTicketPackageToEvent(CustomDTO customDTO);

    EventDTO addNewArtistToEvent(CustomDTO customDTO);

    boolean updateEvent(EventDTO eventDTO);

    boolean removeLocation(int eventID, int locationID);

    EventDTO addNewLocation(CustomDTO customDTO);

    boolean confirmArtistForEvent(int eventID, int artistID, String status);

    boolean updateEventName(int eventID, String name, String eventOrganizer);

    boolean updateEventPreOrderClosingDate(int eventID, String preOrderClosingDate);

    boolean updateEventTargetBudget(int eventID, double targetBudget);

    boolean confirmLocationForEvent(int eventID, int locationID);

    boolean cancelTheEvent(int eventID);

    boolean publishTheEvent(int eventID, String eventDueDate, int locationID);

    double getCollectedRevenue(int eventID);

    long getCancelledEventCount();

    long getPendingEventCount();

    long getInProgressEventCount();

    List<EventDTO> getPendingEvents();

    List<EventDTO> getInProgressEvents();

}
