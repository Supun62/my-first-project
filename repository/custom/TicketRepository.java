package com.base245.apeevent.repository.custom;

import com.base245.apeevent.entity.Ticket;
import com.base245.apeevent.entity.TicketID;
import com.base245.apeevent.repository.CrudRepository;

/**
 * @author Dilini Peiris on 6/11/2019
 */
public interface TicketRepository extends CrudRepository<Ticket, TicketID> {
}
