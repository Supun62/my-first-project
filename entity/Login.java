package com.base245.apeevent.entity;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author Amesh M Jayaweera as 1/26/2019
 * edited by Dilini Peiris
 */
@Entity
@Table(name = "LOGIN")
@DynamicUpdate
public class Login extends SuperEntity {

    @Id
    @Column(name = "USERNAME", nullable = false, updatable = false)
    private String username;
    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "STATUS", nullable = false)
    @Type(type = "yes_no")
    private boolean status;

    @Column(name = "CREATED_DATE", nullable = false)
    private LocalDateTime createdDateTime;

    @OneToOne
    @JoinColumn(name = "fk_facilitator")
    private Facilitator facilitator;

    @OneToOne
    @JoinColumn(name = "fk_eventOrganizer")
    private EventOrganizer eventOrganizer;

    @OneToOne
    @JoinColumn(name = "fk_fan")
    private Fan fan;

    public Login(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public Login() {
    }

    public Login(String username, String password, boolean status) {
        this.username = username;
        this.password = password;
        this.status = status;
    }

    public LocalDateTime getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(LocalDateTime createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Fan getFan() {
        return fan;
    }

    public void setFan(Fan fan) {
        this.fan = fan;
    }

    public EventOrganizer getEventOrganizer() {
        return eventOrganizer;
    }

    public void setEventOrganizer(EventOrganizer eventOrganizer) {
        this.eventOrganizer = eventOrganizer;
    }

    public Facilitator getFacilitator() {
        return facilitator;
    }

    public void setFacilitator(Facilitator facilitator) {
        this.facilitator = facilitator;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Login{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", status=" + status +
                ", facilitator=" + facilitator +
                ", eventOrganizer=" + eventOrganizer +
                ", fan=" + fan +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        System.out.println("----------------- executing equals of login");
        Login login = null;
        if (obj instanceof Login)
            login = (Login) obj;
        boolean status = false;
        status = username.equals(login.getUsername()) && password.equals(login.getPassword()) && (this.status == login.isStatus());
        return status;
    }
}
