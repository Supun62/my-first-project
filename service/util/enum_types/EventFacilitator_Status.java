package com.base245.apeevent.service.util.enum_types;

/**
 * @author Dilini Peiris on 6/18/2019
 */
public enum EventFacilitator_Status {
    PENDING, CONFIRMED
}
