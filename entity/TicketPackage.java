package com.base245.apeevent.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 *   @author Amesh M Jayaweera as 1/26/2019
 */
@Entity
@Table(name = "TICKET_PACKAGE",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"NAME"})}
)
public class TicketPackage extends SuperEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PACK_ID",nullable = false)
    private int packId;
    @Column(name = "PRICE",nullable = false)
    private double price;
    @Column(name = "NAME",nullable = false)
    private String name;
    @Column(name = "DISCOUNT")
    private double discount;
    @Column(name = "DESCRIPTION")
    private String description;

    @ManyToOne
    @JoinColumn(name = "fk_event")
    private Event event;

    @OneToMany(mappedBy = "primaryKey.ticketPackage")
    private List<Ticket> ticket = new ArrayList<>();

    public List<Ticket> getTicket() {
        return ticket;
    }

    public void setTicket(List<Ticket> ticket) {
        this.ticket = ticket;
    }

    public TicketPackage(int packId, double price, String name, double discount, String description) {
        this.packId = packId;
        this.price = price;
        this.name = name;
        this.discount = discount;
        this.description = description;
    }

    public TicketPackage() {
    }

    public int getPackId() {
        return packId;
    }

    public void setPackId(int packId) {
        this.packId = packId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
}
