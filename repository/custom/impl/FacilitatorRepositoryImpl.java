package com.base245.apeevent.repository.custom.impl;

import com.base245.apeevent.entity.Facilitator;
import com.base245.apeevent.repository.CrudRepositoryImpl;
import com.base245.apeevent.repository.custom.FacilitatorRepository;
import com.base245.apeevent.repository.session.SessionManager;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dilini Peiris
 */
@Repository
public class FacilitatorRepositoryImpl extends CrudRepositoryImpl<Facilitator,Integer> implements FacilitatorRepository {
    @Autowired
    private SessionManager sessionManager;

    @Override
    public int findIdByEmail(String email) {
        NativeQuery nativeQuery = sessionManager.getSession().createNativeQuery("select id from Facilitator where email='" + email + "';");
        int data = (int) nativeQuery.getSingleResult();
        return data;
    }

    @Override
    public Facilitator findUserByEmail(String email) {
        NativeQuery<Facilitator> nativeQuery = sessionManager.getSession().createNativeQuery("select * from Facilitator where EMAIL='" + email + "';", Facilitator.class);
        List<Facilitator> data = new ArrayList<>();
        data = nativeQuery.getResultList();
        if ((null != data) && !data.isEmpty()) {
            return data.get(0);
        }
        return null;
    }

}
