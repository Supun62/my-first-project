package com.base245.apeevent.dto;

public class VoteCountDTO extends SuperDTO {
    String name;
    int count;

    public VoteCountDTO() {
    }

    public VoteCountDTO(String name, int count) {
        this.name = name;
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
