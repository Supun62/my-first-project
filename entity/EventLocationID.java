package com.base245.apeevent.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class EventLocationID implements Serializable {

    @Column(name = "EVENT_LOCATION_ID", nullable = false)
    private long eventLocationID;

    @ManyToOne
    private Event event;
    @ManyToOne
    private Location location;

    public EventLocationID() {
    }

    public EventLocationID(Event event, Location location) {
        this.event = event;
        this.location = location;
    }

    public EventLocationID(long eventLocationID, Event event, Location location) {
        this.eventLocationID = eventLocationID;
        this.event = event;
        this.location = location;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
