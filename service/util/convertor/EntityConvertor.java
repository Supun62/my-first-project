package com.base245.apeevent.service.util.convertor;

import com.base245.apeevent.dto.*;
import com.base245.apeevent.entity.*;
import com.base245.apeevent.repository.JoinQueryRepository;
import com.base245.apeevent.repository.custom.FanRepository;
import com.base245.apeevent.repository.custom.TicketRepository;
import com.base245.apeevent.service.util.enum_types.UserTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dilini Peiris
 * edited by Supun Rangana
 */
@Component
public class EntityConvertor {

    @Autowired
    FanRepository fanRepository;

    @Autowired
    JoinQueryRepository joinQueryRepository;

    @Autowired
    TicketRepository ticketRepository;

    public SuperEntity convertToEntity(SuperDTO dto) {

        if (dto instanceof LoginDTO) {
            LoginDTO login = (LoginDTO) dto;
            return new Login(login.getUsername(), login.getPassword());
        } else if (dto instanceof UserDTO) {
            UserDTO userDTO = (UserDTO) dto;
            switch (userDTO.getUserType()) {
                case FAN: {
                    return new Fan(userDTO.getId(), userDTO.getName(), userDTO.getDistrict(), userDTO.getEmail(), userDTO.getProfileImage());
                }
                case EVENT_ORGANIZER: {
                    return new EventOrganizer(userDTO.getId(), userDTO.getName(), userDTO.getOccupation(), userDTO.getOrganization(), userDTO.getEmail(), userDTO.getContactNo(), userDTO.getProfileImage());
                }
                case FACILITATOR: {
                    return new Facilitator(userDTO.getId(), userDTO.getName(), userDTO.getOccupation(), userDTO.getOrganization(), userDTO.getEmail(), userDTO.getContactNo(), userDTO.getProfileImage());
                }
            }
        } else if (dto instanceof ArtistDTO) {
            ArtistDTO artistDTO = (ArtistDTO) dto;
            return new Artist(artistDTO.getId(), artistDTO.getName());
        } else if (dto instanceof CardDTO) {
            CardDTO cardDTO = (CardDTO) dto;
            return new Card(cardDTO.getCardNo(), cardDTO.getCardType(), cardDTO.getNameOnCard(), cardDTO.getExpiry(), cardDTO.getCvv());
        } else if (dto instanceof BankAccountDTO) {
            BankAccountDTO bankAccountDTO = (BankAccountDTO) dto;
            return new BankAccount(bankAccountDTO.getAccNo(), bankAccountDTO.getBankName(), bankAccountDTO.getBranch(), bankAccountDTO.getSwiftCode());
        } else if (dto instanceof EventDTO) {
            EventDTO eventDTO = (EventDTO) dto;
            return new Event(eventDTO.getId(), eventDTO.getName(), eventDTO.getStatus().toString(), eventDTO.getPreOrderClosingDate(), eventDTO.getCreatedDateTime(), eventDTO.getTargetBudget(), eventDTO.getNoOfVotes(), eventDTO.getEventDueDate(), eventDTO.getPublishedDateTime());
        } else if (dto instanceof LocationDTO) {
            LocationDTO locationDTO = (LocationDTO) dto;
            return new Location(locationDTO.getId(), locationDTO.getName(), locationDTO.getLatitude(), locationDTO.getLongitude());
        } else if (dto instanceof TicketDTO) {
            TicketDTO ticketDTO = (TicketDTO) dto;
            Ticket ticket = new Ticket(ticketDTO.getTicketNo(), ticketDTO.getPurchaseDate());
            Fan fan = fanRepository.findByID(ticketDTO.getFanId());
            TicketPackage ticketPackage = joinQueryRepository.findPackageByEventAndName(ticketDTO.getEventName(), ticketDTO.getPackageName());

            long count = ticketRepository.getCount();
            ticket.setPrimaryKey(new TicketID(++count, fan, ticketPackage));

            return ticket;
        } else if (dto instanceof TicketPackageDTO) {
            TicketPackageDTO ticketPackageDTO = (TicketPackageDTO) dto;
            return new TicketPackage(ticketPackageDTO.getPackId(), ticketPackageDTO.getPrice(), ticketPackageDTO.getName(), ticketPackageDTO.getDiscount(), ticketPackageDTO.getDescription());
        } else if (dto instanceof TransactionDTO) {
            TransactionDTO transactionDTO = (TransactionDTO) dto;
            return new Transaction(transactionDTO.getTransId(), transactionDTO.getFrom_amount(), transactionDTO.getFrom(), transactionDTO.getTo_amount(), transactionDTO.getTo(), transactionDTO.getDate());
        }
//        }else if(dto instanceof VoteDTO ){
//            VoteDTO voteDTO=(VoteDTO)dto;
//            return new Vote(voteDTO.getFanId(),voteDTO.getVoteeId(),voteDTO.getVoteDate());
//        }
//           dilini - create vote entity

        return null;

    }

    public List convertToEntityList(List<SuperDTO> dtos) {

        if (dtos.get(0) instanceof UserDTO) {
            UserTypes userType = ((UserDTO) dtos.get(0)).getUserType();
            List<Fan> fans = new ArrayList<>();
            List<EventOrganizer> eventOrganizers = new ArrayList<>();
            List<Facilitator> facilitators = new ArrayList<>();

            for (Object e : dtos) {
                UserEntity user = (UserEntity) convertToEntity((SuperDTO) e);
                if (user instanceof Fan) {
                    fans.add((Fan) user);
                } else if (user instanceof EventOrganizer) {
                    eventOrganizers.add((EventOrganizer) user);
                } else if (user instanceof Facilitator) {
                    facilitators.add((Facilitator) user);
                }
            }

            switch (userType) {
                case FACILITATOR:
                    return facilitators;
                case EVENT_ORGANIZER:
                    return eventOrganizers;
                case FAN:
                    return fans;
            }

        } else if (dtos instanceof LoginDTO) {
            List<Login> ll = new ArrayList<>();
            for (Object e : dtos) {
                Login f = (Login) convertToEntity((SuperDTO) e);
                ll.add(f);
            }
            return ll;
        }
        return null;

    }
}
