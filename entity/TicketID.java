package com.base245.apeevent.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 *   @author Amesh M Jayaweera as 1/27/2019
 */

@Embeddable
public class TicketID implements Serializable {

    @Column(name = "TICKET_ID", nullable = false)
    private long ticketID;

    @ManyToOne
    private Fan fan;

    @ManyToOne
    private TicketPackage ticketPackage;

    public TicketID(long ticketID, Fan fan, TicketPackage ticketPackage) {
        this.ticketID = ticketID;
        this.fan = fan;
        this.ticketPackage = ticketPackage;
    }

    public TicketID() {
    }

    public Fan getFan() {
        return fan;
    }

    public void setFan(Fan fan) {
        this.fan = fan;
    }

    public TicketPackage getTicketPackage() {
        return ticketPackage;
    }

    public void setTicketPackage(TicketPackage ticketPackage) {
        this.ticketPackage = ticketPackage;
    }

    public long getTicketID() {
        return ticketID;
    }

    public void setTicketID(long ticketID) {
        this.ticketID = ticketID;
    }
}
