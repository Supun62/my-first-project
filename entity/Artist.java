package com.base245.apeevent.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Amesh M Jayaweera as 1/26/2019
 */
@Entity
@Table(name = "ARTIST")
public class Artist extends SuperEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private int id;
    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "IMAGE")
    private String image;

    @Column(name = "STATUS")
    private String status;

    @OneToMany(mappedBy = "primaryKey.artist")
    private List<FanArtist> fanArtist = new ArrayList<>();

    @OneToMany(mappedBy = "primaryKey.artist")
    private List<EventArtist> eventArtists = new ArrayList<>();

    public List<FanArtist> getFanArtist() {
        return fanArtist;
    }

    public void setFanArtist(List<FanArtist> fanArtist) {
        this.fanArtist = fanArtist;
    }

    public List<EventArtist> getEventArtists() {
        return eventArtists;
    }

    public void setEventArtists(List<EventArtist> eventArtists) {
        this.eventArtists = eventArtists;
    }

    public Artist(int id, String name, String occupation, String organization, String email, String contactNo) {
    }

    public Artist(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Artist(int id, String name, String image) {
        this.id = id;
        this.name = name;
        this.image = image;
    }

    public Artist(int id, String name, String image, String status) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.status = status;
    }

    public Artist() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
