package com.base245.apeevent.repository.custom.impl;

import com.base245.apeevent.entity.TicketPackage;
import com.base245.apeevent.repository.CrudRepositoryImpl;
import com.base245.apeevent.repository.custom.TicketPackageRepository;
import com.base245.apeevent.repository.session.SessionManager;
import com.base245.apeevent.service.util.convertor.DtoConvertor;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Supun Rangana as 2/24/2019
 */

@Repository
public class TicketPackageRepositoryImpl extends CrudRepositoryImpl<TicketPackage,Integer> implements TicketPackageRepository {

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    private DtoConvertor dtoConvertor;

    @Override
    public List<TicketPackage> getTicketPackageList(int eventID) {

        NativeQuery<TicketPackage> nativeQuery = sessionManager.getSession().createNativeQuery("select * from ticket_package where fk_event='" + eventID +
                "' ;", TicketPackage.class);
        List<TicketPackage> data = nativeQuery.getResultList();

        return data;
    }

    @Override
    public boolean removeTicketPackage(int ticketPackageID) {

        boolean state = false;
        try {
            int s = sessionManager.getSession().createNativeQuery("DELETE FROM ticket_package WHERE PACK_ID='" + ticketPackageID + "';").executeUpdate();
            if (s > 0) state = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return state;
    }
}
