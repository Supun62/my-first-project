package com.base245.apeevent.service.custom.impl;

import com.base245.apeevent.dto.TicketDTO;
import com.base245.apeevent.dto.VoteCountDTO;
import com.base245.apeevent.entity.Ticket;
import com.base245.apeevent.entity.TicketPurchaseCount;
import com.base245.apeevent.repository.JoinQueryRepository;
import com.base245.apeevent.repository.custom.TicketRepository;
import com.base245.apeevent.service.custom.TicketService;
import com.base245.apeevent.service.util.convertor.DtoConvertor;
import com.base245.apeevent.service.util.convertor.EntityConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Dilini Peiris on 6/11/2019
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TicketServiceImpl implements TicketService {

    @Autowired
    TicketRepository ticketRepository;

    @Autowired
    EntityConvertor entityConvertor;

    @Autowired
    JoinQueryRepository joinQueryRepository;

    @Autowired
    DtoConvertor dtoConvertor;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public boolean save(TicketDTO dto) {
        Ticket ticket = (Ticket) entityConvertor.convertToEntity(dto);
        return ticketRepository.save(ticket);
    }

    @Override
    public boolean update(TicketDTO dto) {
        return false;
    }

    @Override
    public boolean delete(String key) {
        return false;
    }

    @Override
    public TicketDTO findByID(String key) {
        return null;
    }

    @Override
    public TicketDTO findByName(String name) {
        return null;
    }

    @Override
    public List<TicketDTO> findAll() {
        return null;
    }

    @Override
    public long getCount() {
        return 0;
    }


    @Override
    public boolean saveAllTickets(List<TicketDTO> ticketDTOS) {
        return false;
    }

    @Override
    public List<VoteCountDTO> getTicketPurchases(int eventID) {
        List<TicketPurchaseCount> ticketPurchaseCount = joinQueryRepository.getTicketPurchaseCount(eventID);
        System.out.println(ticketPurchaseCount);
        List<VoteCountDTO> list = dtoConvertor.convertToDTOList(ticketPurchaseCount);
        return list;
    }
}
