package com.base245.apeevent.controller;

import com.base245.apeevent.dto.ImageDTO;
import com.base245.apeevent.service.util.ImageConverter;
import com.base245.apeevent.service.util.ImagePath;
import com.base245.apeevent.service.util.enum_types.ImageType;
import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/v1/file")
public class FileController {

    @Autowired
    ImagePath imagePath;

    @GetMapping("/test")
    public String print() {
        System.out.println("==============================test================================");
        return "Testing----------------";
    }

    @GetMapping("getPath")
    public String getPath() {
        return imagePath.getBaseFolderPath();
    }


    @PostMapping("/images/upload/list")
    public boolean filesUpload(@RequestBody List<ImageDTO> imageDTOList) {
        boolean status = true;
        for (ImageDTO imageDTO : imageDTOList) {
            status = status & fileUpload(imageDTO);
        }
        return status;
    }


    @PostMapping("images/upload")
    public boolean fileUpload(@RequestBody ImageDTO imageDTO) {
        System.out.println("Called ========================= " + imageDTO.getImageType());
        System.out.println("image file size: " + imageDTO.getFile().length());
        System.out.println(imageDTO.getFileName());

        byte[] bytes = new byte[0];
        Path path = null;

        if (imageDTO.getFile() != null) {
            String s = imageDTO.getFile();
            try {
                bytes = Base64.decode(s);
            } catch (Base64DecodingException e) {
                e.printStackTrace();
                System.out.println("ERROR : " + e.getMessage());
            }
        }
        System.out.println("image dto:" + imageDTO);
        String directory = getDirectoryFromType(imageDTO.getImageType());
        path = Paths.get(directory + imageDTO.getFileName());
        try {
            Files.write(path, bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }


    @PostMapping("images/get")
    public ImageDTO getImage(@RequestBody ImageDTO imageDTO) {

        ImageType type = imageDTO.getImageType();
        String directory = getDirectoryFromType(type);

        BufferedImage bImage = null;
        try {
            File file = new File(directory + imageDTO.getFileName());
            System.out.println(file.getPath());
            bImage = ImageIO.read(file);
            imageDTO.setFile(ImageConverter.convertBufferedImageToString(bImage));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("ERROR ======== " + e.getMessage());
        }
        return imageDTO;
    }

    @GetMapping("/updateFileName")
    public boolean renameFile(@RequestParam("name") String name, @RequestParam("updateName") String updateName, @RequestParam("type") String type) {

        ImageType imageType = ImageType.valueOf(type);
        String directory = getDirectoryFromType(imageType);

        File file = new File(directory + name);
        File updateFile = new File(directory + updateName);
        if (file.exists()) {
            file.renameTo(updateFile);
            return true;
        }
        return false;
    }

    private String getDirectoryFromType(ImageType type) {
        String directory = "";
        switch (type) {
            case EVENT: {
                directory = imagePath.getEventImagePath();
                break;
            }
            case ARTIST: {
                directory = imagePath.getArtistImagePath();
                break;
            }
            case PROFILE: {
                directory = imagePath.getProfileImagePath();
                break;
            }
            case LOCATION: {
                directory = imagePath.getLocationImagePath();
                break;
            }
        }
        return directory;
    }
}
