package com.base245.apeevent.dto;

import java.time.LocalDate;

/**
 *  @author Supun Rangana as 1/23/2019
 *
 */
public class TicketDTO extends SuperDTO {
    private String eventName;
    private String packageName;
    private int fanId;
    private String ticketNo;
    private LocalDate purchaseDate;

    public TicketDTO() {
    }

    public TicketDTO(String ticketNo, LocalDate purchaseDate) {
        this.ticketNo = ticketNo;
        this.purchaseDate = purchaseDate;
    }

    public TicketDTO(String eventName, String packageName, int fanId, LocalDate purchaseDate) {
        this.eventName = eventName;
        this.packageName = packageName;
        this.fanId = fanId;
        this.purchaseDate = purchaseDate;
    }

    public TicketDTO(String eventName, String packageName, int fanId, String ticketNo, LocalDate purchaseDate) {
        this.eventName = eventName;
        this.packageName = packageName;
        this.fanId = fanId;
        this.ticketNo = ticketNo;
        this.purchaseDate = purchaseDate;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public int getFanId() {
        return fanId;
    }

    public void setFanId(int fanId) {
        this.fanId = fanId;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public LocalDate getPurchaseDate() {
        return purchaseDate;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public void setPurchaseDate(LocalDate purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    @Override
    public String toString() {
        return eventName + "_" + packageName + "_" + fanId + "_" + ticketNo + "_" + purchaseDate;
    }
}
