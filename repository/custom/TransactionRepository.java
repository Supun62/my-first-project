package com.base245.apeevent.repository.custom;

import com.base245.apeevent.entity.Transaction;
import com.base245.apeevent.repository.CrudRepository;

import java.util.List;

/**
 * @author Dilini Peiris on 6/11/2019
 */


public interface TransactionRepository extends CrudRepository<Transaction, Integer> {

    double getAccumulatedBudget(String eventName);

    double getAccumulatedBudget(int eventId);

    List<Transaction> getTransactionsByEvent(int eventId);
}
