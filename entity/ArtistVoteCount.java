package com.base245.apeevent.entity;

import javax.persistence.EntityResult;
import javax.persistence.SqlResultSetMapping;

@SqlResultSetMapping(name = "ArtistVoteResults",
        entities = {
                @EntityResult(entityClass = ArtistVoting.class),
                @EntityResult(entityClass = Artist.class)
        })
public class ArtistVoteCount extends SuperEntity {
    String artistName;
    int voteCount;

    public ArtistVoteCount(String artistName, int voteCount) {
        this.artistName = artistName;
        this.voteCount = voteCount;
    }

    public ArtistVoteCount() {
    }

    public String getArtistName() {
        return artistName;
    }

    public int getVoteCount() {
        return voteCount;
    }

}
