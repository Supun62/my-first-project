package com.base245.apeevent.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ArtistVotingID implements Serializable {

    @Column(name = "ARTIST_VOTING_ID")
    private long artistVotingID;

    private int eventID;

    private int artistID;

    private String username;

    public ArtistVotingID() {
    }

    public ArtistVotingID(long artistVotingID, int eventID, int artistID, String username) {
        this.artistVotingID = artistVotingID;
        this.eventID = eventID;
        this.artistID = artistID;
        this.username = username;
    }


    public long getArtistVotingID() {
        return artistVotingID;
    }

    public void setArtistVotingID(long artistVotingID) {
        this.artistVotingID = artistVotingID;
    }

    public int getEventID() {
        return eventID;
    }

    public void setEventID(int eventID) {
        this.eventID = eventID;
    }

    public int getArtistID() {
        return artistID;
    }

    public void setArtistID(int artistID) {
        this.artistID = artistID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ArtistVotingID)) return false;
        ArtistVotingID that = (ArtistVotingID) o;
        return getArtistVotingID() == that.getArtistVotingID() &&
                getEventID() == that.getEventID() &&
                getArtistID() == that.getArtistID() &&
                Objects.equals(getUsername(), that.getUsername());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getArtistVotingID(), getEventID(), getArtistID(), getUsername());
    }
}


