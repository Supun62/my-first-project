package com.base245.apeevent.entity;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 *   @author Amesh M Jayaweera as 1/26/2019
 *   edited by Supun Rangana
 */
@Entity
@Table(name = "EVENT")
@DynamicUpdate
public class Event extends SuperEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID",nullable = false)
    private int id;
    @Column(name = "NAME",nullable = false)
    private String name;
    @Column(name = "STATUS",nullable = false)
    private String status;

    @Column(name = "PRE_ORDER_CLOSING_DATE",nullable = false)
    private String preOrderClosingDate;

    @Column(name = "CREATED_DATE", nullable = false)
    private String createdDateTime;
    @Column(name = "TARGET_BUDGET",nullable = false)
    private double target_budget;
    @Column(name = "IMAGE")
    private String image;

    @Column(name = "NO_OF_VOTES")
    private int noOfVotes;

    @Column(name = "EVENT_DUE_DATE")
    private String eventDueDate;

    @Column(name = "PUBLISHED_DATE")
    private String publishedDateTime;

    @ManyToOne
    @JoinColumn(name = "eventOrganizer")
    private EventOrganizer eventOrganizer;

    @OneToMany(mappedBy = "event")
    private List<Transaction> transaction = new ArrayList<>();

    @OneToMany(mappedBy = "primaryKey.event")
    private List<EventArtist> eventArtists = new ArrayList<>();

    @OneToMany(mappedBy = "primaryKey.event")
    private List<EventLocation> eventLocations = new ArrayList<>();

    public Event() {
    }

    public Event(int id, String name, String status, String preOrderClosingDate, String createdDateTime, double target_budget, int noOfVotes, String eventDueDate, String publishedDateTime) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.preOrderClosingDate = preOrderClosingDate;
        this.createdDateTime = createdDateTime;
        this.target_budget = target_budget;
        this.noOfVotes = noOfVotes;
        this.eventDueDate = eventDueDate;
        this.publishedDateTime = publishedDateTime;
    }

    public Event(String name, String status, String preOrderClosingDate, String createdDateTime, double target_budget, String image, int noOfVotes, String eventDueDate, String publishedDateTime) {
        this.name = name;
        this.status = status;
        this.preOrderClosingDate = preOrderClosingDate;
        this.createdDateTime = createdDateTime;
        this.target_budget = target_budget;
        this.image = image;
        this.noOfVotes = noOfVotes;
        this.eventDueDate = eventDueDate;
        this.publishedDateTime = publishedDateTime;
    }

    public List<EventArtist> getEventArtists() {
        return eventArtists;
    }

    public void setEventArtists(List<EventArtist> eventArtists) {
        this.eventArtists = eventArtists;
    }

    public List<EventLocation> getEventLocations() {
        return eventLocations;
    }

    public void setEventLocations(List<EventLocation> eventLocations) {
        this.eventLocations = eventLocations;
    }

    public List<Transaction> getTransaction() {
        return transaction;
    }

    public void setTransaction(List<Transaction> transaction) {
        this.transaction = transaction;
    }

    public EventOrganizer getEventOrganizer() {
        return eventOrganizer;
    }

    public void setEventOrganizer(EventOrganizer eventOrganizer) {
        this.eventOrganizer = eventOrganizer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNoOfVotes() {
        return noOfVotes;
    }

    public void setNoOfVotes(int noOfVotes) {
        this.noOfVotes = noOfVotes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPreOrderClosingDate() {
        return preOrderClosingDate;
    }

    public void setPreOrderClosingDate(String preOrderClosingDate) {
        this.preOrderClosingDate = preOrderClosingDate;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public double getTarget_budget() {
        return target_budget;
    }

    public void setTarget_budget(double target_budget) {
        this.target_budget = target_budget;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEventDueDate() {
        return eventDueDate;
    }

    public void setEventDueDate(String eventDueDate) {
        this.eventDueDate = eventDueDate;
    }

    public String getPublishedDateTime() {
        return publishedDateTime;
    }

    public void setPublishedDateTime(String publishedDateTime) {
        this.publishedDateTime = publishedDateTime;
    }
}
