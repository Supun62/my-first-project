package com.base245.apeevent.service.custom;

import com.base245.apeevent.dto.TicketPackageDTO;
import com.base245.apeevent.service.SuperService;

import java.util.List;

/**
 * @author Dilini Peiris on 6/9/2019
 */
public interface TicketPackageService extends SuperService<TicketPackageDTO, Integer> {

    List<TicketPackageDTO> getTicketPackagesByEvent(String eventName, String organizerName);

    List<TicketPackageDTO> getTicketPackagesByEvent(int eventID);

    @Override
    default List<TicketPackageDTO> findAll() {
        return null;
    }
}
