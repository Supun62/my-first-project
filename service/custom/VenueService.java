package com.base245.apeevent.service.custom;

import com.base245.apeevent.dto.LocationDTO;
import com.base245.apeevent.service.SuperService;

import java.util.List;

/**
 * @author Dilini Peiris on 6/11/2019
 */
public interface VenueService extends SuperService<LocationDTO, Integer> {
    List<LocationDTO> getVenueList(int eventID);
}
