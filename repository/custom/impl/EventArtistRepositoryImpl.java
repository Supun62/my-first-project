package com.base245.apeevent.repository.custom.impl;

import com.base245.apeevent.entity.EventArtist;
import com.base245.apeevent.entity.EventArtistID;
import com.base245.apeevent.repository.CrudRepositoryImpl;
import com.base245.apeevent.repository.custom.EventArtistRepository;
import com.base245.apeevent.repository.session.SessionManager;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Dilini Peiris on 6/11/2019
 */
@Repository
public class EventArtistRepositoryImpl extends CrudRepositoryImpl<EventArtist, EventArtistID> implements EventArtistRepository {

    @Autowired
    private SessionManager sessionManager;

    @Override
    public List<EventArtist> getEventArtist(int eventID) {
        NativeQuery<EventArtist> nativeQuery = sessionManager.getSession().createNativeQuery("select * from event_artist where EVENT_ID='" + eventID +
                "';", EventArtist.class);

        List<EventArtist> data = nativeQuery.getResultList();
        return data;
    }

    @Override
    public int updateVotes(int vote,int eventID,int artistID) {


        try {
            sessionManager.getSession().createNativeQuery("update event_artist set NO_OF_VOTES = NO_OF_VOTES + '" + vote + "' where EVENT_ID='" + eventID + "' AND ARTIST_ID='" +artistID+ "' ;").executeUpdate();

        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        NativeQuery nativeQuery = sessionManager.getSession().createNativeQuery("select NO_OF_VOTES from event_artist where EVENT_ID='" + eventID + "' AND ARTIST_ID='" +artistID+ "' ;");
        int data = (int) nativeQuery.getSingleResult();
        return data;
    }

    @Override
    public int getArtistVoteCount(int eventID, int artistID) {
        NativeQuery nativeQuery = sessionManager.getSession().createNativeQuery("select NO_OF_VOTES from event_artist where EVENT_ID='" + eventID + "' AND ARTIST_ID='" +artistID+ "' ;");
        int data = (int) nativeQuery.getSingleResult();
        return data;
    }

    @Override
    public boolean removeArtistFromEvent(int eventID, int artistID) {
        boolean state = false;
        try {
            int s = sessionManager.getSession().createNativeQuery("DELETE FROM event_artist WHERE EVENT_ID='" + eventID + "' AND ARTIST_ID='" + artistID + "';").executeUpdate();
            if (s > 0) state = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return state;
    }

    @Override
    public boolean confirmArtistForEvent(int eventID, int artistID, String status) {
        boolean state = false;
        try {
            int s = sessionManager.getSession().createNativeQuery("update event_artist SET STATUS='" + status + "' WHERE  EVENT_ID='" + eventID + "' AND ARTIST_ID='" + artistID + "';").executeUpdate();
            if (s > 0) state = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return state;
    }

    @Override
    public String getEventArtistStatus(int eventID, int artistID) {
        NativeQuery nativeQuery = sessionManager.getSession().createNativeQuery("select STATUS from event_artist where EVENT_ID='" + eventID + "' AND ARTIST_ID='" + artistID + "' ;");
        String data = (String) nativeQuery.getSingleResult();
        return data;
    }
}
