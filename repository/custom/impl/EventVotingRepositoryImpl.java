package com.base245.apeevent.repository.custom.impl;

import com.base245.apeevent.entity.EventVoting;
import com.base245.apeevent.entity.EventVotingID;
import com.base245.apeevent.repository.CrudRepositoryImpl;
import com.base245.apeevent.repository.custom.EventVotingRepository;
import com.base245.apeevent.repository.session.SessionManager;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EventVotingRepositoryImpl extends CrudRepositoryImpl<EventVoting, EventVotingID> implements EventVotingRepository {

    @Autowired
    private SessionManager sessionManager;

    @Override
    public EventVoting getEventVotingByID(int eventID, String username) {
        NativeQuery<EventVoting> nativeQuery = sessionManager.getSession().createNativeQuery("select * from Event_Voting where eventID='" + eventID + "' AND username='" + username  +"' ;", EventVoting.class);

        List<EventVoting> data = nativeQuery.getResultList();

        if ((null != data) && !data.isEmpty()) {
            return data.get(0);
        }
        return null;
    }
}
