package com.base245.apeevent.service.util.email.listener;

import com.base245.apeevent.dto.OnRegistrationEventDTO;
import com.base245.apeevent.service.util.email.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * @author Hkp Maheshika
 */
@Component
public class RegistrationEventListener implements ApplicationListener<OnRegistrationEventDTO> {

    @Autowired
    EmailService emailService;

    @Override
    public void onApplicationEvent(OnRegistrationEventDTO event) {
        emailService.sendVerificationEmail(event);
    }
}
