package com.base245.apeevent.service.util.email;

import com.base245.apeevent.dto.*;
import com.base245.apeevent.entity.EventOrganizer;
import com.base245.apeevent.entity.Fan;
import com.base245.apeevent.repository.JoinQueryRepository;
import com.base245.apeevent.repository.custom.*;
import com.base245.apeevent.service.util.convertor.EntityConvertor;
import com.base245.apeevent.service.util.enum_types.ImageType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import java.util.UUID;

/**
 * @author Hkp Maheshika
 */
@Service
@PropertySource("classpath:application.properties")
public class EmailServiceImpl implements EmailService {

    @Autowired
    TokenRepository tokenRepository;

    @Autowired
    EntityConvertor entityConvertor;

    @Autowired
    LoginRepository loginRepository;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    EmailCrudService emailCrudService;

    @Autowired
    JoinQueryRepository joinQueryRepository;

    @Autowired
    Environment environment;

    @Autowired
    EventOrganizerRepository eventOrganizerRepository;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    FanRepository fanRepository;

    @Override
    public boolean sendVerificationEmail(OnRegistrationEventDTO dto) {
        String recipientAddress = dto.getUserRegisterDTO().getUserDTO().getEmail();
        //        String verificationToken = createVerificationToken().substring(0,6);
        String verificationToken = createVerificationToken();
        emailCrudService.saveVerificationToken(dto.getUserRegisterDTO().getLoginDTO(), verificationToken);
        return createVerificationEmail(recipientAddress, verificationToken);
    }

    private boolean createVerificationEmail(String receipientAddress, String verificationToken) {
        String subject = "ApeEvent Registration Confirmation";
//        String message = "You are almost there! Please enter the following verification code to activate your account." +
//                " \n\nVerification code: "+verificationToken;
        String url = environment.getRequiredProperty("backend_url") +
                "/api/v1/authentication/confirmRegistration?token=" + verificationToken;

        String message = "You are almost there! Please click the link to activate your account. \n\n" + url;
        return sendMail(receipientAddress, subject, message);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean resendVerificationEmail(String username) {
        String receipientAddress = joinQueryRepository.getEmailFromUsername(username);
        String verificationToken = createVerificationToken();
        emailCrudService.saveVerificationToken(new LoginDTO(username), verificationToken);
        return createVerificationEmail(receipientAddress, verificationToken);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean enableUser(String token) {
        return tokenRepository.checkVerificationToken(token);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean sendForgotPasswordEmail(String username) {
        String password = generatePassword();
        emailCrudService.saveGeneratedPassword(username, password);

        String recipientAddress = joinQueryRepository.getEmailFromUsername(username);
        if (null != recipientAddress) {
            String subject = "ApeEvent Password Reset";
            String message = "Your password for " + username + " have been reset. Please use this new password to Log in.\n\n" +
                    "Password: " + password + "\n\nThis is only a temporary password and we request you to change your Password " +
                    "from settings once you log in.\n\nThank You,\nTeam Ape Event";
            System.out.println(password);
            return sendMail(recipientAddress, subject, message);
        }

        return false;
    }

    @Override
    public boolean sendTicketPurchaseEmail(UserDTO userDTO, CartItemDTO cartItemDTO, double transAmount, List<String> qrPaths) {
        String recipientAddress = userDTO.getEmail();
        String subject = "Ticket Purchase Successful";
        String message = "You have Successfully purchased ticket(s) for the event \'" + cartItemDTO.getTicketPackageDTO().getEventName() +
                "\' on " + LocalDate.now().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)) + ".\n\n" +
                "Please find you ticket details below.\n\n" +
                cartItemDTO.getTicketPackageDTO().getName() + " : " + cartItemDTO.getQty() + " ticket(s) = " + transAmount + "\n\n" +
                "Please present the attached QR code(s) at the entrance of the event." +
                "\n\nThank You,\nTeam Ape Event";

        return sendEmailWithAttachment(recipientAddress, subject, message, qrPaths);
    }


    @Override
    public boolean sendCustomizedEmail(String eventName, String message, String subject) {

        List<String> emails = joinQueryRepository.findFanEmailsByEvent(eventName);
        if (emails != null) {
            for (String recipientAddress : emails)
                if (!sendMail(recipientAddress, subject, message))
                    return false;
            return true;
        }
        return false;
    }

    @Override
    public void sendBudgetAchievedEmail(EventDTO eventDTO, double accumulatedBudget) {
        String recipientAddress = eventRepository.findByID(eventDTO.getId()).getEventOrganizer().getEmail();
        String message = "Congratualations!\nTarget Budget for the event " + eventDTO.getName() + " achieved!!\n\n" +
                "We are happy to inform you that the target budget for the above event has been met as of today," +
                LocalDate.now().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)) + "\n\nTarget Budget : LKR" + eventDTO.getTargetBudget() +
                "\nTotal Accumulated Budget as of now : LKR" + accumulatedBudget + "\n\nCongratulations once again! " +
                "We encourage you to publish your event and take it to the next phase." +
                "\n\nThank You,\nTeam Ape Event";
        String subject = "Target Budget achieved for your event " + eventDTO.getName();

        sendMail(recipientAddress, subject, message);

    }

//        private helper methods-----------------------------------------------

    private String createVerificationToken() {
        return UUID.randomUUID().toString();
    }

    private String generatePassword() {
        return UUID.randomUUID().toString().substring(0, 8);
    }

    //send normal email
    private boolean sendMail(String recipientAddress, String subject, String message) {
        try {
            MimeMessage msg = mailSender.createMimeMessage();
            MimeMessageHelper email = new MimeMessageHelper(msg);
            email.setFrom(environment.getRequiredProperty("email"));
            email.setTo(recipientAddress);
            email.setSubject(subject);
            email.setText(message);
            mailSender.send(msg);
            System.out.println("email sent");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    //send event publishing email to event organizers and app owner (developer)
    @Override
    public boolean publishEventEmail(EventDTO eventDTO, Double forOrganizer, Double forDeveloper) {

        String confirmedLocation = "";
        String eventDetails = "Event Date: " + eventDTO.getEventDueDate() + "\n" +
                "Event Location : " + confirmedLocation + "\n\nThank You";

        String msgOrganizer = "Congratulations!\nYour event " + eventDTO.getName() + " has been published today. " +
                "The accumulated ticket sales of Rs." + forOrganizer + " will be deposited to your account soon.\n\n" +
                "We wish you all the best in organizing your event!\n\n" +
                "Please note that your fans have also been informed about the confirmation of the event with the following details.\n\n" + eventDetails;
        String subjectOrganizer = "Your Event at ApeEvent has been Confirmed";
        EventOrganizer organizer = (EventOrganizer) eventOrganizerRepository.findByName(eventDTO.getOrganizerName());
        if (sendMail(organizer.getEmail(), subjectOrganizer, msgOrganizer)) {
            String msgFan = "Hi,\n" +
                    "The event " + eventDTO.getName() + " you pre-ordered tickets for have been confirmed by the organizer." +
                    " Thank you for pre-ordering tickets!\n\nThe confirmed details are as follows." + eventDetails;
            String subjectFan = eventDTO.getName() + " has been Confirmed.";
            List<String> fansEmails = joinQueryRepository.findFanEmailsByEvent(eventDTO.getName());
            for (String email : fansEmails) {
                if (!sendMail(email, subjectFan, msgFan))
                    return false;
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean cancelEventEmailToOrganizer(EventDTO eventDTO) {
        String msgOrganizer = "Hi,\n\nThis is to inform you that the event " + eventDTO.getName() + " has been cancelled by you.\n\n" +
                "Please note that your fans have also been informed about the cancellation of the event and the relevant amount they have paid" +
                "have been refunded.";
        String subjectOrganizer = "Your Event at ApeEvent has been Cancelled";
        EventOrganizer organizer = (EventOrganizer) eventOrganizerRepository.findByName(eventDTO.getOrganizerName());
        return sendMail(organizer.getEmail(), subjectOrganizer, msgOrganizer);
    }

    @Override
    public boolean cancelEventEmailToFan(EventDTO eventDTO, double returnAmount, int fanId) {
        String subjectFan = eventDTO.getName() + " has been Cancelled.";
        Fan fan = fanRepository.findByID(fanId);
        String msgFan = "Hi " + fan.getName() + ",\n" +
                "The event " + eventDTO.getName() + "which you pre-ordered tickets, have been cancelled by the organizer." +
                " Please note that 85% of the amount you paid will be refunded soon. Thank you for choosing to fund this event.";
        return sendMail(fan.getEmail(), subjectFan, msgFan);
    }

    //send email with attachments
    private boolean sendEmailWithAttachment(String recipientAddress, String subject, String message, List<String> qrPaths) {
        try {
            MimeMessage msg = mailSender.createMimeMessage();
            MimeMessageHelper email = new MimeMessageHelper(msg, true);
            email.setFrom(environment.getRequiredProperty("email"));
            email.setTo(recipientAddress);
            email.setSubject(subject);
            email.setText(message);

            for (int i = 0; i < qrPaths.size(); i++) {
                String path = qrPaths.get(i);
                FileSystemResource file = new FileSystemResource(new File(path));
                email.addAttachment("QR Code " + (i + 1) + "." + ImageType.PNG.toString().toLowerCase(), file);
            }

            mailSender.send(msg);
            System.out.println("email sent");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


//    EmailCrudServiceImpl includes the Implementations for saving/modifying data related to this module

}
