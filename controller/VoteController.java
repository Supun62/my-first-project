package com.base245.apeevent.controller;


import com.base245.apeevent.dto.VoteCountDTO;
import com.base245.apeevent.dto.VotingDTO;
import com.base245.apeevent.service.custom.VotingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/v1/voting")
public class VoteController {

    @Autowired
    VotingService votingService;

    @PostMapping("/vote")
    public int vote(@RequestBody VotingDTO votingDTO){
        return votingService.vote(votingDTO);
    }

    @PostMapping("/getVoteStatus")
    public boolean getVoteStatus(@RequestBody VotingDTO votingDTO){
        return votingService.getVoteStatus(votingDTO);
    }

    @PostMapping("/getArtistVoteCount")
    public int getArtistVoteCount(@RequestBody VotingDTO votingDTO){
        return votingService.getArtistVoteCount(votingDTO);
    }

    @PostMapping("/getLocationVoteCount")
    public int getLocationVoteCount(@RequestBody VotingDTO votingDTO){
        return votingService.getLocationVoteCount(votingDTO);
    }

    @GetMapping("/locations")
    public List<VoteCountDTO> getLocationVotesByEvent(@RequestParam("eventID") int eventID) {
        return votingService.getLocationVotes(eventID);
    }

    @GetMapping("/artists")
    public List<VoteCountDTO> getArtistVotesByEvent(@RequestParam("eventID") int eventID) {
        return votingService.getArtistVotes(eventID);
    }

}
