package com.base245.apeevent.dto;

import java.time.LocalDate;

/**
 *  @author Supun Rangana as 1/23/2019
 */

public class VoteDTO extends SuperDTO{
    private int fanId;
    private int voteeId; /** this variable can either hold artist id or location id according to the situation*/
    private LocalDate voteDate;

    public VoteDTO(int fanId, int voteeId, LocalDate voteDate) {
        this.fanId = fanId;
        this.voteeId = voteeId;
        this.voteDate = voteDate;
    }

    public VoteDTO() {
    }

    public int getFanId() {
        return fanId;
    }

    public void setFanId(int fanId) {
        this.fanId = fanId;
    }

    public int getVoteeId() {
        return voteeId;
    }

    public void setVoteeId(int voteeId) {
        this.voteeId = voteeId;
    }

    public LocalDate getVoteDate() {
        return voteDate;
    }

    public void setVoteDate(LocalDate voteDate) {
        this.voteDate = voteDate;
    }
}
