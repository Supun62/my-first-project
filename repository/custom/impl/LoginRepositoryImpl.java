package com.base245.apeevent.repository.custom.impl;

import com.base245.apeevent.entity.Login;
import com.base245.apeevent.repository.CrudRepositoryImpl;
import com.base245.apeevent.repository.custom.LoginRepository;
import com.base245.apeevent.repository.session.SessionManager;
import com.base245.apeevent.service.util.enum_types.UserTypes;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dilini Peiris
 */

@Repository
public class LoginRepositoryImpl extends CrudRepositoryImpl<Login, String> implements LoginRepository {

    @Autowired
    private SessionManager sessionManager;

    @Override
    public boolean checkUserStatus(String username) {
        Query query = sessionManager.getSession().createSQLQuery("select STATUS from Login where username='" + username + "';");
        Character data = (Character) query.uniqueResult();
        return data == ('Y') ? true : false;
    }

    @Override
    public Login checkLogin(Login login) {
//        Query query = getSession().createSQLQuery("select * from Login where username='" + login.getUsername() + "' and " +
//                "password='" + login.getPassword() + "';");
//        List<Login> data = query.list();

        NativeQuery<Login> nativeQuery = sessionManager.getSession().createNativeQuery("select * from Login where username='" + login.getUsername() +
                "';", Login.class);
        List<Login> data = nativeQuery.getResultList();

        if (null != data) {
            return data.get(0);
        }
        return null;
    }

    @Override
    public boolean checkUsernameExists(String username) {
        NativeQuery nativeQuery = sessionManager.getSession().createNativeQuery("select username from Login where username='" + username + "';");
        String data = null;
        try {
            data = (String) nativeQuery.getSingleResult();
        } catch (Exception e) {

        }

        if (data != null) {
            return true;
        }
        return false;
    }

    @Override
    public List<String> getUserTypeAndId(String username) {
        NativeQuery<Login> nativeQuery = sessionManager.getSession().createNativeQuery("select * from Login where USERNAME='" + username + "';", Login.class);
        List<Login> data = nativeQuery.getResultList();
        int id = 0;
        List<String> list = new ArrayList<>();
        if (null != data) {
            Login login = (Login) data.get(0);
            if (login.getFan() != null) {
                id = login.getFan().getId();
                list.add(Integer.toString(id));
                list.add(UserTypes.FAN.toString());
            } else if (login.getEventOrganizer() != null) {
                id = login.getEventOrganizer().getId();
                list.add(Integer.toString(id));
                list.add(UserTypes.EVENT_ORGANIZER.toString());
            } else if (login.getFacilitator() != null) {
                id = login.getFacilitator().getId();
                list.add(Integer.toString(id));
                list.add(UserTypes.EVENT_ORGANIZER.toString());
            }

            return list;
        }
        return null;
    }

    @Override
    public Login loadUserByUsername(String username) {
        NativeQuery<Login> nativeQuery = sessionManager.getSession().createNativeQuery("select * from Login where username='" + username + "';", Login.class);
        List<Login> data = nativeQuery.getResultList();

        if (null != data) {
            return data.get(0);
        }
        return null;
    }
}
