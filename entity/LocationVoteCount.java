package com.base245.apeevent.entity;

import javax.persistence.EntityResult;
import javax.persistence.SqlResultSetMapping;

@SqlResultSetMapping(name = "LocationVoteResults",
        entities = {
                @EntityResult(entityClass = LocationVoting.class),
                @EntityResult(entityClass = Location.class)
        })
public class LocationVoteCount extends SuperEntity {
    String locationName;
    int voteCount;

    public LocationVoteCount(String locationName, int voteCount) {
        this.locationName = locationName;
        this.voteCount = voteCount;
    }

    public LocationVoteCount() {
    }

    public String getLocationName() {
        return locationName;
    }

    public int getVoteCount() {
        return voteCount;
    }

}
