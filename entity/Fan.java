package com.base245.apeevent.entity;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 *   @author Amesh M Jayaweera as 1/26/2019
 *   edited by Dilini Peiris
 */
@Entity
@Table(name = "FAN",
uniqueConstraints = {@UniqueConstraint(columnNames = {"EMAIL"})}
)
@DynamicUpdate
public class Fan extends UserEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "FID",nullable = false)
    private int id;
    @Column(name = "NAME",nullable = false)
    private String name;
    @Column(name = "DISTRICT")
    private String district;
    @Column(name = "EMAIL",nullable = false)
    private String email;
    @Column(name = "PROFILE_IMAGE")
    private String profileImage;

    @OneToMany
    @JoinColumn(name = "fk_card")
    private List<Card> card = new ArrayList<>();

    @OneToMany(mappedBy = "primaryKey.fan")
    private List<Ticket> ticket = new ArrayList<>();

    @OneToMany(mappedBy = "primaryKey.fan")
    private List<FanArtist> fanArtist = new ArrayList<>();

    @OneToMany(mappedBy = "primaryKey.fan")
    private List<FanLocation> fanLocation = new ArrayList<>();

    public Fan(int id, String name, String district, String email, String profileImage) {
        this.id = id;
        this.name = name;
        this.district = district;
        this.email = email;
        this.profileImage = profileImage;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public List<FanArtist> getFanArtist() {
        return fanArtist;
    }

    public void setFanArtist(List<FanArtist> fanArtist) {
        this.fanArtist = fanArtist;
    }

    public List<FanLocation> getFanLocation() {
        return fanLocation;
    }

    public void setFanLocation(List<FanLocation> fanLocation) {
        this.fanLocation = fanLocation;
    }

    public List<Ticket> getTicket() {
        return ticket;
    }

    public void setTicket(List<Ticket> ticket) {
        this.ticket = ticket;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public Fan() {
    }

    public List<Card> getCard() {
        return card;
    }

    public void setCard(List<Card> card) {
        this.card = card;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
