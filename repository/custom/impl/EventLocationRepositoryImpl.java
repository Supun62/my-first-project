package com.base245.apeevent.repository.custom.impl;

import com.base245.apeevent.entity.EventLocation;
import com.base245.apeevent.entity.EventLocationID;
import com.base245.apeevent.repository.CrudRepositoryImpl;
import com.base245.apeevent.repository.custom.EventLocationRepository;
import com.base245.apeevent.repository.session.SessionManager;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Dilini Peiris on 6/11/2019
 */
@Repository
public class EventLocationRepositoryImpl extends CrudRepositoryImpl<EventLocation, EventLocationID> implements EventLocationRepository {

    @Autowired
    private SessionManager sessionManager;

    @Override
    public List<EventLocation> getEventLocation(int eventID) {
        NativeQuery<EventLocation> nativeQuery = sessionManager.getSession().createNativeQuery("select * from event_location where EVENT_ID='" + eventID +
                "';", EventLocation.class);

        List<EventLocation> data = nativeQuery.getResultList();

        return data;
    }

    @Override
    public int updateVotes(int vote, int eventID, int locationID) {
        try {
            sessionManager.getSession().createNativeQuery("update event_location set NO_OF_VOTES = NO_OF_VOTES + '" +
                    vote + "' where EVENT_ID='" + eventID + "' AND LOCATION_ID='" +locationID+ "' ;").executeUpdate();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        NativeQuery nativeQuery = sessionManager.getSession().createNativeQuery("select NO_OF_VOTES from event_location where EVENT_ID='" +
                eventID + "' AND LOCATION_ID='" +locationID+ "' ;");
        int data = (int) nativeQuery.getSingleResult();
        return data;
    }

    @Override
    public int getLocationVoteCount(int eventID, int locationID) {
        NativeQuery nativeQuery = sessionManager.getSession().createNativeQuery("select NO_OF_VOTES from event_location where EVENT_ID='" +
                eventID + "' AND LOCATION_ID='" +locationID+ "' ;");
        int data = (int) nativeQuery.getSingleResult();
        return data;
    }

    @Override
    public boolean removeLocation(int eventID, int locationID) {
        boolean state = false;
        try {
            int s = sessionManager.getSession().createNativeQuery("DELETE FROM event_location WHERE EVENT_ID='" + eventID + "' AND LOCATION_ID='" + locationID + "';").executeUpdate();
            if (s > 0) state = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return state;
    }

    @Override
    public boolean confirmLocationForEvent(int eventID, int locationID) {
        boolean state = false;
        try {
            int s = sessionManager.getSession().createNativeQuery("UPDATE event_location SET STATUS = ' " + "CONFIRMED" + " ' WHERE EVENT_ID='" + eventID + "' AND LOCATION_ID='" + locationID + "';").executeUpdate();
            if (s > 0) state = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return state;
    }
}
