package com.base245.apeevent.repository.custom.impl;

import com.base245.apeevent.entity.Event;
import com.base245.apeevent.entity.Transaction;
import com.base245.apeevent.repository.CrudRepositoryImpl;
import com.base245.apeevent.repository.custom.EventRepository;
import com.base245.apeevent.repository.custom.TransactionRepository;
import com.base245.apeevent.repository.session.SessionManager;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Dilini Peiris on 6/11/2019
 */
@Repository
public class TransactionRepositoryImpl extends CrudRepositoryImpl<Transaction, Integer> implements TransactionRepository {

    @Autowired
    EventRepository eventRepository;

    @Autowired
    SessionManager sessionManager;

    public double getAccumulatedBudget(String eventName) {
        Event byName = (Event) eventRepository.findByName(eventName);
        Query query = sessionManager.getSession().createNativeQuery("select SUM(TO_AMOUNT) from TRANSACTION where fk_event=" + byName.getId());
        List<Double> data = query.getResultList();

        System.out.println(data);
        if (null != data) {
            return data.get(0);
        }
        return 0;
    }

    @Override
    public double getAccumulatedBudget(int eventId) {
        Query query = sessionManager.getSession().createNativeQuery("select SUM(TO_AMOUNT) from TRANSACTION where fk_event=" + eventId);
        List<Double> data = query.getResultList();

        System.out.println(data);
        if (null != data) {
            return data.get(0);
        }
        return 0;
    }

    @Override
    public List<Transaction> getTransactionsByEvent(int eventId) {
        Query query = sessionManager.getSession().createNativeQuery("select * from TRANSACTION where fk_event=" + eventId);
        List<Transaction> data = query.getResultList();

        System.out.println(data);
        return data;
    }
}
