package com.base245.apeevent.service.custom.impl;

import com.base245.apeevent.dto.UserDTO;
import com.base245.apeevent.repository.custom.EventOrganizerRepository;
import com.base245.apeevent.service.custom.EventOrganizerService;
import com.base245.apeevent.service.util.convertor.DtoConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.jws.soap.SOAPBinding;


@Service
@Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
public class EventOrganizerServiceImpl implements EventOrganizerService {

    @Autowired
    EventOrganizerRepository eventOrganizerRepository;

    @Autowired
    DtoConvertor dtoConvertor;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void setBankAccountForeignKey(int id, String accountNo) {
        System.out.println("Transaction begin === ");
        eventOrganizerRepository.updateBankAccountKey(id,accountNo);
    }


    @Override
    public UserDTO getEventOrganizerByName(String name) {
        return (UserDTO) dtoConvertor.convertToDTO(eventOrganizerRepository.getEventOrganizerByName(name));
    }
}
