package com.base245.apeevent.entity;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 *   @author Amesh M Jayaweera as 1/27/2019
 */
@Embeddable
public class FanArtistID implements Serializable {
    @ManyToOne
    private Fan fan;
    @ManyToOne
    private Artist artist;

    public FanArtistID(Fan fan, Artist artist) {
        this.fan = fan;
        this.artist = artist;
    }

    public FanArtistID() {
    }

    public Fan getFan() {
        return fan;
    }

    public void setFan(Fan fan) {
        this.fan = fan;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }
}
