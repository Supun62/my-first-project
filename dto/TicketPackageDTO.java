package com.base245.apeevent.dto;

/**
 *  @author Supun Rangana as 1/23/2019
 */
public class TicketPackageDTO extends SuperDTO{

    private int packId;
    private double price;
    private String name;
    private double discount;
    private String description;
    private String eventName;

    public TicketPackageDTO(int packId, double price, String name, double discount, String description) {
        this.packId = packId;
        this.price = price;
        this.name = name;
        this.discount = discount;
        this.description = description;
    }

    public TicketPackageDTO(int packId, double price, String name, double discount, String description, String eventName) {
        this.packId = packId;
        this.price = price;
        this.name = name;
        this.discount = discount;
        this.description = description;
        this.eventName = eventName;
    }

    public TicketPackageDTO() {
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public int getPackId() {
        return packId;
    }

    public double getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public double getDiscount() {
        return discount;
    }

    public String getDescription() {
        return description;
    }

    public void setPackId(int packId) {
        this.packId = packId;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
