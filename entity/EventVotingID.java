package com.base245.apeevent.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class EventVotingID implements Serializable {

    @Column(name = "EVENT_VOTING_ID")
    private long eventVotingID;

    private int eventID;

    private String username;

    public EventVotingID() {
    }

    public EventVotingID(long eventVotingID, int eventID, String username) {
        this.eventVotingID = eventVotingID;
        this.eventID = eventID;
        this.username = username;
    }


    public long getEventVotingID() {
        return eventVotingID;
    }

    public void setEventVotingID(long eventVotingID) {
        this.eventVotingID = eventVotingID;
    }

    public int getEventID() {
        return eventID;
    }

    public void setEventID(int eventID) {
        this.eventID = eventID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EventVotingID)) return false;
        EventVotingID that = (EventVotingID) o;
        return getEventVotingID() == that.getEventVotingID() &&
                getEventID() == that.getEventID() &&
                Objects.equals(getUsername(), that.getUsername());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEventVotingID(), getEventID(), getUsername());
    }
}
