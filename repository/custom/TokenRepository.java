package com.base245.apeevent.repository.custom;

import com.base245.apeevent.entity.VerificationToken;
import com.base245.apeevent.repository.CrudRepository;

/**
 * @author Hkp Maheshika
 */


public interface TokenRepository extends CrudRepository<VerificationToken, String> {

    boolean checkVerificationToken(String token);
}
