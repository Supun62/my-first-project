package com.base245.apeevent.dto;

import com.base245.apeevent.service.util.enum_types.ImageType;

public class ImageDTO {

    String file;
    String fileName;
    ImageType imageType;

    public ImageDTO() {
    }

    public ImageDTO(String file, String fileName) {
        this.file = file;
        this.fileName = fileName;
    }

    public ImageDTO(String file, String fileName, ImageType imageType) {
        this.file = file;
        this.fileName = fileName;
        this.imageType = imageType;
    }

    public ImageType getImageType() {
        return imageType;
    }

    public void setImageType(ImageType imageType) {
        this.imageType = imageType;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
