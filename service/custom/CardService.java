package com.base245.apeevent.service.custom;

import com.base245.apeevent.dto.CardDTO;
import com.base245.apeevent.service.SuperService;

/**
 * @author Dilini Peiris on 3/29/2019
 */
public interface CardService extends SuperService<CardDTO, String> {
}
