package com.base245.apeevent.repository.custom;

import com.base245.apeevent.entity.Facilitator;
import com.base245.apeevent.repository.CrudRepository;

/**
 * @author Dilini Peiris
 */


public interface FacilitatorRepository extends CrudRepository<Facilitator,Integer> {
    int findIdByEmail(String email);

    Facilitator findUserByEmail(String email);

}
