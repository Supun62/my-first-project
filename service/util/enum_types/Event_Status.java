package com.base245.apeevent.service.util.enum_types;

/**
 * @author Dilini Peiris on 6/20/2019
 */
public enum Event_Status {
    PENDING, CREATED, TARGET_ACHIEVED , PUBLISHED , CANCELLED , BANNED
}
