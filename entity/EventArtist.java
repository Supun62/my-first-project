package com.base245.apeevent.entity;

import javax.persistence.*;

@Entity
@Table(name = "EVENT_ARTIST")
@AssociationOverrides({
        @AssociationOverride(name = "primaryKey.event",
                joinColumns = @JoinColumn(name = "EVENT_ID")),
        @AssociationOverride(name = "primaryKey.artist",
                joinColumns = @JoinColumn(name = "ARTIST_ID"))
})

public class EventArtist extends SuperEntity{

    @EmbeddedId
    private EventArtistID primaryKey = new EventArtistID();

    @Column(name = "STATUS", nullable = false)
    private String status;

    @Column(name = "NO_OF_VOTES")
    private int noOfVotes;

    public EventArtist() {
    }

    public EventArtist(EventArtistID primaryKey) {
        this.primaryKey = primaryKey;
    }

    public EventArtist(EventArtistID primaryKey, String status,int noOfVotes) {
        this.primaryKey = primaryKey;
        this.status = status;
        this.noOfVotes = noOfVotes;
    }

    public int getNoOfVotes() {
        return noOfVotes;
    }

    public void setNoOfVotes(int noOfVotes) {
        this.noOfVotes = noOfVotes;
    }

    public EventArtistID getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(EventArtistID primaryKey) {
        this.primaryKey = primaryKey;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
