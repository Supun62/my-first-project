package com.base245.apeevent.entity;

import javax.persistence.*;

/**
 *   @author Amesh M Jayaweera as 1/27/2019
 *   edited by Dilini Peiris
 */
@Entity
@Table(name = "FAN_LOCATION")
@AssociationOverrides({
        @AssociationOverride(name = "primaryKey.fan",
                joinColumns = @JoinColumn(name = "FAN_ID")),
        @AssociationOverride(name = "primaryKey.location",
                joinColumns = @JoinColumn(name = "LOCATION_ID"))
})
public class FanLocation extends SuperEntity{
    @EmbeddedId
    private FanLocationID primaryKey = new FanLocationID();

    @Column(name = "DATE")
    private String votedDate;

    public FanLocationID getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(FanLocationID primaryKey) {
        this.primaryKey = primaryKey;
    }

    public String getVotedDate() {
        return votedDate;
    }

    public void setVotedDate(String votedDate) {
        this.votedDate = votedDate;
    }

    public FanLocation() {
    }

    public FanLocation(FanLocationID primaryKey, String votedDate) {
        this.primaryKey = primaryKey;
        this.votedDate = votedDate;
    }
}
