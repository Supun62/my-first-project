package com.base245.apeevent.entity;

import javax.persistence.*;

@Entity
@Table(name = "EVENT_LOCATION")
@AssociationOverrides({
        @AssociationOverride(name = "primaryKey.event",
                joinColumns = @JoinColumn(name = "EVENT_ID")),
        @AssociationOverride(name = "primaryKey.location",
                joinColumns = @JoinColumn(name = "LOCATION_ID"))
})
public class EventLocation extends SuperEntity{

    @EmbeddedId
    private EventLocationID primaryKey = new EventLocationID();

    @Column(name = "STATUS", nullable = false)
    private String status;

    @Column(name = "NO_OF_VOTES")
    private int noOfVotes;

    public EventLocation(EventLocationID primaryKey) {
        this.primaryKey = primaryKey;
    }

    public EventLocation(EventLocationID primaryKey, String status,int noOfVotes) {
        this.primaryKey = primaryKey;
        this.status = status;
        this.noOfVotes = noOfVotes;
    }

    public EventLocation() {
    }

    public int getNoOfVotes() {
        return noOfVotes;
    }

    public void setNoOfVotes(int noOfVotes) {
        this.noOfVotes = noOfVotes;
    }

    public EventLocationID getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(EventLocationID primaryKey) {
        this.primaryKey = primaryKey;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
