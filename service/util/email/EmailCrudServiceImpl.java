package com.base245.apeevent.service.util.email;

import com.base245.apeevent.dto.LoginDTO;
import com.base245.apeevent.entity.Login;
import com.base245.apeevent.entity.VerificationToken;
import com.base245.apeevent.repository.JoinQueryRepository;
import com.base245.apeevent.repository.custom.LoginRepository;
import com.base245.apeevent.repository.custom.TokenRepository;
import com.base245.apeevent.service.util.convertor.EntityConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Dilini Peiris on 5/21/2019
 */
@Service
public class EmailCrudServiceImpl implements EmailCrudService {

    @Autowired
    EntityConvertor entityConvertor;

    @Autowired
    LoginRepository loginRepository;

    @Autowired
    TokenRepository tokenRepository;

    @Autowired
    JoinQueryRepository joinQueryRepository;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean saveVerificationToken(LoginDTO loginDTO, String token) {
        VerificationToken verificationToken = new VerificationToken();
        Login login = (Login) entityConvertor.convertToEntity(loginDTO);
        verificationToken.setLogin(login);
        verificationToken.setToken(token);
        verificationToken.setExpiryDate(calculateExpiryDate());
        verificationToken.setCreatedDate(LocalDate.now());
        return tokenRepository.save(verificationToken);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean saveGeneratedPassword(String username, String password) {
        Login byID = loginRepository.findByID(username);
        System.out.println(byID);
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        byID.setPassword(passwordEncoder.encode(password));
        System.out.println(byID);
        return loginRepository.update(byID, username);
    }

    @Override
    public List<String> getEvents(String username) {
        return joinQueryRepository.findEventByOrganizer(username);
    }

//    private helper methods

    private LocalDate calculateExpiryDate() {
        return LocalDate.now().plusDays(2);
    }

}
