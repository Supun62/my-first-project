package com.base245.apeevent.service.util.enum_types;

public enum ImageType {
    PROFILE, ARTIST, EVENT, JPEG, PNG, LOCATION
}
