package com.base245.apeevent.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *   @author Amesh M Jayaweera as 1/26/2019
 */

@Entity
@Table(name = "BANK_ACCOUNT")
public class BankAccount extends SuperEntity{
    @Id
    @Column(name = "ACCOUNT_NO",nullable = false)
    private String accountNo;
    @Column(name = "BANK_NAME",nullable = false)
    private String bankName;
    @Column(name = "BRANCH",nullable = false)
    private String branch;
    @Column(name = "SWIFT_CODE",nullable = false)
    private String swiftCode;

    public BankAccount() {
    }

    public BankAccount(String accountNo, String bankName, String branch, String swiftCode) {
        this.accountNo = accountNo;
        this.bankName = bankName;
        this.branch = branch;
        this.swiftCode = swiftCode;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getSwiftCode() {
        return swiftCode;
    }

    public void setSwiftCode(String swiftCode) {
        this.swiftCode = swiftCode;
    }
}
