package com.base245.apeevent.service.custom;

import com.base245.apeevent.dto.VoteCountDTO;
import com.base245.apeevent.dto.VotingDTO;

import java.util.List;

public interface VotingService {

    int vote(VotingDTO votingDTO);

    boolean getVoteStatus(VotingDTO votingDTO);

    int getArtistVoteCount(VotingDTO votingDTO);

    int getLocationVoteCount(VotingDTO votingDTO);

    List<VoteCountDTO> getArtistVotes(int eventID);

    List<VoteCountDTO> getLocationVotes(int eventID);

}
