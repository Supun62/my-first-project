package com.base245.apeevent.controller;

import com.base245.apeevent.dto.*;
import com.base245.apeevent.service.custom.EventService;
import com.base245.apeevent.service.custom.TicketPackageService;
import com.base245.apeevent.service.util.enum_types.ImageType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Dilini Peiris on 6/9/2019
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/v1/event")
public class EventController {

    @Autowired
    EventService eventService;

    @Autowired
    TicketPackageService ticketPackageService;

    @Autowired
    FileController fileController;

    @Autowired
    VoteController voteController;

    @GetMapping("/all")
    public List<EventDTO> getAllEvents() {
        System.out.println("get all events - backend controller");
        return eventService.findAll();
    }

    @GetMapping("/in-progress")
    public List<EventDTO> getInProgressEvents() {
        return eventService.getInProgressEvents();
    }

    @GetMapping("/packages")
    public List<TicketPackageDTO> getTicketPackagesByEvent(@RequestParam("event") String eventName,
                                                           @RequestParam("organizer") String organizerName) {
        return ticketPackageService.getTicketPackagesByEvent(eventName, organizerName);
    }


    @GetMapping("/getEventsByEventOrganizer")
    public List<EventDTO> getEventsByEventOrganizer(@RequestParam("eventOrganizerID") int eventOrganizerID) {

        List<EventDTO> eventDTOS = eventService.getEventsByEventOrganizer(eventOrganizerID);

        for (EventDTO eventDTO : eventDTOS) {
            ImageDTO imageDTO = new ImageDTO();
            imageDTO.setFile(null);
            imageDTO.setFileName(eventDTO.getOrganizerName() + eventDTO.getName().trim() + "." + ImageType.JPEG.toString().toLowerCase());
            imageDTO.setImageType(ImageType.EVENT);
            eventDTO.setImageFile(fileController.getImage(imageDTO).getFile());
        }
        return eventDTOS;
    }


    @GetMapping("/getOnGoingEventsByEventOrganizer")
    public List<EventDTO> getOnGoingEventsByEventOrganizer(@RequestParam("eventOrganizerID") int eventOrganizerID) {

        List<EventDTO> eventDTOS = eventService.getOnGoingEventsByEventOrganizer(eventOrganizerID);

        for (EventDTO eventDTO : eventDTOS) {
            ImageDTO imageDTO = new ImageDTO();
            imageDTO.setFile(null);
            imageDTO.setFileName(eventDTO.getOrganizerName() + eventDTO.getName().trim() + "." + ImageType.JPEG.toString().toLowerCase());
            imageDTO.setImageType(ImageType.EVENT);
            eventDTO.setImageFile(fileController.getImage(imageDTO).getFile());
        }
        return eventDTOS;
    }

    @GetMapping("/getPreviousEventsByEventOrganizer")
    public List<EventDTO> getPreviousEventsByEventOrganizer(@RequestParam("eventOrganizerID") int eventOrganizerID) {

        List<EventDTO> eventDTOS = eventService.getPreviousEventsByEventOrganizer(eventOrganizerID);

        for (EventDTO eventDTO : eventDTOS) {
            ImageDTO imageDTO = new ImageDTO();
            imageDTO.setFile(null);
            imageDTO.setFileName(eventDTO.getOrganizerName() + eventDTO.getName().trim() + "." + ImageType.JPEG.toString().toLowerCase());
            imageDTO.setImageType(ImageType.EVENT);
            eventDTO.setImageFile(fileController.getImage(imageDTO).getFile());
        }
        return eventDTOS;
    }



    @GetMapping("/getEventByEventID")
    public EventDTO getEventByEventID(@RequestParam("eventID") int eventID) {
        EventDTO eventDTO = eventService.getEventByEventID(eventID);
        for (ArtistDTO artistDTO : eventDTO.getArtistDTOS()) {

            ImageDTO imageDTO = new ImageDTO();
            imageDTO.setImageType(ImageType.ARTIST);
            imageDTO.setFileName(artistDTO.getArtistImage());
            artistDTO.setImageFile(fileController.getImage(imageDTO).getFile());

            VotingDTO votingDTO = new VotingDTO(eventID, artistDTO.getId());
            artistDTO.setNo_of_votes(voteController.getArtistVoteCount(votingDTO));

        }
        return eventDTO;
    }

    @GetMapping("/removeTicketPackageFromEvent")
    public boolean removeTicketPackage(@RequestParam("ticketPackageID") int ticketPackageID) {
        return eventService.removeTicketPackage(ticketPackageID);
    }

    @GetMapping("/removeArtistFromEvent")
    public boolean removeArtistFromEvent(@RequestParam("eventID") int eventID, @RequestParam("artistID") int artistID) {
        return eventService.removeArtist(eventID, artistID);
    }

    @PostMapping("/updateTicketPackage")
    public boolean updateTicketPackage(@RequestBody TicketPackageDTO ticketPackageDTO) {
        return eventService.updateTicketPackage(ticketPackageDTO);
    }

    @PostMapping("/addNewTicketPackageToEvent")
    public EventDTO addNewTicketPackageToEvent(@RequestBody CustomDTO customDTO) {
        return eventService.addNewTicketPackageToEvent(customDTO);
    }

    @PostMapping("/addNewArtistToEvent")
    public EventDTO addNewArtistToEvent(@RequestBody CustomDTO customDTO) {
        return eventService.addNewArtistToEvent(customDTO);
    }

    @PostMapping("/updateEvent")
    public boolean updateEvent(@RequestBody EventDTO eventDTO) {
        return eventService.updateEvent(eventDTO);
    }


    @GetMapping("/confirmArtistForEvent")
    public boolean confirmArtistForEvent(@RequestParam("eventID") int eventID, @RequestParam("artistID") int artistID, @RequestParam("status") String status) {
        return eventService.confirmArtistForEvent(eventID, artistID, status);
    }

    @GetMapping("/removeLocationFormEvent")
    public boolean removeLocation(@RequestParam("eventID") int eventID, @RequestParam("locationID") int locationID) {
        return eventService.removeLocation(eventID, locationID);
    }

    @PostMapping("/addNewLocationToEvent")
    public EventDTO addNewLocation(@RequestBody CustomDTO customDTO) {
        return eventService.addNewLocation(customDTO);
    }


    @GetMapping("/updateEventName")
    public boolean updateEventName(@RequestParam("eventID") int eventID, @RequestParam("name") String name, @RequestParam("eventOrganizer") String eventOrganizer) {
        return eventService.updateEventName(eventID, name, eventOrganizer);
    }

    @GetMapping("/updateEventPreOrderClosingDate")
    public boolean updateEventPreOrderClosingDate(@RequestParam("eventID") int eventID, @RequestParam("preOrderClosingDate") String preOrderClosingDate) {
        return eventService.updateEventPreOrderClosingDate(eventID, preOrderClosingDate);
    }

    @GetMapping("/updateEventTargetBudget")
    public boolean updateEventTargetBudget(@RequestParam("eventID") int eventID, @RequestParam("targetBudget") double targetBudget) {
        return eventService.updateEventTargetBudget(eventID, targetBudget);
    }

    @GetMapping("/confirmLocationForEvent")
    public boolean confirmLocationForEvent(@RequestParam("eventID") int eventID, @RequestParam("locationID") int locationID) {
        return eventService.confirmLocationForEvent(eventID, locationID);
    }

    @GetMapping("/cancelTheEvent")
    public boolean cancelTheEvent(@RequestParam("eventID") int eventID) {
        return eventService.cancelTheEvent(eventID);
    }

    @GetMapping("/publishTheEvent")
    public boolean publishTheEvent(@RequestParam("eventID") int eventID, @RequestParam("eventDueDate") String eventDueDate, @RequestParam("locationID") int locationID) {
        return eventService.publishTheEvent(eventID, eventDueDate, locationID);
    }

    @GetMapping("/revenue")
    public double getCollectedRevenue(@RequestParam("eventID") int eventID) {
        return eventService.getCollectedRevenue(eventID);
    }

}


