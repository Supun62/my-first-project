package com.base245.apeevent.repository.custom.impl;

import com.base245.apeevent.entity.Ticket;
import com.base245.apeevent.entity.TicketID;
import com.base245.apeevent.repository.CrudRepositoryImpl;
import com.base245.apeevent.repository.custom.TicketRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Dilini Peiris on 6/11/2019
 */
@Repository
public class TicketRepositoryImpl extends CrudRepositoryImpl<Ticket, TicketID> implements TicketRepository {

}
