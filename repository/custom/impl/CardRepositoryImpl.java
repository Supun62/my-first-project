package com.base245.apeevent.repository.custom.impl;

import com.base245.apeevent.entity.Card;
import com.base245.apeevent.repository.CrudRepositoryImpl;
import com.base245.apeevent.repository.custom.CardRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Dilini Peiris on 3/29/2019
 */

@Repository
public class CardRepositoryImpl extends CrudRepositoryImpl<Card,String> implements CardRepository{
}
