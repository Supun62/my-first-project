package com.base245.apeevent.dto;

import java.util.Calendar;

/**
 *  @author Supun Rangana as 1/23/2019
 */
public class TransactionDTO extends SuperDTO{

    private int transId;
    private String from;
    private double from_amount;
    private String to;
    private double to_amount;
    private Calendar datetime;

    public TransactionDTO() {
    }

    public TransactionDTO(int transId, String from, double from_amount, String to, double to_amount, Calendar datetime) {
        this.transId = transId;
        this.from = from;
        this.from_amount = from_amount;
        this.to = to;
        this.to_amount = to_amount;
        this.datetime = datetime;
    }

    public double getFrom_amount() {
        return from_amount;
    }

    public void setFrom_amount(double from_amount) {
        this.from_amount = from_amount;
    }

    public double getTo_amount() {
        return to_amount;
    }

    public void setTo_amount(double to_amount) {
        this.to_amount = to_amount;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Calendar getDatetime() {
        return datetime;
    }

    public void setDatetime(Calendar datetime) {
        this.datetime = datetime;
    }

    public int getTransId() {
        return transId;
    }

    public String getFrom() {
        return from;
    }

    public Calendar getDate() {
        return datetime;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setDate(Calendar datetime) {
        this.datetime = datetime;
    }
}