package com.base245.apeevent.repository.custom;

import com.base245.apeevent.entity.TicketPackage;
import com.base245.apeevent.repository.CrudRepository;

import java.util.List;

/**
 * @author Supun Rangana as 2/20/2019
 */


public interface TicketPackageRepository extends CrudRepository<TicketPackage,Integer> {
    List<TicketPackage> getTicketPackageList(int eventID);

    boolean removeTicketPackage(int ticketPackageID);
}
