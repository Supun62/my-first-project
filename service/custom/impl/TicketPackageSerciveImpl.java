package com.base245.apeevent.service.custom.impl;

import com.base245.apeevent.dto.TicketPackageDTO;
import com.base245.apeevent.entity.TicketPackage;
import com.base245.apeevent.repository.custom.TicketPackageRepository;
import com.base245.apeevent.service.custom.TicketPackageService;
import com.base245.apeevent.service.util.convertor.DtoConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Dilini Peiris on 6/9/2019
 */

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TicketPackageSerciveImpl implements TicketPackageService {

    @Autowired
    TicketPackageRepository ticketPackageRepository;

    @Autowired
    DtoConvertor dtoConvertor;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public boolean save(TicketPackageDTO dto) {
        return false;
    }

    @Override
    public boolean update(TicketPackageDTO dto) {
        return false;
    }

    @Override
    public boolean delete(Integer key) {
        return false;
    }

    @Override
    public TicketPackageDTO findByID(Integer key) {
        return null;
    }

    @Override
    public TicketPackageDTO findByName(String name) {
        return null;
    }

    @Override
    public List<TicketPackageDTO> getTicketPackagesByEvent(String eventName, String organizerName) {
        return null;
    }

    @Override
    public List<TicketPackageDTO> getTicketPackagesByEvent(int eventID) {
        List<TicketPackage> ticketPackageList = ticketPackageRepository.getTicketPackageList(eventID);
        System.out.println("ticket packages in event " + eventID + "=================" + ticketPackageList);
        List<TicketPackageDTO> ticketPackageDTOS = null;
        if (null != ticketPackageList && 0 != ticketPackageList.size()) {
            ticketPackageDTOS = dtoConvertor.convertToDTOList(ticketPackageList);
        }
        System.out.println("ticket package dtos===================" + ticketPackageDTOS);
        return ticketPackageDTOS;
    }
}
