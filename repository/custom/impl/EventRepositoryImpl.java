package com.base245.apeevent.repository.custom.impl;

import com.base245.apeevent.entity.Event;
import com.base245.apeevent.repository.CrudRepositoryImpl;
import com.base245.apeevent.repository.custom.EventRepository;
import com.base245.apeevent.repository.session.SessionManager;
import com.base245.apeevent.service.util.convertor.DtoConvertor;
import com.base245.apeevent.service.util.enum_types.Event_Status;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Supun Rangana as 2/24/2019
 */

@Repository
public class EventRepositoryImpl extends CrudRepositoryImpl<Event, Integer> implements EventRepository {

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    private DtoConvertor dtoConvertor;

    @Override
    public Event findByName(String name) {

        NativeQuery<Event> nativeQuery = sessionManager.getSession().createNativeQuery
                ("select * from Event where name='" + name + "';", Event.class);
        List<Event> data = nativeQuery.getResultList();
        System.out.println(data);

        if (null != data) {
            return data.get(0);
        }
        return null;
    }

    @Override
    public int updateVotes(int vote, int eventID) {

        if (vote == 1) {
            sessionManager.getSession().createNativeQuery("update event set NO_OF_VOTES = NO_OF_VOTES + 1  where ID='"
                    + eventID + "';").executeUpdate();
        } else if (vote == -1) {
            sessionManager.getSession().createNativeQuery("update event set NO_OF_VOTES = NO_OF_VOTES -1 where ID='"
                    + eventID + "';").executeUpdate();
        }


        NativeQuery nativeQuery = sessionManager.getSession().createNativeQuery("select NO_OF_VOTES from event where ID='"
                + eventID + "';");
        int data = (int) nativeQuery.getSingleResult();
        return data;
    }

    @Override
    public List<Event> getEventsByEventOrganizer(int eventOrganizerID) {
        NativeQuery<Event> nativeQuery = sessionManager.getSession().createNativeQuery("select * from Event WHERE eventOrganizer='"
                + eventOrganizerID + "';", Event.class);
        return nativeQuery.getResultList();
    }

    @Override
    public boolean updateEventName(int eventID, String eventName, String eventOrganizer) {
        boolean state = false;
        try {
            int s = sessionManager.getSession().createNativeQuery("UPDATE event SET NAME='" + eventName + "' , IMAGE='" +
                    eventOrganizer + eventName + ".jpeg" + "', STATUS='" + Event_Status.PENDING.toString() +
                    "' WHERE ID = '" + eventID + "';").executeUpdate();
            if (s > 0) state = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return state;
    }

    @Override
    public boolean updateEventPreOrderClosingDate(int eventID, String preOrderClosingDate) {
        boolean state = false;
        try {
            int s = sessionManager.getSession().createNativeQuery("UPDATE event SET PRE_ORDER_CLOSING_DATE='" +
                    preOrderClosingDate + "', STATUS='" + Event_Status.PENDING.toString() + "' WHERE ID ='" + eventID + "';").executeUpdate();
            if (s > 0) state = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return state;
    }

    @Override
    public boolean updateEventTargetBudget(int eventID, double targetBudget) {
        boolean state = false;
        try {
            int s = sessionManager.getSession().createNativeQuery("UPDATE event SET TARGET_BUDGET='" + targetBudget +
                    "', STATUS='" + Event_Status.PENDING.toString() + "' WHERE ID = '" + eventID + "';").executeUpdate();
            if (s > 0) state = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return state;
    }

    @Override
    public boolean cancelTheEvent(int eventID) {
        boolean state = false;
        try {
            int s = sessionManager.getSession().createNativeQuery("UPDATE event SET STATUS='" + Event_Status.CANCELLED.toString() +
                    "' WHERE ID = '" + eventID + "';").executeUpdate();
            if (s > 0) state = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return state;
    }

    @Override
    public boolean publishTheEvent(int eventID, String eventDueDate, String publishedDate) {
        boolean state = false;
        try {
            int s = sessionManager.getSession().createNativeQuery("UPDATE event SET STATUS='" + Event_Status.PUBLISHED.toString() + "' , EVENT_DUE_DATE='" + eventDueDate + "', PUBLISHED_DATE='" + publishedDate + "' WHERE ID = '" + eventID + "';").executeUpdate();
            if (s > 0) state = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return state;
    }

    @Override
    public boolean allowEventByAdmin(int eventId) {
        return sessionManager.getSession().createNativeQuery("UPDATE event SET STATUS='" + Event_Status.CREATED.toString() + "' WHERE ID = '" + eventId + "';").executeUpdate() > 0;
    }

    @Override
    public long getCancelledEventCount() {
        NativeQuery sqlQuery = sessionManager.getSession().createSQLQuery("SELECT COUNT(*) FROM EVENT WHERE STATUS='" + Event_Status.CANCELLED.toString() + "';");
        return (long) sqlQuery.iterate().next();
    }

    @Override
    public long getPendingEventCount() {
        NativeQuery sqlQuery = sessionManager.getSession().createSQLQuery("SELECT COUNT(*) FROM EVENT WHERE STATUS='" + Event_Status.PENDING.toString() + "';");
        return (long) sqlQuery.iterate().next();
    }

    @Override
    public long getInProgressEventCount() {
        NativeQuery sqlQuery = sessionManager.getSession().createSQLQuery("SELECT COUNT(*) FROM EVENT WHERE STATUS='" +
                Event_Status.CREATED.toString() + "' OR STATUS='" + Event_Status.TARGET_ACHIEVED + "' OR STATUS='" + Event_Status.PUBLISHED + "';");
        return (long) sqlQuery.iterate().next();
    }

    @Override
    public List<Event> getPendingEvents() {
        NativeQuery<Event> nativeQuery = sessionManager.getSession().createNativeQuery("SELECT * FROM EVENT WHERE STATUS='" +
                Event_Status.PENDING.toString() + "';", Event.class);
        return nativeQuery.getResultList();
    }

    @Override
    public List<Event> getInProgressEvents() {
        NativeQuery nativeQuery = sessionManager.getSession().createNativeQuery("SELECT * FROM EVENT WHERE STATUS='" +
                Event_Status.CREATED.toString() + "' OR STATUS='" + Event_Status.TARGET_ACHIEVED + "' OR STATUS='" + Event_Status.PUBLISHED + "';", Event.class);
        return nativeQuery.getResultList();
    }
}
