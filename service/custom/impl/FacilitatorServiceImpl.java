package com.base245.apeevent.service.custom.impl;

import com.base245.apeevent.service.custom.FacilitatorService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Dilini Peiris
 */

@Service
@Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
public class FacilitatorServiceImpl implements FacilitatorService {

}
