package com.base245.apeevent.service.custom.impl;

import com.base245.apeevent.dto.*;
import com.base245.apeevent.entity.*;
import com.base245.apeevent.repository.JoinQueryRepository;
import com.base245.apeevent.repository.custom.*;
import com.base245.apeevent.service.custom.ArtistService;
import com.base245.apeevent.service.custom.EventService;
import com.base245.apeevent.service.custom.TicketPackageService;
import com.base245.apeevent.service.custom.VenueService;
import com.base245.apeevent.service.util.convertor.DtoConvertor;
import com.base245.apeevent.service.util.convertor.EntityConvertor;
import com.base245.apeevent.service.util.email.EmailService;
import com.base245.apeevent.service.util.enum_types.EventFacilitator_Status;
import com.base245.apeevent.service.util.enum_types.Event_Status;
import com.base245.apeevent.service.util.enum_types.ImageType;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Supun Rangana as 2/18/2019
 */

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class EventServiceImpl implements EventService {

    @Autowired
    DtoConvertor dtoConvertor;

    @Autowired
    EntityConvertor entityConvertor;

    @Autowired
    EventOrganizerRepository eventOrganizerRepository;

    @Autowired
    TicketPackageRepository ticketPackageRepository;

    @Autowired
    TicketPackageService ticketPackageService;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    EventArtistRepository eventArtistRepository;

    @Autowired
    EventLocationRepository eventLocationRepository;

    @Autowired
    ArtistService artistService;

    @Autowired
    ArtistRepository artistRepository;

    @Autowired
    LocationRepository locationRepository;

    @Autowired
    VenueService venueService;

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    EmailService emailService;

    @Autowired
    JoinQueryRepository joinQueryRepository;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public boolean save(EventDTO dto) {
        dto.setName(dto.getName().trim());
        System.out.println("EVENT DTO ======== " + new Gson().toJson(dto));

        boolean saveArtist = true;
        boolean savePackages = true;
        boolean saveVenue = true;
        boolean saveEvent = true;

        Event event = (Event) entityConvertor.convertToEntity(dto);
        event.setCreatedDateTime(LocalDateTime.now().toString());
        event.setPreOrderClosingDate(dto.getPreOrderClosingDate());
        event.setEventOrganizer((EventOrganizer) eventOrganizerRepository.findByName(dto.getOrganizerName()));

        dto.setImage(dto.getOrganizerName() + dto.getName() + "." + ImageType.JPEG.toString().toLowerCase());
        event.setImage(dto.getImage());
        event.setStatus(Event_Status.PENDING.toString());

        try {
            saveEvent = eventRepository.save(event);
        } catch (Exception e) {
            saveEvent = false;
            System.out.println("Exception ===== Event" + e.getMessage());
        }

        if (saveEvent) {
            try {
                for (TicketPackageDTO ticketPackageDTO : dto.getTicketPackageDTOS()) {
                    TicketPackage ticketPackage = (TicketPackage) entityConvertor.convertToEntity(ticketPackageDTO);
                    ticketPackage.setEvent(event);
                    savePackages = savePackages && ticketPackageRepository.save(ticketPackage);
                }
            } catch (Exception e) {
                savePackages = false;
                System.out.println("Exception ===== TicketPackage" + e.getMessage());
            }

            if (savePackages) {
                try {
                    for (ArtistDTO artistDTO : dto.getArtistDTOS()) {
                        Artist artist = (Artist) entityConvertor.convertToEntity(artistDTO);
                        artist.setImage(artistDTO.getName() + "." + ImageType.JPEG.toString().toLowerCase());
                        // artistRepository.save(artist);
                        long count = artistRepository.getCount();
                        saveArtist = saveArtist && eventArtistRepository.save(new EventArtist(new EventArtistID(++count, event, artist),
                                EventFacilitator_Status.PENDING.toString(), 0));
                    }
                } catch (Exception e) {
                    saveArtist = false;
                    System.out.println("Exception ===== Artist" + e.getMessage());
                }

                if (saveArtist) {
                    try {
                        for (LocationDTO locationDTO : dto.getLocationDTOS()) {
                            Location location = (Location) entityConvertor.convertToEntity(locationDTO);
                            locationRepository.save(location);
                            long count = locationRepository.getCount();
                            saveVenue = saveVenue && eventLocationRepository.save(new EventLocation(new EventLocationID(++count, event, location),
                                    EventFacilitator_Status.PENDING.toString(), 0));
                        }
                    } catch (Exception e) {
                        saveVenue = false;
                        System.out.println("Exception ===== Venue" + e.getMessage());
                    }
                }
            }
        }
        return (saveEvent && savePackages && saveArtist && saveVenue);
    }


    @Override
    public boolean update(EventDTO dto) {
        Event event = (Event) entityConvertor.convertToEntity(dto);
        return eventRepository.update(event, dto.getId());
    }

    @Override
    public boolean delete(Integer key) {
        return false;
    }

    @Override
    public EventDTO findByID(Integer key) {
        Event byID = eventRepository.findByID(key);
        return (EventDTO) dtoConvertor.convertToDTO(byID);
    }

    @Override
    public EventDTO findByName(String name) {
        Event byName = (Event) eventRepository.findByName(name);
        return (EventDTO) dtoConvertor.convertToDTO(byName);
    }

    @Override
    public List<EventDTO> findAll() {
        System.out.println("event service findall");
        List<EventDTO> list = null;
        try {
            list = dtoConvertor.convertToDTOList(eventRepository.findAll());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        System.out.println(list);
        if (null != list) {
            System.out.println("list is not null");
            return getEventListDetails(list);
        } else
            throw new RuntimeException("There are no events posted");
    }


    @Override
    public boolean compareEventBudget(int id) {
        System.out.println("event id : " + id);
        EventDTO byID = findByID(id);
        System.out.println("compare event budget----------------------" + byID + " status: ");
        boolean b = false;
        if (Event_Status.CREATED == byID.getStatus()) {
            double accumulatedBudget = transactionRepository.getAccumulatedBudget(id);
            b = accumulatedBudget > byID.getTargetBudget();
            if (b) {
                System.out.println("target budget achieved--------------------");
                emailService.sendBudgetAchievedEmail(byID, accumulatedBudget);
                byID.setStatus(Event_Status.TARGET_ACHIEVED);
                b = update(byID);
            }
        }
        return b;
    }

    @Override
    public int updatePostVote(int vote, int eventID) {
        return eventRepository.updateVotes(vote, eventID);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public int updatePostArtistVote(int vote, int eventID, int artistID) {
        return eventArtistRepository.updateVotes(vote, eventID, artistID);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public int updatePostLocationVote(int vote, int eventID, int locationID) {
        return eventLocationRepository.updateVotes(vote, eventID, locationID);
    }

    @Override
    public List<EventDTO> getEventsByEventOrganizer(int eventOrganizerID) {
        List<Event> eventsByEventOrganizer = eventRepository.getEventsByEventOrganizer(eventOrganizerID);
        if (eventsByEventOrganizer != null) {
            List<EventDTO> eventDTOS = dtoConvertor.convertToDTOList(eventsByEventOrganizer);
            int eventID;
            for (EventDTO eventDTO : eventDTOS) {
                eventID = eventDTO.getId();
                eventDTO.setArtistDTOS(artistService.getArtistList(eventID));
                eventDTO.setLocationDTOS(venueService.getVenueList(eventID));
                eventDTO.setTicketPackageDTOS(ticketPackageService.getTicketPackagesByEvent(eventID));
            }
            return eventDTOS;
        }
        return null;
    }

    @Override
    public List<EventDTO> getOnGoingEventsByEventOrganizer(int eventOrganizerID) {

        Date currentDate = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("M/dd/yyyy h:mm a");
        Date date;

        List<EventDTO> onGoingEventsList = new ArrayList<>();

        List<EventDTO> eventDTOS = getEventsByEventOrganizer(eventOrganizerID);

        for (EventDTO eventDTO : eventDTOS) {
            if (eventDTO.getEventDueDate() != null) {
                try {
                    date = formatter.parse(eventDTO.getEventDueDate());
                    if (date.compareTo(currentDate) > 0) onGoingEventsList.add(eventDTO);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else {
                onGoingEventsList.add(eventDTO);
            }
        }

        return onGoingEventsList;

    }

    @Override
    public List<EventDTO> getPreviousEventsByEventOrganizer(int eventOrganizerID) {

        Date currentDate = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("M/dd/yyyy h:mm a");
        Date date;

        List<EventDTO> previousEventsList = new ArrayList<>();

        List<EventDTO> eventDTOS = getEventsByEventOrganizer(eventOrganizerID);

        for (EventDTO eventDTO : eventDTOS) {
            if (eventDTO.getEventDueDate() != null) {
                try {
                    date = formatter.parse(eventDTO.getEventDueDate());
                    if (date.compareTo(currentDate) < 0) previousEventsList.add(eventDTO);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else {

            }
        }

        return previousEventsList;
    }

    @Override
    public EventDTO getEventByEventID(int eventID) {
        EventDTO eventDTO = (EventDTO) dtoConvertor.convertToDTO(eventRepository.findByID(eventID));

        List<ArtistDTO> artistDTOS = artistService.getArtistList(eventID);

        for (ArtistDTO artistDTO : artistDTOS) {
            String eventArtistStatus = eventArtistRepository.getEventArtistStatus(eventID, artistDTO.getId());
            artistDTO.setstate(EventFacilitator_Status.valueOf(eventArtistStatus));
        }
        eventDTO.setArtistDTOS(artistDTOS);

        eventDTO.setLocationDTOS(venueService.getVenueList(eventID));
        eventDTO.setTicketPackageDTOS(ticketPackageService.getTicketPackagesByEvent(eventID));
        return eventDTO;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean removeTicketPackage(int ticketPackageID) {
        return ticketPackageRepository.removeTicketPackage(ticketPackageID);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean removeArtist(int eventID, int artistID) {
        return eventArtistRepository.removeArtistFromEvent(eventID, artistID);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean updateTicketPackage(TicketPackageDTO ticketPackageDTO) {
        return ticketPackageRepository.update((TicketPackage) entityConvertor.convertToEntity(ticketPackageDTO), ticketPackageDTO.getPackId());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public EventDTO addNewTicketPackageToEvent(CustomDTO customDTO) {
        boolean status = true;
        for (TicketPackageDTO ticketPackageDTO : customDTO.getTicketPackageDTOS()) {
            TicketPackage ticketPackage = (TicketPackage) entityConvertor.convertToEntity(ticketPackageDTO);
            ticketPackage.setEvent((Event) entityConvertor.convertToEntity(customDTO.getEventDTO()));
            status = status & ticketPackageRepository.save(ticketPackage);
        }
        return getEventByEventID(customDTO.getEventDTO().getId());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public EventDTO addNewArtistToEvent(CustomDTO customDTO) {
        boolean status = true;
        for (ArtistDTO artistDTO : customDTO.getArtistDTOS()) {
            long count = artistRepository.getCount();
            status = status & eventArtistRepository.save(new EventArtist(new EventArtistID(++count,
                    (Event) entityConvertor.convertToEntity(customDTO.getEventDTO()), (Artist) entityConvertor.convertToEntity(artistDTO)), EventFacilitator_Status.PENDING.toString(), 0));
        }
        return getEventByEventID(customDTO.getEventDTO().getId());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean removeLocation(int eventID, int locationID) {
        boolean status;
        status = eventLocationRepository.removeLocation(eventID, locationID);
        return status;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public EventDTO addNewLocation(CustomDTO customDTO) {
        boolean status = true;
        for (LocationDTO locationDTO : customDTO.getLocationDTOS()) {
            Location location = (Location) entityConvertor.convertToEntity(locationDTO);
            locationRepository.save(location);
            long count = locationRepository.getCount();
            status = status && eventLocationRepository.save(new EventLocation(new EventLocationID(++count,
                    (Event) entityConvertor.convertToEntity(customDTO.getEventDTO()), location), EventFacilitator_Status.PENDING.toString(), 0));
        }
        return getEventByEventID(customDTO.getEventDTO().getId());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean updateEvent(EventDTO eventDTO) {
        return eventRepository.update((Event) entityConvertor.convertToEntity(eventDTO), eventDTO.getId());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean confirmArtistForEvent(int eventID, int artistID, String status) {
        return eventArtistRepository.confirmArtistForEvent(eventID, artistID, status);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean updateEventName(int eventID, String name, String eventOrganizer) {
        boolean status = true;
        status = status & eventRepository.updateEventName(eventID, name, eventOrganizer);
        return status;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean updateEventPreOrderClosingDate(int eventID, String preOrderClosingDate) {
        return eventRepository.updateEventPreOrderClosingDate(eventID, preOrderClosingDate);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean updateEventTargetBudget(int eventID, double targetBudget) {
        return eventRepository.updateEventTargetBudget(eventID, targetBudget);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean confirmLocationForEvent(int eventID, int locationID) {
        return eventLocationRepository.confirmLocationForEvent(eventID, locationID);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean cancelTheEvent(int eventID) {
        return eventRepository.cancelTheEvent(eventID);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean publishTheEvent(int eventID, String eventDueDate, int locationID) {
        boolean status = true;
        status = status & eventRepository.publishTheEvent(eventID, eventDueDate, LocalDateTime.now().toString());
        status = status & eventLocationRepository.confirmLocationForEvent(eventID, locationID);
        return status;
    }

    @Override
    public double getCollectedRevenue(int eventID) {
        return transactionRepository.getAccumulatedBudget(eventID);
    }

    @Override
    public long getCancelledEventCount() {
        return eventRepository.getCancelledEventCount();
    }

    @Override
    public long getPendingEventCount() {
        return eventRepository.getPendingEventCount();
    }

    @Override
    public long getInProgressEventCount() {
        return eventRepository.getInProgressEventCount();
    }

    @Override
    public List<EventDTO> getPendingEvents() {
        List<Event> pendingEvents = eventRepository.getPendingEvents();
        if (null != pendingEvents) {
            List<EventDTO> eventDTOS = dtoConvertor.convertToDTOList(pendingEvents);
            return getEventListDetails(eventDTOS);
        }
        return null;
    }

    @Override
    public List<EventDTO> getInProgressEvents() {
        List<Event> inProgressEvents = eventRepository.getInProgressEvents();

        if (null != inProgressEvents) {
            List<EventDTO> list = dtoConvertor.convertToDTOList(inProgressEvents);
            return getEventListDetails(list);
        }
        return null;
    }

    private List<EventDTO> getEventListDetails(List<EventDTO> list) {
        System.out.println("getting event details-------------------------------------");
        for (int i = 0; i < list.size(); i++) {
            EventDTO eventDTO = list.get(i);
            int eventID = eventDTO.getId();

            List<ArtistDTO> artistList = artistService.getArtistList(eventID);
            eventDTO.setArtistDTOS(artistList);

            List<LocationDTO> locationDTOS = venueService.getVenueList(eventID);
            eventDTO.setLocationDTOS(locationDTOS);

            List<TicketPackageDTO> ticketPackageDTOS = ticketPackageService.getTicketPackagesByEvent(eventID);
            eventDTO.setTicketPackageDTOS(ticketPackageDTOS);

            list.set(i, eventDTO);
        }

        System.out.println("event dtos: " + list);
        return list;
    }
}
