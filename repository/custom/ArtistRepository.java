package com.base245.apeevent.repository.custom;

        import com.base245.apeevent.entity.Artist;
        import com.base245.apeevent.repository.CrudRepository;

/**
 * @author Dilini Peiris on 6/11/2019
 */


public interface ArtistRepository extends CrudRepository<Artist, Integer> {

    boolean confirmArtist(int artistID);
}
