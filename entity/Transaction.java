package com.base245.apeevent.entity;

import javax.persistence.*;
import java.util.Calendar;

/**
 *   @author Amesh M Jayaweera as 1/26/2019
 */

@Entity
@Table(name = "TRANSACTION")
public class Transaction extends SuperEntity{
    @Id
    @Column(name = "TRANS_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int transId;
    @Column(name = "FROM_AMOUNT", nullable = false)
    private double from_amount;
    @Column(name = "FROM_PARTY",nullable = false)
    private String from;
    @Column(name = "TO_AMOUNT", nullable = false)
    private double to_amount;
    @Column(name = "TO_PARTY",nullable = false)
    private String to;
    @Column(name = "DATE_TIME",nullable = false)
    @Temporal(TemporalType.DATE)
    private Calendar datetime;

    @ManyToOne
    @JoinColumn(name = "fk_event")
    private Event event;

    public Transaction(int transId, double from_amount, String from, double to_amount, String to, Calendar datetime) {
        this.transId = transId;
        this.from_amount = from_amount;
        this.to_amount = to_amount;
        this.from = from;
        this.to = to;
        this.datetime = datetime;
    }

    public Transaction(double from_amount, String from, double to_amount, String to, Calendar datetime) {
        this.from_amount = from_amount;
        this.to_amount = to_amount;
        this.from = from;
        this.to = to;
        this.datetime = datetime;
    }
    public Transaction() {
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public int getTransId() {
        return transId;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public double getFrom_amount() {
        return from_amount;
    }

    public void setFrom_amount(double from_amount) {
        this.from_amount = from_amount;
    }

    public double getTo_amount() {
        return to_amount;
    }

    public void setTo_amount(double to_amount) {
        this.to_amount = to_amount;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public Calendar getDate() {
        return datetime;
    }

    public void setDate(Calendar datetime) {
        this.datetime = datetime;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public void setDatetime(Calendar datetime) {
        this.datetime = datetime;
    }

    public String getTo() {
        return to;
    }

    public Calendar getDatetime() {
        return datetime;
    }
}
