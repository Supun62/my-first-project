package com.base245.apeevent.service.custom.impl;

import com.base245.apeevent.dto.BankAccountDTO;
import com.base245.apeevent.entity.BankAccount;
import com.base245.apeevent.repository.custom.BankAccountRepository;
import com.base245.apeevent.service.custom.BankAccountService;
import com.base245.apeevent.service.util.convertor.EntityConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Dilini Peiris on 3/29/2019
 */
@Service
public class BankAccountServiceImpl implements BankAccountService {

    @Autowired
    BankAccountRepository bankAccountRepository;

    @Autowired
    EntityConvertor entityConvertor;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public boolean save(BankAccountDTO dto) {
        BankAccount bankAccount = (BankAccount) entityConvertor.convertToEntity(dto);
        return bankAccountRepository.save(bankAccount);
    }

    @Override
    public boolean update(BankAccountDTO dto) {
        return false;
    }

    @Override
    public boolean delete(String key) {
        return false;
    }

    @Override
    public BankAccountDTO findByID(String key) {
        return null;
    }

    @Override
    public BankAccountDTO findByName(String name) {
        return null;
    }

    @Override
    public List<BankAccountDTO> findAll() {
        return null;
    }
}
