package com.base245.apeevent.main;

import com.base245.apeevent.service.util.ImagePath;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.io.File;
import java.util.Properties;

@Configuration
@PropertySource("classpath:application.properties")
@EnableScheduling
@EnableTransactionManagement
public class WebRootConfig {

    @Autowired
    Environment environment;

    @Bean
    public LocalSessionFactoryBean sessionFactory(DataSource dataSource) {
        LocalSessionFactoryBean sfb = new LocalSessionFactoryBean();
        sfb.setDataSource(dataSource);
        sfb.setPackagesToScan("com.base245.apeevent.entity");
        sfb.setHibernateProperties(hibernateProperties());
        System.out.println("database connection made====================");
        return sfb;
    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClassName(environment.getRequiredProperty("javax.persistence.jdbc.driver"));
        ds.setUrl(environment.getRequiredProperty("javax.persistence.jdbc.url"));
        ds.setUsername(environment.getRequiredProperty("javax.persistence.jdbc.user"));
        ds.setPassword(environment.getRequiredProperty("javax.persistence.jdbc.password"));
        return ds;
    }

    @Bean
    public Properties hibernateProperties() {
        Properties hb = new Properties();
        hb.put("hibernate.dialect", environment.getRequiredProperty("hibernate.dialect"));
        hb.put("hibernate.show_sql", environment.getRequiredProperty("hibernate.show_sql"));
        hb.put("hibernate.hbm2ddl.auto", environment.getRequiredProperty("hibernate.hbm2ddl.auto"));
        return hb;
    }

    @Bean
    public PlatformTransactionManager transactionManager(SessionFactory sessionFactory) {
        return new HibernateTransactionManager(sessionFactory);
    }

    @Bean(name = "mailSender")
    public JavaMailSenderImpl javaMailService() {
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
        javaMailSender.setHost("smtp.gmail.com");
        javaMailSender.setPort(587);
        javaMailSender.setProtocol("smtp");
        javaMailSender.setUsername(environment.getRequiredProperty("email"));
        javaMailSender.setPassword("Please#Start@Coding!");
        Properties mailProperties = new Properties();
        mailProperties.put("mail.smtp.auth", "true");
        mailProperties.put("mail.smtp.starttls.enable", "true");
        mailProperties.put("mail.smtp.debug", "true");
        mailProperties.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        javaMailSender.setJavaMailProperties(mailProperties);
        return javaMailSender;
    }

    @Bean
    public ImagePath imagePath() {
        String image_saving_path = environment.getRequiredProperty("image_saving_path");
        String profile_images = environment.getRequiredProperty("profile_images");
        String location_images = environment.getRequiredProperty("location_images");
        String artist_images = environment.getRequiredProperty("artist_images");
        String event_images = environment.getRequiredProperty("event_images");
        boolean isProfile = new File(image_saving_path + profile_images).mkdirs();
        boolean isLoc = new File(image_saving_path + location_images).mkdirs();
        boolean isArtist = new File(image_saving_path + artist_images).mkdirs();
        boolean isEvent = new File(image_saving_path + event_images).mkdirs();

        if (isArtist && isEvent && isLoc && isProfile)
            System.out.println("created all image folders===========================================");
        System.out.println("all folders were not created...........................");
        return new ImagePath(image_saving_path, event_images, profile_images, artist_images, location_images);
    }
}
