package com.base245.apeevent.service.custom;

import com.base245.apeevent.dto.ArtistDTO;
import com.base245.apeevent.service.SuperService;

import java.util.List;

/**
 * @author Dilini Peiris on 6/11/2019
 */
public interface ArtistService extends SuperService<ArtistDTO, Integer> {

    List<ArtistDTO> getArtistList(int eventID);

    boolean confirmArtist(int artistID);

    boolean rejectArtist(int artistID);
}
