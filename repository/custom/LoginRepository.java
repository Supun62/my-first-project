package com.base245.apeevent.repository.custom;

import com.base245.apeevent.entity.Login;
import com.base245.apeevent.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Dilini Peiris
 */


@Transactional
public interface LoginRepository extends CrudRepository<Login,String> {

    Login checkLogin(Login login);

    boolean checkUsernameExists(String username);

    List<String> getUserTypeAndId(String username);

    Login loadUserByUsername(String username);

    boolean checkUserStatus(String username);
}
