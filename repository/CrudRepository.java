package com.base245.apeevent.repository;

import com.base245.apeevent.entity.SuperEntity;

import java.util.List;

/**
 * @author Dilini Peiris
 */
public interface CrudRepository<T extends SuperEntity,ID> extends SuperRepository {
    boolean save(T entity);

    boolean update(T entity,ID key);

    boolean delete(T entity,ID key);

    T findByID(ID key);

    List<T> findAll();

    long getCount();

}
