package com.base245.apeevent.repository.custom.impl;

import com.base245.apeevent.entity.Login;
import com.base245.apeevent.entity.VerificationToken;
import com.base245.apeevent.repository.CrudRepositoryImpl;
import com.base245.apeevent.repository.custom.LoginRepository;
import com.base245.apeevent.repository.custom.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author Hkp Maheshika
 */
@Repository
public class TokenRepositoryImpl extends CrudRepositoryImpl<VerificationToken, String> implements TokenRepository {

    @Autowired
    LoginRepository loginRepository;

    @Override
    public boolean checkVerificationToken(String token) {

        VerificationToken byID = findByID(token);
        System.out.println(byID);

        if (null != byID) {
            Login login = byID.getLogin();
            System.out.println(login);
            login.setStatus(true);
            System.out.println(login);
            return loginRepository.update(login, login.getUsername());
        } else
            return false;
    }
}
