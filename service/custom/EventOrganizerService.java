package com.base245.apeevent.service.custom;

import com.base245.apeevent.dto.UserDTO;

/**
 * @author Dilini Peiris
 */

public interface EventOrganizerService {
    void setBankAccountForeignKey(int id,String accountNo);
    UserDTO getEventOrganizerByName(String name);
}
