package com.base245.apeevent.dto;

import com.base245.apeevent.service.util.enum_types.EventFacilitator_Status;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Supun Rangana as 1/23/2019
 */
public class ArtistDTO extends SuperDTO {

    private String name;
    private int id;
    private String artistImage;
    private String imageFile;
    private EventFacilitator_Status state;
    private int no_of_votes;

    public ArtistDTO() {
    }

    public ArtistDTO(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public ArtistDTO(String name, int id, String artistImage) {
        this.name = name;
        this.id = id;
        this.artistImage = artistImage;
    }

    public ArtistDTO(String name, int id, String artistImage, int no_of_votes) {
        this.name = name;
        this.id = id;
        this.artistImage = artistImage;
        this.no_of_votes = no_of_votes;
    }


    public ArtistDTO(String name, int id, String artistImage, String imageFile, int no_of_votes, String artist_status) {
        this.name = name;
        this.id = id;
        this.artistImage = artistImage;
        this.imageFile = imageFile;
        this.no_of_votes = no_of_votes;
    }

    public ArtistDTO(String name, int id, String artistImage, String imageFile, EventFacilitator_Status state) {
        this.name = name;
        this.id = id;
        this.artistImage = artistImage;
        this.imageFile = imageFile;
        this.state = state;
    }

    public ArtistDTO(int id, String name, String artistImage, String imageFile, EventFacilitator_Status state, int no_of_votes) {
        this.name = name;
        this.id = id;
        this.artistImage = artistImage;
        this.imageFile = imageFile;
        this.state = state;
        this.no_of_votes = no_of_votes;
    }

    public ArtistDTO(String name, int id, String artistImage, EventFacilitator_Status state) {
        this.name = name;
        this.id = id;
        this.artistImage = artistImage;
        this.state = state;
    }

    public ArtistDTO(int id, String name, String artistImage, EventFacilitator_Status state, int no_of_votes) {
        this.name = name;
        this.id = id;
        this.artistImage = artistImage;
        this.state = state;
        this.no_of_votes = no_of_votes;
    }

    public String getImageFile() {
        return imageFile;
    }

    public void setImageFile(String imageFile) {
        this.imageFile = imageFile;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getArtistImage() {
        return artistImage;
    }

    public void setArtistImage(String artistImage) {
        this.artistImage = artistImage;
    }

    public EventFacilitator_Status getstate() {
        return state;
    }

    public void setstate(EventFacilitator_Status state) {
        this.state = state;
    }

    @JsonIgnore
    public EventFacilitator_Status getState() {
        return state;
    }

    public void setState(EventFacilitator_Status state) {
        this.state = state;
    }

    public int getNo_of_votes() {
        return no_of_votes;
    }

    public void setNo_of_votes(int no_of_votes) {
        this.no_of_votes = no_of_votes;
    }

}
