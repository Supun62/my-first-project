package com.base245.apeevent.entity;

import javax.persistence.EntityResult;
import javax.persistence.SqlResultSetMapping;

@SqlResultSetMapping(name = "TicketPurchaseResults",
        entities = {
                @EntityResult(entityClass = TicketPackage.class),
                @EntityResult(entityClass = Ticket.class)
        })
public class TicketPurchaseCount extends SuperEntity {
    String ticketPackageName;
    int purchaseCount;

    public TicketPurchaseCount(String ticketPackageName, int purchaseCount) {
        this.ticketPackageName = ticketPackageName;
        this.purchaseCount = purchaseCount;
    }

    public TicketPurchaseCount() {
    }

    public String getTicketPackageName() {
        return ticketPackageName;
    }

    public int getPurchaseCount() {
        return purchaseCount;
    }

}
