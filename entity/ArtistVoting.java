package com.base245.apeevent.entity;


import javax.persistence.*;

@Entity
@Table(name = "ARTIST_VOTING")
public class ArtistVoting extends SuperEntity {

    @EmbeddedId
    private ArtistVotingID artistVotingID;

    @Column(name = "vote",nullable = false)
    private boolean vote;

    public ArtistVoting() {
    }

    public ArtistVoting(ArtistVotingID artistVotingID) {
        this.artistVotingID = artistVotingID;
    }

    public ArtistVoting(ArtistVotingID artistVotingID, boolean vote) {
        this.artistVotingID = artistVotingID;
        this.vote = vote;
    }

    public ArtistVotingID getArtistVotingID() {
        return artistVotingID;
    }

    public void setArtistVotingID(ArtistVotingID artistVotingID) {
        this.artistVotingID = artistVotingID;
    }

    public boolean getvote() {
        return vote;
    }

    public void setvote(boolean vote) {
        this.vote = vote;
    }
}
