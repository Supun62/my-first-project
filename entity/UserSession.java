package com.base245.apeevent.entity;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "USER_SESSION")
@DynamicUpdate
public class UserSession extends SuperEntity {
    @Id
    @Column(name = "USERNAME", nullable = false)
    private String username;
    @Column(name = "LOG_IN_TIME", nullable = false)
    private String log_in_time;
    @Column(name = "LOG_OUT_TIME", nullable = false)
    private String log_out_time;


    public UserSession(String username, String log_in_time, String log_out_time) {
        this.username = username;
        this.log_in_time = log_in_time;
        this.log_out_time = log_out_time;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLog_in_time() {
        return log_in_time;
    }

    public void setLog_in_time(String log_in_time) {
        this.log_in_time = log_in_time;
    }

    public String getLog_out_time() {
        return log_out_time;
    }

    public void setLog_out_time(String log_out_time) {
        this.log_out_time = log_out_time;
    }
}
