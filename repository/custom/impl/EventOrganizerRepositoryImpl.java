package com.base245.apeevent.repository.custom.impl;

import com.base245.apeevent.entity.EventOrganizer;
import com.base245.apeevent.repository.CrudRepositoryImpl;
import com.base245.apeevent.repository.custom.EventOrganizerRepository;
import com.base245.apeevent.repository.session.SessionManager;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dilini Peiris
 */
@Repository
public class EventOrganizerRepositoryImpl extends CrudRepositoryImpl<EventOrganizer,Integer> implements EventOrganizerRepository  {
    @Autowired
    private SessionManager sessionManager;

    @Override
    public void updateBankAccountKey(int id, String accountNo) {
        try {
            sessionManager.getSession().createNativeQuery("update event_organizer set fk_bankAccount='" + accountNo + "' where ID='" + id + "';").executeUpdate();



        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    @Override
    public int findIdByEmail(String email) {
        NativeQuery nativeQuery = sessionManager.getSession().createNativeQuery("select id from Event_Organizer where email='" + email + "';");
        int data  = 0;
        try{
            data = (int) nativeQuery.getSingleResult();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        return data;
    }

    @Override
    public EventOrganizer findByName(String name) {
        NativeQuery<EventOrganizer> nativeQuery = sessionManager.getSession().createNativeQuery("select * from Event_Organizer where name='" + name + "';", EventOrganizer.class);
        List<EventOrganizer> data = nativeQuery.getResultList();

        if ((null != data) && !data.isEmpty()) {
            return data.get(0);
        }
        return null;
    }

    @Override
    public EventOrganizer findUserByEmail(String email) {
        NativeQuery<EventOrganizer> nativeQuery = sessionManager.getSession().createNativeQuery("select * from event_organizer where EMAIL='" + email + "';", EventOrganizer.class);
        List<EventOrganizer> data = new ArrayList<>();
        data = nativeQuery.getResultList();
        if ((null != data) && !data.isEmpty()) {
            return data.get(0);
        }
        return null;
    }

    @Override
    public EventOrganizer getEventOrganizerByName(String name) {
        NativeQuery<EventOrganizer> nativeQuery = sessionManager.getSession().createNativeQuery("select * from event_organizer where NAME='" + name + "';", EventOrganizer.class);
        List<EventOrganizer> data = new ArrayList<>();
        data = nativeQuery.getResultList();
        if ((null != data) && !data.isEmpty()) {
            return data.get(0);
        }
        return null;
    }
}
