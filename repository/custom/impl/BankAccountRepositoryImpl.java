package com.base245.apeevent.repository.custom.impl;

import com.base245.apeevent.entity.BankAccount;
import com.base245.apeevent.repository.CrudRepositoryImpl;
import com.base245.apeevent.repository.custom.BankAccountRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Dilini Peiris on 3/29/2019
 */
@Repository
public class BankAccountRepositoryImpl extends CrudRepositoryImpl<BankAccount, String> implements BankAccountRepository {
}
