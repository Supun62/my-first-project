package com.base245.apeevent.dto;

import java.util.List;

public class CustomDTO {

    private EventDTO eventDTO;
    private List<ArtistDTO> artistDTOS;
    private List<LocationDTO> locationDTOS;
    private List<TicketPackageDTO> ticketPackageDTOS;

    public CustomDTO() {
    }

    public CustomDTO(EventDTO eventDTO, List<ArtistDTO> artistDTOS, List<LocationDTO> locationDTOS, List<TicketPackageDTO> ticketPackageDTOS) {
        this.eventDTO = eventDTO;
        this.artistDTOS = artistDTOS;
        this.locationDTOS = locationDTOS;
        this.ticketPackageDTOS = ticketPackageDTOS;
    }

    public EventDTO getEventDTO() {
        return eventDTO;
    }

    public void setEventDTO(EventDTO eventDTO) {
        this.eventDTO = eventDTO;
    }

    public List<ArtistDTO> getArtistDTOS() {
        return artistDTOS;
    }

    public void setArtistDTOS(List<ArtistDTO> artistDTOS) {
        this.artistDTOS = artistDTOS;
    }

    public List<LocationDTO> getLocationDTOS() {
        return locationDTOS;
    }

    public void setLocationDTOS(List<LocationDTO> locationDTOS) {
        this.locationDTOS = locationDTOS;
    }

    public List<TicketPackageDTO> getTicketPackageDTOS() {
        return ticketPackageDTOS;
    }

    public void setTicketPackageDTOS(List<TicketPackageDTO> ticketPackageDTOS) {
        this.ticketPackageDTOS = ticketPackageDTOS;
    }
}
