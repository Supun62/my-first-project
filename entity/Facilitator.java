package com.base245.apeevent.entity;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

/**
 *   @author Amesh M Jayaweera as 1/26/2019
 *   edited by Dilini Peiris
 */

@Entity
@Table(name = "FACILITATOR",
uniqueConstraints = {@UniqueConstraint(columnNames = {"EMAIL","CONTACT_NO"})}
)
@DynamicUpdate
public class Facilitator extends UserEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID",nullable = false)
    private int id;
    @Column(name = "NAME",nullable = false)
    private String name;
    @Column(name = "OCCUPATION")
    private String occupation;
    @Column(name = "ORGANIZATION")
    private String organization;
    @Column(name = "EMAIL",nullable = false)
    private String email;
    @Column(name = "CONTACT_NO")
    private String contactNo;
    @Column(name = "PROFILE_IMAGE")
    private String profileImage;

    public Facilitator(int id, String name, String occupation, String organization, String email, String contactNo, String profileImage) {
        this.id = id;
        this.name = name;
        this.occupation = occupation;
        this.organization = organization;
        this.email = email;
        this.contactNo = contactNo;
        this.profileImage = profileImage;
    }

    public Facilitator() {
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }
}
