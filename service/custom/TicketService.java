package com.base245.apeevent.service.custom;

import com.base245.apeevent.dto.TicketDTO;
import com.base245.apeevent.dto.VoteCountDTO;
import com.base245.apeevent.service.SuperService;

import java.util.List;

/**
 * @author Dilini Peiris on 6/11/2019
 */
public interface TicketService extends SuperService<TicketDTO, String> {

    boolean saveAllTickets(List<TicketDTO> ticketDTOS);

    List<VoteCountDTO> getTicketPurchases(int eventID);
}
