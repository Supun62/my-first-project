package com.base245.apeevent.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class LocationVotingID implements Serializable {

    @Column(name = "LOCATION_VOTING_ID")
    private long locationVotingID;

    private int eventID;

    private int locationID;

    private String username;

    public LocationVotingID() {
    }

    public LocationVotingID(long locationVotingID, int eventID, int locationID, String username) {
        this.locationVotingID = locationVotingID;
        this.eventID = eventID;
        this.locationID = locationID;
        this.username = username;
    }



    public long getLocationVotingID() {
        return locationVotingID;
    }

    public void setLocationVotingID(long locationVotingID) {
        this.locationVotingID = locationVotingID;
    }

    public int getEventID() {
        return eventID;
    }

    public void setEventID(int eventID) {
        this.eventID = eventID;
    }

    public int getLocationID() {
        return locationID;
    }

    public void setLocationID(int locationID) {
        this.locationID = locationID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LocationVotingID)) return false;
        LocationVotingID that = (LocationVotingID) o;
        return getLocationVotingID() == that.getLocationVotingID() &&
                getEventID() == that.getEventID() &&
                getLocationID() == that.getLocationID() &&
                Objects.equals(getUsername(), that.getUsername());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLocationVotingID(), getEventID(), getLocationID(), getUsername());
    }
}
