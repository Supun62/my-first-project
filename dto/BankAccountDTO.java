package com.base245.apeevent.dto;

/**
 *  @author Supun Rangana as 1/23/2019
 */
public class BankAccountDTO extends SuperDTO{
    private String accNo;
    private String bankName;
    private String branch;
    private String swiftCode;

    public BankAccountDTO(String accNo, String bankName, String branch, String swiftCode) {
        this.accNo = accNo;
        this.bankName = bankName;
        this.branch = branch;
        this.swiftCode = swiftCode;
    }

    public BankAccountDTO() {
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }

    public String getSwiftCode() {
        return swiftCode;
    }

    public void setSwiftCode(String swiftCode) {
        this.swiftCode = swiftCode;
    }
}
