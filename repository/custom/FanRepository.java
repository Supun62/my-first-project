package com.base245.apeevent.repository.custom;

import com.base245.apeevent.entity.Fan;
import com.base245.apeevent.repository.CrudRepository;

/**
 * @author Dilini Peiris
 */


public interface FanRepository extends CrudRepository<Fan,Integer> {
    int findIdByEmail(String email);

    Fan findUserByEmail(String email);
}
