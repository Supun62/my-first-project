package com.base245.apeevent.entity;

import javax.persistence.*;


/**
 *   @author Amesh M Jayaweera as 1/27/2019
 */
@Entity
@Table(name = "FAN_ARTIST")
@AssociationOverrides({
        @AssociationOverride(name = "primaryKey.fan",
                joinColumns = @JoinColumn(name = "FAN_ID")),
        @AssociationOverride(name = "primaryKey.artist",
                joinColumns = @JoinColumn(name = "ARTIST_ID"))
})
public class FanArtist extends SuperEntity{
    @EmbeddedId
    private FanArtistID primaryKey = new FanArtistID();

    @Column(name = "DATE")
    private String String;

    public FanArtist(FanArtistID primaryKey, String String) {
        this.primaryKey = primaryKey;
        this.String = String;
    }

    public FanArtist() {
    }

    public FanArtistID getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(FanArtistID primaryKey) {
        this.primaryKey = primaryKey;
    }

    public String getString() {
        return String;
    }

    public void setString(String String) {
        this.String = String;
    }
}
