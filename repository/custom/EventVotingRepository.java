package com.base245.apeevent.repository.custom;

import com.base245.apeevent.entity.EventVoting;
import com.base245.apeevent.entity.EventVotingID;
import com.base245.apeevent.repository.CrudRepository;


public interface EventVotingRepository extends CrudRepository<EventVoting, EventVotingID> {

    EventVoting getEventVotingByID(int eventID,String username);

}
