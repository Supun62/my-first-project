package com.base245.apeevent.service.util.qr;

import com.base245.apeevent.dto.EventDTO;
import com.base245.apeevent.dto.TicketDTO;
import com.base245.apeevent.service.custom.EventService;
import com.base245.apeevent.service.util.enum_types.ImageType;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

@Service
public class QRService {

    @Autowired
    EventService eventService;

    private String image_path = "";

    public String getImage_path() {
        return image_path;
    }

    public List<String> generateQRcode(List<TicketDTO> tickets) {
        List<String> qrPaths = new ArrayList<>();
        for (TicketDTO ticketDTO : tickets) {
            System.out.println("------------------------" + ticketDTO);
            try {
                String ticketDetails = ticketDTO.toString();
                setImagePath(ticketDetails);

                EventDTO byName = eventService.findByName(ticketDTO.getEventName());
                System.out.println(byName);
                String preOrderClosingDate = byName.getPreOrderClosingDate();
                System.out.println(preOrderClosingDate + "----------------closing date");

                if (Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30")).before(preOrderClosingDate)) {
                    ticketDetails += "_gift-included";
                }
                generateQRCodeImage(ticketDetails, 500, 500);
                qrPaths.add(getImage_path());
            } catch (WriterException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return qrPaths;
    }

    public boolean deleteQRFiles(List<String> paths) {
        boolean delete = true;
        for (String path : paths) {
            File file = new File(path);
            delete = file.delete() && delete;
        }
        return delete;
    }

    private void setImagePath(String imageName) {
        System.out.println("---------------------------------------------------------" + imageName);
        String projectPath = System.getProperty("user.dir");
        image_path = projectPath + "\\" + imageName + "."+ImageType.PNG.toString().toLowerCase();
    }

    //save QR image in the above given path
    private void generateQRCodeImage(String imageData, int width, int height)
            throws WriterException, IOException {
        System.out.println("generating qr code");
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(imageData, BarcodeFormat.QR_CODE, width, height);
        Path path = FileSystems.getDefault().getPath(getImage_path());
        System.out.println(path);
        MatrixToImageWriter.writeToPath(bitMatrix, ImageType.PNG.toString(), path);
    }


}


