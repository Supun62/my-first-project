package com.base245.apeevent.repository.custom;

import com.base245.apeevent.entity.EventLocation;
import com.base245.apeevent.entity.EventLocationID;
import com.base245.apeevent.repository.CrudRepository;

import java.util.List;

/**
 * @author Dilini Peiris on 6/11/2019
 */

public interface EventLocationRepository extends CrudRepository<EventLocation, EventLocationID> {
    List<EventLocation> getEventLocation(int eventID);

    int updateVotes(int vote,int eventID,int locationID);

    int getLocationVoteCount(int eventID,int locationID);

    boolean removeLocation(int eventID, int locationID);

    boolean confirmLocationForEvent(int eventID, int locationID);

}
