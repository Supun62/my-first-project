package com.base245.apeevent.service.util.payment;

import com.base245.apeevent.dto.CartDTO;
import com.base245.apeevent.dto.EventDTO;

/**
 * @author Dilini Peiris on 6/11/2019
 */
public interface PaymentService {

    boolean recordTicketBought(CartDTO cartDTO);

    boolean confirmPaymentOnPublishEvent(EventDTO eventDTO);

    boolean returnPaymentOnEventCancellation(EventDTO eventDTO);
}
