package com.base245.apeevent.service.custom.impl;

import com.base245.apeevent.dto.LoginDTO;
import com.base245.apeevent.dto.OnRegistrationEventDTO;
import com.base245.apeevent.dto.UserDTO;
import com.base245.apeevent.dto.UserRegisterDTO;
import com.base245.apeevent.entity.*;
import com.base245.apeevent.repository.custom.*;
import com.base245.apeevent.service.custom.UserService;
import com.base245.apeevent.service.util.ImageConverter;
import com.base245.apeevent.service.util.ImagePath;
import com.base245.apeevent.service.util.convertor.DtoConvertor;
import com.base245.apeevent.service.util.convertor.EntityConvertor;
import com.base245.apeevent.service.util.email.EmailService;
import com.base245.apeevent.service.util.email.listener.RegistrationEventPublisher;
import com.base245.apeevent.service.util.enum_types.ImageType;
import com.base245.apeevent.service.util.enum_types.UserTypes;
import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Dilini Peiris
 */

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class UserServiceImpl implements UserService {

    @Autowired
    LoginRepository loginRepository;

    @Autowired
    EntityConvertor entityConvertor;

    @Autowired
    DtoConvertor dtoConvertor;

    @Autowired
    FacilitatorRepository facilitatorRepository;

    @Autowired
    FanRepository fanRepository;

    @Autowired
    EventOrganizerRepository eventOrganizerRepository;

    @Autowired
    RegistrationEventPublisher registrationEventPublisher;

    @Autowired
    EmailService emailService;

    @Autowired
    UserSessionRepository userSessionRepository;

    @Autowired
    ImagePath imagePath;

    @Autowired
    Environment environment;

    PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public UserDTO logIn(LoginDTO dto) {
        Login login = (Login) entityConvertor.convertToEntity(dto);
        login.setPassword(dto.getPassword());
        Login data = loginRepository.checkLogin(login);
        System.out.println(login);

        UserDTO userDTO = null;
        if (null != data) {
            if (null != data.getEventOrganizer()) {
                EventOrganizer byID = eventOrganizerRepository.findByID(data.getEventOrganizer().getId());
                userDTO = (UserDTO) dtoConvertor.convertToDTO(byID);
            } else if (null != data.getFacilitator()) {
                Facilitator byID = facilitatorRepository.findByID(data.getFacilitator().getId());
                userDTO = (UserDTO) dtoConvertor.convertToDTO(byID);
            } else if (null != data.getFan()) {
                System.out.println(data.getFan().getId());
                Fan byID = fanRepository.findByID(data.getFan().getId());
                userDTO = (UserDTO) dtoConvertor.convertToDTO(byID);
            }

            return setImage(userDTO);
        }

        return null;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public boolean registerUser(UserRegisterDTO userRegisterDTO, String appUrl) {
        UserDTO userDTO = userRegisterDTO.getUserDTO();
        userDTO.setName(userDTO.getName().trim());
        LoginDTO loginDTO = userRegisterDTO.getLoginDTO();
        UserTypes userType = userDTO.getUserType();

        UserEntity user = (UserEntity) entityConvertor.convertToEntity(userDTO);
        Login login = (Login) entityConvertor.convertToEntity(loginDTO);

        login.setCreatedDateTime(LocalDateTime.now());
        login.setPassword(passwordEncoder.encode(loginDTO.getPassword()));

        boolean status = false;
        switch (userType) {
            case FAN: {
                System.out.println("casting to fan");
                Fan fan = (Fan) user;
                fan.setProfileImage(userDTO.getName() + "profileImage" + "." + ImageType.JPEG.toString().toLowerCase());
                status = fanRepository.save(fan);
                login.setFan(fan);
                break;
            }
            case FACILITATOR: {
                System.out.println("casting to facilitator");
                Facilitator facilitator = (Facilitator) user;
                status = facilitatorRepository.save(facilitator);
                login.setFacilitator(facilitator);
                break;
            }
            case EVENT_ORGANIZER: {
                System.out.println("casting to event organizer");
                EventOrganizer eventOrganizer = (EventOrganizer) user;
                status = eventOrganizerRepository.save(eventOrganizer);
                login.setEventOrganizer(eventOrganizer);
                break;
            }
        }

        status = status && (loginRepository.save(login));

        if (status) {  // error occurred
            try {
                registrationEventPublisher.publishRegistrationEvent(new OnRegistrationEventDTO(appUrl, userRegisterDTO));
            } catch (Exception e) {
                System.out.println("Exception " + e.getMessage());
            }
        }
        return status;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public boolean updateUserDetails(UserDTO userDTO) {
        UserTypes userType = userDTO.getUserType();
        userDTO.setProfileImage(userDTO.getName() + "profileImage" + "." + ImageType.JPEG.toString().toLowerCase());
        System.out.println("PROFILE IMAGE ==========" + userDTO.getProfileImage());

        try {
            String s = userDTO.getImage();
            byte[] bytes = new byte[0];
            try {
                bytes = Base64.decode(s);
            } catch (Base64DecodingException e) {
                e.printStackTrace();
            }
            String pathURL = imagePath.getProfileImagePath() + userDTO.getProfileImage();
            Path path = Paths.get(pathURL);
            System.out.println("==========================path writable? " + Files.isWritable(path));
            Files.write(path, bytes);

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error ===================== " + e.getMessage());
        }


        boolean update = false;
        switch (userType) {

            case FAN: {
                Fan fan = (Fan) entityConvertor.convertToEntity(userDTO);
                update = fanRepository.update(fan, fan.getId());
                break;
            }
            case EVENT_ORGANIZER: {
                EventOrganizer organizer = (EventOrganizer) entityConvertor.convertToEntity(userDTO);
                update = eventOrganizerRepository.update(organizer, organizer.getId());
                break;
            }
            case FACILITATOR: {
                Facilitator facilitator = (Facilitator) entityConvertor.convertToEntity(userDTO);
                update = facilitatorRepository.update(facilitator, facilitator.getId());
                break;
            }
        }
        return update;
    }


    @Override
    public int getUserID(UserTypes userType, String email) {
        int id = 0;
        switch (userType) {

            case FAN: {
                id = fanRepository.findIdByEmail(email);
                break;
            }
            case EVENT_ORGANIZER: {
                id = eventOrganizerRepository.findIdByEmail(email);
                break;
            }
            case FACILITATOR: {
                id = facilitatorRepository.findIdByEmail(email);
                break;
            }
        }
        return id;
    }

    @Override
    public boolean checkUsernameExists(String username) {
        return loginRepository.checkUsernameExists(username);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public boolean delete(Integer key) {
        return false;
    }

    @Override
    public UserDTO findByName(String name) {
        return null;
    }

    @Override
    public List<UserDTO> findAll() {
        return null;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public boolean logOutTime(String username) {
        return userSessionRepository.updateLogOutTime(username);
    }

    @Override
    public UserDTO getUserDetailsByEmail(UserTypes userType, String email) {

        UserDTO userDTO = new UserDTO();

        switch (userType) {

            case FAN: {
                Fan fan = (Fan) fanRepository.findUserByEmail(email);
                userDTO = (UserDTO) dtoConvertor.convertToDTO(fan);
                break;
            }
            case EVENT_ORGANIZER: {
                EventOrganizer eventOrganizer = (EventOrganizer) eventOrganizerRepository.findUserByEmail(email);
                userDTO = (UserDTO) dtoConvertor.convertToDTO(eventOrganizer);
                break;
            }
            case FACILITATOR: {
                Facilitator facilitator = (Facilitator) facilitatorRepository.findUserByEmail(email);
                userDTO = (UserDTO) dtoConvertor.convertToDTO(facilitator);
                break;
            }
        }

        return userDTO;
    }

    @Override
    public List<String> getUserDetailsV2(String username) {

        List<String> list = new ArrayList<>();
        list = loginRepository.getUserTypeAndId(username);
        return list;
    }

    @Override
    public UserDTO getUserDetailsByID(UserTypes userType, int id) {
        UserDTO userDTO = new UserDTO();

        switch (userType) {

            case FAN: {
                Fan fan = (Fan) fanRepository.findByID(id);
                userDTO = (UserDTO) dtoConvertor.convertToDTO(fan);
                break;
            }
            case EVENT_ORGANIZER: {
                EventOrganizer eventOrganizer = (EventOrganizer) eventOrganizerRepository.findByID(id);
                userDTO = (UserDTO) dtoConvertor.convertToDTO(eventOrganizer);
                break;
            }
            case FACILITATOR: {
                Facilitator facilitator = (Facilitator) facilitatorRepository.findByID(id);
                userDTO = (UserDTO) dtoConvertor.convertToDTO(facilitator);
                break;
            }
        }
        return setImage(userDTO);
    }

    private UserDTO setImage(UserDTO userDTO) {
        BufferedImage bImage = null;
        try {
            System.out.println("----------------------------" + userDTO.getProfileImage());
            String path = imagePath.getProfileImagePath() + userDTO.getProfileImage();
            System.out.println("----------------------------path = " + path);
            bImage = ImageIO.read(new File(path));
            userDTO.setImage(ImageConverter.convertBufferedImageToString(bImage));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("ERROR ======== " + e.getMessage());
        }
        return userDTO;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public boolean logInFirstTime(String username) {
        UserSession userSession = new UserSession(username, LocalDateTime.now().toString(), "LOGGED");
        return userSessionRepository.save(userSession);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public boolean logInTime(String username) {
        return userSessionRepository.updateLogInTime(username);
    }

    @Override
    public boolean checkUserStatus(String username) {
        return loginRepository.checkUserStatus(username);
    }
}
