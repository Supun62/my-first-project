package com.base245.apeevent.repository.custom;

import com.base245.apeevent.entity.EventOrganizer;
import com.base245.apeevent.repository.CrudRepository;


/**
 * @author Dilini Peiris
 */


public interface EventOrganizerRepository extends CrudRepository<EventOrganizer,Integer> {
    int findIdByEmail(String email);
    void updateBankAccountKey(int id,String accountNo);

    EventOrganizer findUserByEmail(String email);
    EventOrganizer getEventOrganizerByName(String name);

}
