package com.base245.apeevent.repository.custom.impl;

import com.base245.apeevent.entity.Location;
import com.base245.apeevent.repository.CrudRepositoryImpl;
import com.base245.apeevent.repository.custom.LocationRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Dilini Peiris on 6/11/2019
 */
@Repository
public class LocationRepositoryImpl extends CrudRepositoryImpl<Location, Integer> implements LocationRepository {

}
