package com.base245.apeevent.service.custom.impl;

import com.base245.apeevent.dto.LocationDTO;
import com.base245.apeevent.entity.EventLocation;
import com.base245.apeevent.entity.Location;
import com.base245.apeevent.repository.custom.EventLocationRepository;
import com.base245.apeevent.repository.custom.LocationRepository;
import com.base245.apeevent.service.custom.VenueService;
import com.base245.apeevent.service.util.convertor.DtoConvertor;
import com.base245.apeevent.service.util.convertor.EntityConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Dilini Peiris on 6/11/2019
 */

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class VenueServiceImpl implements VenueService {

    @Autowired
    LocationRepository locationRepository;

    @Autowired
    EntityConvertor entityConvertor;

    @Autowired
    DtoConvertor dtoConvertor;

    @Autowired
    private EventLocationRepository eventLocationRepository;


    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public boolean save(LocationDTO dto) {
        Location location = (Location) entityConvertor.convertToEntity(dto);
        return locationRepository.save(location);
    }

    @Override
    public boolean update(LocationDTO dto) {
        return false;
    }

    @Override
    public boolean delete(Integer key) {
        return false;
    }

    @Override
    public LocationDTO findByID(Integer key) {
        return null;
    }

    @Override
    public LocationDTO findByName(String name) {
        return null;
    }

    @Override
    public List<LocationDTO> findAll() {
        List<Location> all = locationRepository.findAll();
        List<LocationDTO> list = dtoConvertor.convertToDTOList(all);
        System.out.println(list);
        return list;
    }

    @Override
    public List<LocationDTO> getVenueList(int eventID) {
        List<EventLocation> eventLocations = eventLocationRepository.getEventLocation(eventID);
        System.out.println("event-venues: ------------------------------------------------" + eventLocations);
        List<LocationDTO> locationDTOS = null;
        if (null != eventLocations && 0 != eventLocations.size()) {
            locationDTOS = dtoConvertor.convertToDTOList(eventLocations);
        }
        System.out.println("location dtos ============" + locationDTOS);
        return locationDTOS;
    }
}
