package com.base245.apeevent.dto;

import java.util.List;

/**
 * @author Dilini Peiris on 6/11/2019
 */
public class CartDTO {
    List<CartItemDTO> items;
    private UserDTO userDTO;

    public CartDTO() {
    }

    public CartDTO(UserDTO userDTO, List<CartItemDTO> items) {
        this.userDTO = userDTO;
        this.items = items;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public List<CartItemDTO> getItems() {
        return items;
    }

    public void setItems(List<CartItemDTO> items) {
        this.items = items;
    }
}
