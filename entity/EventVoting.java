package com.base245.apeevent.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "EVENT_VOTING")
public class EventVoting  extends SuperEntity{

    @EmbeddedId
    private EventVotingID eventVotingID;

    @Column(name = "vote",nullable = false)
    private boolean vote;

    public EventVoting() {
    }

    public EventVoting(EventVotingID eventVotingID) {
        this.eventVotingID = eventVotingID;
    }

    public EventVoting(EventVotingID eventVotingID, boolean vote) {
        this.eventVotingID = eventVotingID;
        this.vote = vote;
    }

    public EventVotingID getEventVotingID() {
        return eventVotingID;
    }

    public void setEventVotingID(EventVotingID eventVotingID) {
        this.eventVotingID = eventVotingID;
    }

    public boolean getVote() {
        return vote;
    }

    public void setVote(boolean vote) {
        this.vote = vote;
    }

}
