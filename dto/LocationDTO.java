package com.base245.apeevent.dto;

import com.base245.apeevent.service.util.enum_types.EventFacilitator_Status;

/**
 *  @author Supun Rangana as 1/23/2019
 */
public class LocationDTO extends SuperDTO{

    private int id;
    private String name;
    private double latitude;
    private double longitude;
    private EventFacilitator_Status status;

    public LocationDTO() {
    }

    public LocationDTO(int id, String name, double latitude, double longitude) {
        this.id = id;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public LocationDTO(int id, String name, EventFacilitator_Status status) {
        this.id = id;
        this.name = name;
        this.status = status;
    }

    public EventFacilitator_Status getStatus() {
        return status;
    }

    public void setStatus(EventFacilitator_Status status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
