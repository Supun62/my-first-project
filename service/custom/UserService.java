package com.base245.apeevent.service.custom;

import com.base245.apeevent.dto.LoginDTO;
import com.base245.apeevent.dto.UserDTO;
import com.base245.apeevent.dto.UserRegisterDTO;
import com.base245.apeevent.service.SuperService;
import com.base245.apeevent.service.util.enum_types.UserTypes;

import java.util.List;

/**
 * @author Dilini Peiris
 */

public interface UserService extends SuperService<UserDTO,Integer> {

    UserDTO logIn(LoginDTO dto);

    @Override
    default boolean save(UserDTO dto) {
        throw new RuntimeException("This method is not implemented for this Service. " +
                "\n Please use the method registerUser(UserRegisterDTO, String) instead");
    }

    @Override
    default boolean update(UserDTO dto) {
        throw new RuntimeException("This method is not implemented for this Service." +
                "\n Please use the method updateUserDetails(UserRegisterDTO) instead");
    }

    @Override
    default UserDTO findByID(Integer key) {
        throw new RuntimeException("This method is not implemented for this Service." +
                "\n Please use the method findByID(Integer key) for each user type entity instead");
    }

    boolean updateUserDetails(UserDTO userDTO);

    boolean registerUser(UserRegisterDTO userRegisterDTO,String appUrl);

    int getUserID(UserTypes userTypes,String email);

    boolean checkUsernameExists(String username);

    boolean logOutTime(String username);

    UserDTO getUserDetailsByEmail(UserTypes userType, String email);

    List<String> getUserDetailsV2(String username);

    UserDTO getUserDetailsByID(UserTypes userType, int id);

    boolean logInFirstTime(String username);

    boolean logInTime(String username);

    boolean checkUserStatus(String username);
}
