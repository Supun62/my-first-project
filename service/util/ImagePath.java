package com.base245.apeevent.service.util;

import org.springframework.stereotype.Component;

@Component
public class ImagePath {
    String baseFolderPath;
    String eventImagePath;
    String profileImagePath;
    String artistImagePath;
    String locationImagePath;

    public ImagePath(String baseFolderPath, String eventImagePath, String profileImagePath, String artistImagePath, String locationImagePath) {
        this.baseFolderPath = baseFolderPath;
        this.eventImagePath = baseFolderPath + eventImagePath;
        this.profileImagePath = baseFolderPath + profileImagePath;
        this.artistImagePath = baseFolderPath + artistImagePath;
        this.locationImagePath = baseFolderPath + locationImagePath;
    }

    public ImagePath() {
    }

    public String getBaseFolderPath() {
        return baseFolderPath;
    }

    public String getEventImagePath() {
        return eventImagePath;
    }

    public String getProfileImagePath() {
        return profileImagePath;
    }

    public String getArtistImagePath() {
        return artistImagePath;
    }

    public String getLocationImagePath() {
        return locationImagePath;
    }
}
