package com.base245.apeevent.repository.custom;

import com.base245.apeevent.entity.Event;
import com.base245.apeevent.repository.CrudRepository;

import java.util.List;


/**
 * @author Supun Rangana as 2/23/2019
 */


public interface EventRepository extends CrudRepository <Event, Integer> {

    int updateVotes(int vote,int eventID);

    List<Event> getEventsByEventOrganizer(int eventOrganizerID);

    boolean updateEventName(int eventID, String eventName, String eventOrganizer);

    boolean updateEventPreOrderClosingDate(int eventID, String preOrderClosingDate);

    boolean updateEventTargetBudget(int eventID, double targetBudget);

    boolean cancelTheEvent(int eventID);

    boolean publishTheEvent(int eventID, String eventDueDate, String publishedDate);

    boolean allowEventByAdmin(int eventId);

    long getCancelledEventCount();

    long getPendingEventCount();

    long getInProgressEventCount();

    List<Event> getPendingEvents();

    List<Event> getInProgressEvents();
}
