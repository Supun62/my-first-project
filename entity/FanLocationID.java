package com.base245.apeevent.entity;


import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 *   @author Amesh M Jayaweera as 1/27/2019
 */
@Embeddable
public class FanLocationID implements Serializable {
    @ManyToOne
    private Fan fan;

    @ManyToOne
    private Location location;

    public FanLocationID(Fan fan, Location location) {
        this.fan = fan;
        this.location = location;
    }

    public FanLocationID() {
    }

    public Fan getFan() {
        return fan;
    }

    public void setFan(Fan fan) {
        this.fan = fan;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }


}
