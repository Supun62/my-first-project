package com.base245.apeevent.repository.custom;

import com.base245.apeevent.entity.EventArtist;
import com.base245.apeevent.entity.EventArtistID;
import com.base245.apeevent.repository.CrudRepository;

import java.util.List;

/**
 * @author Dilini Peiris on 6/11/2019
 */


public interface EventArtistRepository extends CrudRepository<EventArtist, EventArtistID> {

    List<EventArtist> getEventArtist(int eventID);

    int updateVotes(int vote,int eventID,int artistID);

    int getArtistVoteCount(int eventID,int artistID);

    boolean removeArtistFromEvent(int eventID, int artistID);

    boolean confirmArtistForEvent(int eventID, int artistID, String status);

    String getEventArtistStatus(int eventID, int artistID);

}
