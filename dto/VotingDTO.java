package com.base245.apeevent.dto;

import com.base245.apeevent.service.util.enum_types.Voting_Type;

public class VotingDTO {

    private int eventID;
    private String username;
    private boolean vote;
    private Voting_Type voting_type;
    private int artistID;
    private int locationID;

    public VotingDTO() {
    }

    public VotingDTO(int eventID, String username, boolean vote, Voting_Type voting_type, int artistID, int locationID) {
        this.eventID = eventID;
        this.username = username;
        this.vote = vote;
        this.voting_type = voting_type;
        this.artistID = artistID;
        this.locationID = locationID;
    }


    public VotingDTO(int eventID, int artistID) {
        this.eventID = eventID;
        this.artistID = artistID;
    }

    public int getEventID() {
        return eventID;
    }

    public void setEventID(int eventID) {
        this.eventID = eventID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean getVotes() {
        return vote;
    }

    public void setVotes(boolean votes) {
        this.vote = votes;
    }

    public Voting_Type getVoting_type() {
        return voting_type;
    }

    public void setVoting_type(Voting_Type voting_type) {
        this.voting_type = voting_type;
    }

    public boolean isVote() {
        return vote;
    }

    public void setVote(boolean vote) {
        this.vote = vote;
    }

    public int getArtistID() {
        return artistID;
    }

    public void setArtistID(int artistID) {
        this.artistID = artistID;
    }

    public int getLocationID() {
        return locationID;
    }

    public void setLocationID(int locationID) {
        this.locationID = locationID;
    }
}
