package com.base245.apeevent.service;

import com.base245.apeevent.dto.SuperDTO;

import java.util.List;

/**
 * @author Dilini Peiris
 */

public interface SuperService<T extends SuperDTO,ID> {

    boolean save(T dto);

    boolean update(T dto);

    boolean delete(ID key);

    T findByID(ID key);

    T findByName(String name);

    List<T> findAll();

    default long getCount(){
        return findAll().size();
    }
}
