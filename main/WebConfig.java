package com.base245.apeevent.main;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@ComponentScan("com.base245.apeevent")
public class WebConfig {

}
