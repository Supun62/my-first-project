package com.base245.apeevent.repository.custom;

import com.base245.apeevent.entity.LocationVoting;
import com.base245.apeevent.entity.LocationVotingID;
import com.base245.apeevent.repository.CrudRepository;


public interface LocationVotingRepository extends CrudRepository<LocationVoting, LocationVotingID> {

    LocationVoting getLocationVotingByID(int eventID,int locationID,String username);

}
