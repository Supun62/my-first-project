package com.base245.apeevent.repository.custom.impl;

import com.base245.apeevent.entity.Artist;
import com.base245.apeevent.repository.CrudRepositoryImpl;
import com.base245.apeevent.repository.custom.ArtistRepository;
import com.base245.apeevent.repository.session.SessionManager;
import com.base245.apeevent.service.util.enum_types.EventFacilitator_Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author Dilini Peiris on 6/11/2019
 */
@Repository
public class ArtistRepositoryImpl extends CrudRepositoryImpl<Artist, Integer> implements ArtistRepository {

    @Autowired
    SessionManager sessionManager;

    @Override
    public boolean confirmArtist(int artistID) {
        int s = sessionManager.getSession().createNativeQuery("update artist SET STATUS='" + EventFacilitator_Status.CONFIRMED.toString() + "' WHERE  ID='" + artistID + "';").executeUpdate();
        return s > 0;
    }
}
