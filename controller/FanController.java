package com.base245.apeevent.controller;

import com.base245.apeevent.dto.CardDTO;
import com.base245.apeevent.dto.CartDTO;
import com.base245.apeevent.service.custom.CardService;
import com.base245.apeevent.service.util.payment.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author Dilini Peiris
 */

@RestController
@RequestMapping("api/v1/fan")
@CrossOrigin
public class FanController {

    @Autowired
    CardService cardService;

    @Autowired
    PaymentService paymentService;

    @PostMapping("/card")
    public boolean addCardDetails(@RequestBody CardDTO cardDTO) {
        return cardService.save(cardDTO);
    }

    @PostMapping("/buy")
    public boolean recordTicketBought(@RequestBody CartDTO cartDTO) {
        return paymentService.recordTicketBought(cartDTO);
    }
}
