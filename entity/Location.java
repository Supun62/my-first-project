package com.base245.apeevent.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 *   @author Amesh M Jayaweera as 1/26/2019
 *   edited by Dilini Peiris
 */
@Entity
@Table(name = "LOCATION")
public class Location extends SuperEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID",nullable = false)
    private int id;
    @Column(name = "NAME",nullable = false)
    private String name;
    @Column(name = "LATITUDE", nullable = false)
    private double latitude;
    @Column(name = "LONGITUDE", nullable = false)
    private double longitude;

    @OneToMany(mappedBy = "primaryKey.location")
    private List<FanLocation> fanLocation = new ArrayList<>();

    @OneToMany(mappedBy = "primaryKey.location")
    private List<EventLocation> eventLocations = new ArrayList<>();

    public List<FanLocation> getFanLocation() {
        return fanLocation;
    }

    public void setFanLocation(List<FanLocation> fanLocation) {
        this.fanLocation = fanLocation;
    }

    public Location(int id, String name, double latitude, double longitude) {
        this.id = id;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Location() {
    }

    public List<EventLocation> getEventLocations() {
        return eventLocations;
    }

    public void setEventLocations(List<EventLocation> eventLocations) {
        this.eventLocations = eventLocations;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
