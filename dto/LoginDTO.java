package com.base245.apeevent.dto;

/**
 *  @author Supun Rangana as 1/23/2019
 */
public class LoginDTO extends SuperDTO{

    private String username;
    private String password;

    public LoginDTO() {
    }

    public LoginDTO(String username) {
        this.username = username;
    }

    public LoginDTO(String username, String password) {
        this.username = username;
        this.password = password;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
