package com.base245.apeevent.service.custom.impl;

import com.base245.apeevent.dto.ArtistDTO;
import com.base245.apeevent.entity.Artist;
import com.base245.apeevent.entity.EventArtist;
import com.base245.apeevent.repository.custom.ArtistRepository;
import com.base245.apeevent.repository.custom.EventArtistRepository;
import com.base245.apeevent.service.custom.ArtistService;
import com.base245.apeevent.service.util.convertor.DtoConvertor;
import com.base245.apeevent.service.util.convertor.EntityConvertor;
import com.base245.apeevent.service.util.enum_types.EventFacilitator_Status;
import com.base245.apeevent.service.util.enum_types.ImageType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Dilini Peiris on 6/11/2019
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ArtistServiceImpl implements ArtistService {

    @Autowired
    ArtistRepository artistRepository;

    @Autowired
    EntityConvertor entityConvertor;

    @Autowired
    DtoConvertor dtoConvertor;

    @Autowired
    private EventArtistRepository eventArtistRepository;

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public boolean save(ArtistDTO dto) {
        dto.setName(dto.getName().trim());
        dto.setArtistImage(dto.getName() + "." + ImageType.JPEG.toString().toLowerCase());
        Artist artist = (Artist) entityConvertor.convertToEntity(dto);
        artist.setImage(dto.getArtistImage());
        artist.setStatus(EventFacilitator_Status.PENDING.toString());
        return artistRepository.save(artist);
    }

    @Override
    public boolean update(ArtistDTO dto) {
        return false;
    }

    @Override
    public boolean delete(Integer key) {
        return false;
    }

    @Override
    public ArtistDTO findByID(Integer key) {
        return null;
    }

    @Override
    public ArtistDTO findByName(String name) {
        return null;
    }

    @Override
    public List<ArtistDTO> findAll() {
        List<Artist> all = artistRepository.findAll();
        List<ArtistDTO> list = dtoConvertor.convertToDTOList(all);
        System.out.println(list);
        return list;
    }

    @Override
    public List<ArtistDTO> getArtistList(int eventID) {
        List<EventArtist> eventArtists = eventArtistRepository.getEventArtist(eventID);
        System.out.println("event-artists: ------------------------" + eventArtists);
        List<ArtistDTO> artistDTOS = null;
        if (null != eventArtists && 0 != eventArtists.size()) {
            System.out.println("artists are not null");
            System.out.println(eventArtists.size());
            artistDTOS = dtoConvertor.convertToDTOList(eventArtists);
        }
        System.out.println("event-artists================================" + artistDTOS);
        return artistDTOS;
    }

    @Override
    public boolean confirmArtist(int artistID) {
        return artistRepository.confirmArtist(artistID);
    }

    @Override
    public boolean rejectArtist(int artistID) {
        Artist byID = artistRepository.findByID(artistID);
        return artistRepository.delete(byID, artistID);
    }
}
