package com.base245.apeevent.service.util.enum_types;

public enum Payment_Party {
    FAN, PAYHERE, ORGANIZER, APP_OWNER
}
