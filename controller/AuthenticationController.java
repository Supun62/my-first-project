package com.base245.apeevent.controller;

import com.base245.apeevent.dto.LoginDTO;
import com.base245.apeevent.dto.UserDTO;
import com.base245.apeevent.dto.UserRegisterDTO;
import com.base245.apeevent.security.AppConstant;
import com.base245.apeevent.security.TokenUtils;
import com.base245.apeevent.security.model.AuthenticationRequest;
import com.base245.apeevent.security.model.AuthenticationResponse;
import com.base245.apeevent.security.model.SpringSecurityUser;
import com.base245.apeevent.service.custom.UserService;
import com.base245.apeevent.service.util.email.EmailCrudService;
import com.base245.apeevent.service.util.email.EmailService;
import com.base245.apeevent.service.util.enum_types.UserTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/v1/authentication")
public class AuthenticationController {

    @Autowired
    UserService userService;

    @Autowired
    EmailService emailService;

    @Autowired
    EmailCrudService emailCrudService;

    // authentication
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenUtils tokenUtils;

    @Autowired
    private UserDetailsService userDetailsService;

    @PostMapping(value = "/auth")
    public ResponseEntity<?> authenticationRequest(@RequestBody AuthenticationRequest authenticationRequest)
            throws AuthenticationException {

        // Perform the authentication
        Authentication authentication = this.authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        authenticationRequest.getUsername(),
                        authenticationRequest.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);

        // Reload password post-authentication so we can generate token
        UserDetails userDetails = this.userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        String token = this.tokenUtils.generateToken(userDetails);

        // Return the token
        return ResponseEntity.ok(new AuthenticationResponse(token));
    }

    @RequestMapping(value = "refresh", method = RequestMethod.GET)
    public ResponseEntity<?> authenticationRequest(HttpServletRequest request) {
        String token = request.getHeader(AppConstant.tokenHeader);
        String username = this.tokenUtils.getUsernameFromToken(token);
        SpringSecurityUser user = (SpringSecurityUser) this.userDetailsService.loadUserByUsername(username);
        if (this.tokenUtils.canTokenBeRefreshed(token, user.getLastPasswordReset())) {
            String refreshedToken = this.tokenUtils.refreshToken(token);
            return ResponseEntity.ok(new AuthenticationResponse(refreshedToken));
        } else {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @PostMapping("/login")
    public UserDTO login(@RequestBody LoginDTO loginDTO) {
        System.out.println(loginDTO);
        System.out.println(loginDTO.getUsername() + loginDTO.getPassword());
        return userService.logIn(loginDTO);

    }

    @PostMapping("/register")
    public boolean register(@RequestBody UserRegisterDTO userRegisterDTO, WebRequest request) {
        System.out.println(userRegisterDTO.getLoginDTO());
        System.out.println(userRegisterDTO.getUserDTO());
        return userService.registerUser(userRegisterDTO, request.getContextPath());
    }

    @GetMapping("/confirmRegistration")
    public String confirmRegistration(@RequestParam("token") String token) {
        boolean enableUser = emailService.enableUser(token);
        if (enableUser)
            return "Your email has been confirmed. \nPlease log in using the credentials.";
        else
            return "There was an error confirming your Email address. \n Please contact Support - nextlevel.b17@gmail.com";
    }

    @PostMapping("/forgot")
    public boolean forgotPassword(@RequestParam("username") String username) {
        return emailService.sendForgotPasswordEmail(username);
    }

    @GetMapping("/test")
    public String print() {
        System.out.println("==============================test================================");
        return "Testing----------------";
    }

    @PostMapping("/update")
    public boolean update(@RequestBody UserDTO userDTO) {
        return userService.updateUserDetails(userDTO);
    }

    @PostMapping("/getUserID")
    public int checkEmailExists(@RequestParam("userType") UserTypes userType, @RequestParam("email") String email) {
        return userService.getUserID(userType, email);
    }

    @PostMapping("/checkUsernameExists")
    public boolean checkUsernameExists(@RequestParam("username") String username) {
        return userService.checkUsernameExists(username);
    }

    @PostMapping("/getUserDetailsByEmail")
    public UserDTO getUserDetailsByEmail(@RequestParam("userType") UserTypes userType, @RequestParam("email") String email) {
        return userService.getUserDetailsByEmail(userType, email);
    }

    @PostMapping("/getUserDetailsV2")
    public List<String> getUserDetailsV2(@RequestParam("username") String username) {
        return userService.getUserDetailsV2(username);
    }

    @PostMapping("/getUserDetailsByID")
    public UserDTO getUserDetailsByID(@RequestParam("userType") UserTypes userType, @RequestParam("id") int id) {
        return userService.getUserDetailsByID(userType, id);
    }

    @PostMapping("/logOutTime")
    public boolean logOutTime(@RequestParam("username") String username) {
        return userService.logOutTime(username);
    }

    @PostMapping("/logInTime")
    public boolean logInTime(@RequestParam("username") String username) {
        return userService.logInTime(username);
    }

    @PostMapping("/logInFirstTime")
    public boolean logInFirstTime(@RequestParam("username") String username) {
        return userService.logInFirstTime(username);
    }

    @PostMapping("/loadEvents")
    public List<String> loadEvents(@RequestParam("username") String username) {
        return emailCrudService.getEvents(username);
    }

    @PostMapping("/customizedMail")
    public boolean customizedMail(@RequestParam("eventName") String eventName, @RequestParam("message") String message, @RequestParam("subject") String subject) {
        return emailService.sendCustomizedEmail(eventName, message, subject);
    }

    @PostMapping("/user/status")
    public boolean checkUserStatus(@RequestParam("username") String username) {
        return userService.checkUserStatus(username);
    }

    @PostMapping("/resend")
    public boolean resendVerification(@RequestParam("username") String username) {
        return emailService.resendVerificationEmail(username);
    }
}
