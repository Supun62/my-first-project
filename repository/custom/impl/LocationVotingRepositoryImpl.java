package com.base245.apeevent.repository.custom.impl;

import com.base245.apeevent.entity.LocationVoting;
import com.base245.apeevent.entity.LocationVotingID;
import com.base245.apeevent.repository.CrudRepositoryImpl;
import com.base245.apeevent.repository.custom.LocationVotingRepository;
import com.base245.apeevent.repository.session.SessionManager;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class LocationVotingRepositoryImpl extends CrudRepositoryImpl<LocationVoting, LocationVotingID> implements LocationVotingRepository {

    @Autowired
    private SessionManager sessionManager;

    @Override
    public LocationVoting getLocationVotingByID(int eventID, int locationID, String username) {
        NativeQuery<LocationVoting> nativeQuery = sessionManager.getSession().createNativeQuery("select * from Location_Voting where eventID='" + eventID + "' AND locationID='" + locationID +"' AND username='" + username  +"' ;", LocationVoting.class);

        List<LocationVoting> data = nativeQuery.getResultList();

        if ((null != data) && !data.isEmpty()) {
            return data.get(0);
        }
        return null;
    }
}
