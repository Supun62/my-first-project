package com.base245.apeevent.service.util;

import com.base245.apeevent.service.util.enum_types.ImageType;
import com.sun.org.apache.xml.internal.security.utils.Base64;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ImageConverter {
    public static String convertBufferedImageToString(BufferedImage bImage) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(bImage, ImageType.JPEG.toString().toLowerCase(), bos);
        byte[] bytes = bos.toByteArray();
        return Base64.encode(bytes);
    }
}
