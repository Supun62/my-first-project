package com.base245.apeevent.dto;

/**
 * @author Dilini Peiris on 3/29/2019
 */
public class UserRegisterDTO extends SuperDTO {
    private UserDTO userDTO;
    private LoginDTO loginDTO;

    public UserRegisterDTO(UserDTO userDTO, LoginDTO loginDTO) {
        this.userDTO = userDTO;
        this.loginDTO = loginDTO;
    }

    public UserRegisterDTO() {
    }

    public LoginDTO getLoginDTO() {
        return loginDTO;
    }

    public void setLoginDTO(LoginDTO loginDTO) {
        this.loginDTO = loginDTO;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }
}
