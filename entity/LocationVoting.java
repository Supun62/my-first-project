package com.base245.apeevent.entity;


import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "LOCATION_VOTING")
public class LocationVoting  extends SuperEntity{

    @EmbeddedId
    private LocationVotingID locationVotingID;

    @Column(name = "vote")
    private boolean vote;


    public LocationVoting() {
    }

    public LocationVoting(LocationVotingID locationVotingID) {
        this.locationVotingID = locationVotingID;
    }

    public LocationVoting(LocationVotingID locationVotingID, boolean vote) {
        this.locationVotingID = locationVotingID;
        this.vote = vote;
    }

    public LocationVotingID getLocationVotingID() {
        return locationVotingID;
    }

    public void setLocationVotingID(LocationVotingID locationVotingID) {
        this.locationVotingID = locationVotingID;
    }

    public boolean getvote() {
        return vote;
    }

    public void setvote(boolean vote) {
        this.vote = vote;
    }
}
