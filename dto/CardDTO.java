package com.base245.apeevent.dto;

/**
 *  @author Supun Rangana as 1/23/2019
 */
public class CardDTO extends SuperDTO{

    private String cardNo;
    private String cardType;
    private int cvv;
    private String expiry;
    private String nameOnCard;

    public CardDTO(String cardNo, String cardType, int cvv, String expiry, String nameOnCard) {
        this.cardNo = cardNo;
        this.cardType = cardType;
        this.cvv = cvv;
        this.expiry = expiry;
        this.nameOnCard = nameOnCard;
    }

    public CardDTO() {
    }

    public String getCardType() {
        return cardType;
    }

    public String getCardNo() {
        return cardNo;
    }

    public int  getCvv() {
        return cvv;
    }

    public String getExpiry() {
        return expiry;
    }

    public String getNameOnCard() {
        return nameOnCard;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public void setCvv(int cvv) {
        this.cvv = cvv;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public void setNameOnCard(String nameOnCard) {
        this.nameOnCard = nameOnCard;
    }
}
