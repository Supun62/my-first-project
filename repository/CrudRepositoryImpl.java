package com.base245.apeevent.repository;

import com.base245.apeevent.entity.SuperEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;


public class CrudRepositoryImpl<T extends SuperEntity, ID extends Serializable> implements CrudRepository<T, ID> {

    @Autowired
    private SessionFactory sf;

    private Class<T> myEntity;


    public CrudRepositoryImpl() {
        myEntity = (Class<T>) (((ParameterizedType) (this.getClass().getGenericSuperclass())).getActualTypeArguments()[0]);
        System.out.println(myEntity.getName());
    }

    public Session getSession() {
        return sf.getCurrentSession();
    }

    @Override
    public boolean save(T entity) {
        System.out.println("crud repo saving-------------");
        getSession().persist(entity);
        return true;
    }

    @Override
    public boolean update(T entity, ID key) {
        System.out.println("crud repo updating-------------");
        try {
            getSession().update(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }

        T byID = findByID(key);
        System.out.println(byID);
        if (null != byID) {
            boolean equals = entity.equals(byID);
            System.out.println(equals);
            return equals;
        }
        return false;

    }

    @Override
    public boolean delete(T entity, ID key) {
        System.out.println("crud repo deleting-------------");
        getSession().delete(entity);
        T byID = findByID(key);
        return null == byID;
    }

    @Override
    public T findByID(ID key) {
        return getSession().get(myEntity, key);
    }

    @Override
    public List<T> findAll() {
        List<T> list = getSession().createQuery("FROM " + myEntity.getName()).list();
        System.out.println(list);
        return list;
    }

    @Override
    public long getCount() {
        long count = 0;
        Query query = getSession().createQuery("select count(*) from " + myEntity.getSimpleName());
        count = (long) query.iterate().next();
        return count;
    }
}
