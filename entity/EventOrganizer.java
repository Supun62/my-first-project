package com.base245.apeevent.entity;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 *   @author Amesh M Jayaweera as 1/26/2019
 *   edited by Dilini Peiris
 */
@Entity
@Table(name = "EVENT_ORGANIZER",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"EMAIL","CONTACT_NO"})}
)
@DynamicUpdate
public class EventOrganizer extends UserEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID",nullable = false)
    private int id;
    @Column(name = "NAME",nullable = false)
    private String name;
    @Column(name = "OCCUPATION")
    private String occupation;
    @Column(name = "ORGANIZATION")
    private String organization;
    @Column(name = "EMAIL",nullable = false)
    private String email;
    @Column(name = "CONTACT_NO")
    private String contact_no;
    @Column(name = "PROFILE_IMAGE")
    private String profileImage;

    @OneToOne
    @JoinColumn(name = "fk_bankAccount")
    private BankAccount bankAccount;

    @OneToMany(mappedBy = "eventOrganizer")
    private List<Event> event = new ArrayList<>();

    public EventOrganizer(int id, String name, String occupation, String organization, String email, String contact_no, String profileImage) {
        this.id = id;
        this.name = name;
        this.occupation = occupation;
        this.organization = organization;
        this.email = email;
        this.contact_no = contact_no;
        this.profileImage = profileImage;
    }


    public EventOrganizer() {
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public List<Event> getEvent() {
        return event;
    }

    public void setEvent(List<Event> event) {
        this.event = event;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }
}
