package com.base245.apeevent.repository.custom;

import com.base245.apeevent.entity.UserSession;
import com.base245.apeevent.repository.CrudRepository;


public interface UserSessionRepository extends CrudRepository<UserSession, Integer> {
    boolean updateLogOutTime(String username);

    boolean updateLogInTime(String username);
}
