package com.base245.apeevent.dto;


/**
 * @author Supun Rangana as 2/10/2019
 */
public class FanLocationDTO extends SuperDTO{
    private int fanId;
    private int locationId;
    private String votedDate;

    public FanLocationDTO(int fanId, int locationId, String votedDate) {
        this.fanId = fanId;
        this.locationId = locationId;
        this.votedDate = votedDate;
    }

    public FanLocationDTO() {
    }

    public int getFanId() {
        return fanId;
    }

    public void setFanId(int fanId) {
        this.fanId = fanId;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public String getVotedDate() {
        return votedDate;
    }

    public void setVotedDate(String votedDate) {
        this.votedDate = votedDate;
    }
}
