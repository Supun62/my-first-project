package com.base245.apeevent.service.util.payment;

import com.base245.apeevent.dto.*;
import com.base245.apeevent.entity.Event;
import com.base245.apeevent.entity.EventOrganizer;
import com.base245.apeevent.entity.Transaction;
import com.base245.apeevent.repository.custom.EventOrganizerRepository;
import com.base245.apeevent.repository.custom.EventRepository;
import com.base245.apeevent.repository.custom.FanRepository;
import com.base245.apeevent.repository.custom.TransactionRepository;
import com.base245.apeevent.service.custom.EventService;
import com.base245.apeevent.service.custom.TicketService;
import com.base245.apeevent.service.util.convertor.DtoConvertor;
import com.base245.apeevent.service.util.email.EmailService;
import com.base245.apeevent.service.util.enum_types.Payment_Party;
import com.base245.apeevent.service.util.qr.QRService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.*;

/**
 * @author Dilini Peiris on 6/11/2019
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    FanRepository fanRepository;

    @Autowired
    DtoConvertor dtoConvertor;

    @Autowired
    TicketService ticketService;

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    QRService qrService;

    @Autowired
    EmailService emailService;

    @Autowired
    EventService eventService;

    @Autowired
    EventOrganizerRepository eventOrganizerRepository;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public boolean recordTicketBought(CartDTO cartDTO) {
        UserDTO userDTO = cartDTO.getUserDTO();
        List<CartItemDTO> items = cartDTO.getItems();
        int id = userDTO.getId();
        boolean isCompleted = true;

        System.out.println("\n---------------------------cart size " + items.size());
        for (CartItemDTO cartItemDTO : items) {
            TicketPackageDTO ticketPackageDTO = cartItemDTO.getTicketPackageDTO();
            System.out.println("\n" + ticketPackageDTO);
            int qty = cartItemDTO.getQty();
            double fan_paid = 0;
            boolean ticketSave = true;
            Event event = (Event) eventRepository.findByName(ticketPackageDTO.getEventName());

            Calendar instance = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
            boolean isDonate = instance.before(event.getPreOrderClosingDate());

            List<TicketDTO> tickets = new ArrayList<>();

            for (int i = 0; i < qty; i++) {
                TicketDTO ticketDTO = new TicketDTO(event.getName(), ticketPackageDTO.getName(), id,
                        LocalDate.now());
                ticketDTO.setTicketNo(UUID.randomUUID().toString().substring(0, 8));
                tickets.add(ticketDTO);
                System.out.println(ticketDTO);
                ticketSave = ticketService.save(ticketDTO) && ticketSave;
            }

            if (ticketSave) {
                System.out.println("----------------tickets saved successfully");
                boolean transactionSave = true;

                if (isDonate) {
                    fan_paid += (ticketPackageDTO.getPrice() * (100 - ticketPackageDTO.getDiscount()) / 100) * qty;
                } else {
                    fan_paid += (ticketPackageDTO.getPrice() * qty);
                }

                double received = fan_paid - ((fan_paid * 3.9 / 100) + 39);

                Transaction transaction = new Transaction(fan_paid, Payment_Party.FAN.toString() + "-" + id, received,
                        Payment_Party.PAYHERE.toString(), Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30")));
                transaction.setEvent(event);
                transactionSave = transactionRepository.save(transaction) && transactionSave;

                if (transactionSave) {

                    System.out.println("----------------transaction saved successfully");
                    eventService.compareEventBudget(event.getId());
                    List<String> qrPaths = qrService.generateQRcode(tickets);

                    boolean isEmailSent = emailService.sendTicketPurchaseEmail(userDTO, cartItemDTO, fan_paid, qrPaths);
                    if (isEmailSent) {
                        System.out.println("------------email sent successfully");
                        boolean deleteQRFiles = qrService.deleteQRFiles(qrPaths);
                        if (!deleteQRFiles) {
                            System.out.println("qr not deleted successfully");
                            isCompleted = false && isCompleted;
                        }
                    } else {
                        System.out.println("email not sent successfully");
                        isCompleted = false && isCompleted;
                    }
                } else {
                    System.out.println("transactions not saved successfully");
                    isCompleted = false && isCompleted;
                }
            } else {
                System.out.println("tickets not saved successfully");
                isCompleted = false && isCompleted;
            }
        }
        return isCompleted;
    }

    @Override
    public boolean confirmPaymentOnPublishEvent(EventDTO eventDTO) {
        double accumulatedBudget = transactionRepository.getAccumulatedBudget(eventDTO.getName());
        Double forOrganizer = accumulatedBudget * (0.8);
        Double forDeveloper = accumulatedBudget - forOrganizer;

        Calendar time = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));

        EventOrganizer organizer = (EventOrganizer) eventOrganizerRepository.findByName(eventDTO.getOrganizerName());
        Transaction transactionOrganizer = new Transaction(forOrganizer, Payment_Party.PAYHERE.toString(), forOrganizer,
                Payment_Party.ORGANIZER.toString() + "-" + organizer.getId(), time);
        Transaction transactionDeveloper = new Transaction(forDeveloper, Payment_Party.PAYHERE.toString(), forDeveloper,
                Payment_Party.APP_OWNER.toString(), time);

//        ask payhere to transfer the amounts to event organizer's and app owner's accounts -> not featured in the payhere sandbox

        if (transactionRepository.save(transactionOrganizer)) {
            if (transactionRepository.save(transactionDeveloper)) {
                return emailService.publishEventEmail(eventDTO, forOrganizer, forDeveloper);
            } else
                return false;
        } else
            return false;
    }

    @Override
    public boolean returnPaymentOnEventCancellation(EventDTO eventDTO) {
        Event event = (Event) eventRepository.findByName(eventDTO.getName());
        List<Transaction> transactionsByEvent = transactionRepository.getTransactionsByEvent(event.getId());
        if (transactionsByEvent != null) {
            for (Transaction transaction : transactionsByEvent) {

//                ask payhere to refund

                Double returnAmount = transaction.getTo_amount() - ((transaction.getTo_amount() * 3.9 / 100) + 39);
                Transaction returnTrans = new Transaction(returnAmount, transaction.getTo(), returnAmount, transaction.getFrom(),
                        Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30")));
                if (transactionRepository.save(returnTrans)) {
                    if (emailService.cancelEventEmailToFan(eventDTO, returnAmount, Integer.parseInt(transaction.getFrom().split("-")[1])))
                        return false;
                }
            }
        }
        return false;
    }
}
