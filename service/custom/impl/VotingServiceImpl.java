package com.base245.apeevent.service.custom.impl;

import com.base245.apeevent.dto.VoteCountDTO;
import com.base245.apeevent.dto.VotingDTO;
import com.base245.apeevent.entity.*;
import com.base245.apeevent.repository.JoinQueryRepository;
import com.base245.apeevent.repository.custom.*;
import com.base245.apeevent.service.custom.EventService;
import com.base245.apeevent.service.custom.VotingService;
import com.base245.apeevent.service.util.convertor.DtoConvertor;
import com.base245.apeevent.service.util.enum_types.Voting_Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class VotingServiceImpl implements VotingService {

    @Autowired
    ArtistVotingRepository artistVotingRepository;

    @Autowired
    LocationVotingRepository locationVotingRepository;

    @Autowired
    EventVotingRepository eventVotingRepository;

    @Autowired
    EventArtistRepository eventArtistRepository;

    @Autowired
    EventLocationRepository eventLocationRepository;

    @Autowired
    EventService eventService;

    @Autowired
    JoinQueryRepository joinQueryRepository;

    @Autowired
    DtoConvertor dtoConvertor;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public int vote(VotingDTO votingDTO) {

        Voting_Type voting_type = votingDTO.getVoting_type();

        int eventID = votingDTO.getEventID();
        int artistID = votingDTO.getArtistID();
        int locationID = votingDTO.getLocationID();
        String username = votingDTO.getUsername();
        boolean vote = votingDTO.getVotes();

        int voteCount = 0;

        if (voting_type.equals(Voting_Type.EVENT)) {

            EventVoting eventVoting = eventVotingRepository.getEventVotingByID(eventID, username);

            if (eventVoting == null) {
                long count = eventVotingRepository.getCount();
                EventVotingID eventVotingID = new EventVotingID(++count, eventID, username);
                EventVoting eventVoting1 = new EventVoting(eventVotingID, vote);
                eventVotingRepository.save(eventVoting1);
            } else {
                eventVoting.setVote(vote);
                eventVotingRepository.update(eventVoting, eventVoting.getEventVotingID());
            }

            if (votingDTO.getVotes()) {
                voteCount = eventService.updatePostVote(1, votingDTO.getEventID());
            } else {
                voteCount = eventService.updatePostVote(-1, votingDTO.getEventID());
            }

        } else if (voting_type.equals(Voting_Type.ARTIST)) {

            ArtistVoting artistVoting = artistVotingRepository.getArtistVotingByID(eventID, artistID, username);


            if (artistVoting == null) {
                long count = artistVotingRepository.getCount();
                ArtistVotingID artistVotingID = new ArtistVotingID(++count, eventID, artistID, username);
                ArtistVoting artistVoting1 = new ArtistVoting(artistVotingID, vote);
                artistVotingRepository.save(artistVoting1);
            } else {
                artistVoting.setvote(vote);
                artistVotingRepository.update(artistVoting, artistVoting.getArtistVotingID());
            }


            if (votingDTO.getVotes()) {
                voteCount = eventService.updatePostArtistVote(1, votingDTO.getEventID(), votingDTO.getArtistID());
            } else {
                voteCount = eventService.updatePostArtistVote(-1, votingDTO.getEventID(), votingDTO.getArtistID());
            }

        } else if (voting_type.equals(Voting_Type.LOCATION)) {

            LocationVoting locationVoting = locationVotingRepository.getLocationVotingByID(eventID, locationID, username);

            if (locationVoting == null) {
                long count = locationVotingRepository.getCount();
                LocationVotingID locationVotingID = new LocationVotingID(++count, eventID, locationID, username);
                LocationVoting locationVoting1 = new LocationVoting(locationVotingID, vote);
                locationVotingRepository.save(locationVoting1);
            } else {
                locationVoting.setvote(vote);
                locationVotingRepository.update(locationVoting, locationVoting.getLocationVotingID());
            }

            if (votingDTO.getVotes()) {
                voteCount = eventService.updatePostLocationVote(1, votingDTO.getEventID(), votingDTO.getLocationID());
            } else {
                voteCount = eventService.updatePostLocationVote(1, votingDTO.getEventID(), votingDTO.getLocationID());
            }
        }


        return voteCount;

    }

    @Override
    public boolean getVoteStatus(VotingDTO votingDTO) {

        Voting_Type voting_type = votingDTO.getVoting_type();

        String username = votingDTO.getUsername();
        int eventID = votingDTO.getEventID();
        int artistID = votingDTO.getArtistID();
        int locationID = votingDTO.getLocationID();


        boolean status = false;

        if (voting_type.equals(Voting_Type.LOCATION)) {
            LocationVoting locationVoting = locationVotingRepository.getLocationVotingByID(eventID, locationID, username);
            if (locationVoting != null) {
                status = locationVoting.getvote();
            }
        } else if (voting_type.equals(Voting_Type.ARTIST)) {
            ArtistVoting artistVoting = artistVotingRepository.getArtistVotingByID(eventID, artistID, username);
            if (artistVoting != null) {
                status = artistVoting.getvote();
            }
        } else if (voting_type.equals(Voting_Type.EVENT)) {
            EventVoting eventVoting = eventVotingRepository.getEventVotingByID(eventID, username);
            if (eventVoting != null) {
                status = eventVoting.getVote();
            }
        }
        return status;
    }

    @Override
    public int getArtistVoteCount(VotingDTO votingDTO) {
        return eventArtistRepository.getArtistVoteCount(votingDTO.getEventID(), votingDTO.getArtistID());
    }

    @Override
    public int getLocationVoteCount(VotingDTO votingDTO) {
        return eventLocationRepository.getLocationVoteCount(votingDTO.getEventID(), votingDTO.getLocationID());
    }

    @Override
    public List<VoteCountDTO> getArtistVotes(int eventID) {
        List<ArtistVoteCount> artistVoting = joinQueryRepository.getArtistVoting(eventID);
        List<VoteCountDTO> list = dtoConvertor.convertToDTOList(artistVoting);
        return list;
    }

    @Override
    public List<VoteCountDTO> getLocationVotes(int eventID) {
        List<LocationVoteCount> locationVoting = joinQueryRepository.getLocationVoting(eventID);
        List<VoteCountDTO> list = dtoConvertor.convertToDTOList(locationVoting);
        return list;
    }
}
