package com.base245.apeevent.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *   @author Amesh M Jayaweera as 1/26/2019
 */
@Entity
@Table(name = "CARD")
public class Card extends SuperEntity{

    @Id
    @Column(name = "CARD_NO",nullable = false)
    private String card_no;
    @Column(name = "CARD_TYPE",nullable = false)
    private String card_type;
    @Column(name = "NAME_ON_CARD",nullable = false)
    private String name_on_card;
    @Column(name = "EXPIRY",nullable = false)
    private String expiry;
    @Column(name = "CVV",nullable = false)
    private int cvv;

    public Card() {
    }

    public Card(String card_no, String card_type, String name_on_card, String expiry, int cvv) {
        this.card_no = card_no;
        this.card_type = card_type;
        this.name_on_card = name_on_card;
        this.expiry = expiry;
        this.cvv = cvv;
    }

    public String getCard_no() {
        return card_no;
    }

    public void setCard_no(String card_no) {
        this.card_no = card_no;
    }

    public String getCard_type() {
        return card_type;
    }

    public void setCard_type(String card_type) {
        this.card_type = card_type;
    }

    public String getName_on_card() {
        return name_on_card;
    }

    public void setName_on_card(String name_on_card) {
        this.name_on_card = name_on_card;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public int getCvv() {
        return cvv;
    }

    public void setCvv(int cvv) {
        this.cvv = cvv;
    }
}
