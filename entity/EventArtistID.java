package com.base245.apeevent.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class EventArtistID implements Serializable {

    @Column(name = "EVENT_ARTIST_ID", nullable = false)
    private long eventArtistID;

    @ManyToOne
    private Event event;
    @ManyToOne
    private Artist artist;

    public EventArtistID() {
    }

    public EventArtistID(long eventArtistID, Event event, Artist artist) {
        this.eventArtistID = eventArtistID;
        this.event = event;
        this.artist = artist;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }
}
