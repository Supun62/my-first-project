package com.base245.apeevent.service.util.email;

import com.base245.apeevent.dto.CartItemDTO;
import com.base245.apeevent.dto.EventDTO;
import com.base245.apeevent.dto.OnRegistrationEventDTO;
import com.base245.apeevent.dto.UserDTO;

import java.util.List;

/**
 * @author Hkp Maheshika
 */
public interface EmailService {

    boolean sendVerificationEmail(OnRegistrationEventDTO dto);

    boolean enableUser(String token);

    boolean sendForgotPasswordEmail(String username);

    boolean sendCustomizedEmail(String eventName,String message,String subject);

    boolean sendTicketPurchaseEmail(UserDTO userDTO, CartItemDTO cartItemDTO, double transAmount, List<String> qrPaths);

    void sendBudgetAchievedEmail(EventDTO eventDTO, double accumulatedBudget);

    boolean publishEventEmail(EventDTO eventDTO, Double forOrganizer, Double forDeveloper);

    boolean cancelEventEmailToFan(EventDTO eventDTO, double returnAmount, int fanId);

    boolean cancelEventEmailToOrganizer(EventDTO eventDTO);

    boolean resendVerificationEmail(String username);
}