package com.base245.apeevent.repository;

import com.base245.apeevent.entity.SuperEntity;

/**
 * @author Dilini Peiris
 */
public interface SuperRepository<T extends SuperEntity> {

    default T findByName(String name) {
        return null;
    }
}
