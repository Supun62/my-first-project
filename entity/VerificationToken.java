package com.base245.apeevent.entity;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * @author Hkp Maheshika
 */
@Entity
@Table(name = "VERIFY_TOKEN")
public class VerificationToken extends SuperEntity {

    @Id
    @Column(name = "TOKEN", nullable = false)
    private String token;

    @Column(name = "CREATED_DATE", nullable = false)
    private LocalDate createdDate;

    @Column(name = "EXPIRY_DATE", nullable = false)
    private LocalDate expiryDate;

    @OneToOne
    @JoinColumn(name = "fk_username")
    private Login login;

    public VerificationToken(String token, LocalDate createdDate, LocalDate expiryDate, Login login) {
        this.token = token;
        this.createdDate = createdDate;
        this.expiryDate = expiryDate;
        this.login = login;
    }

    public VerificationToken() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDate getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDate expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }
}
