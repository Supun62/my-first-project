package com.base245.apeevent.service.util.email.listener;

import com.base245.apeevent.dto.OnRegistrationEventDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

/**
 * @author Hkp Maheshika
 */
@Component
public class RegistrationEventPublisher {
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    public void publishRegistrationEvent(OnRegistrationEventDTO dto) {
        System.out.println("Publishing...");
        OnRegistrationEventDTO onRegistrationEventDTO = new OnRegistrationEventDTO(this, dto.getAppUrl(), dto.getUserRegisterDTO());
        applicationEventPublisher.publishEvent(onRegistrationEventDTO);
    }
}
