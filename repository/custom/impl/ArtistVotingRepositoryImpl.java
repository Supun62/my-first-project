package com.base245.apeevent.repository.custom.impl;

import com.base245.apeevent.entity.ArtistVoting;
import com.base245.apeevent.entity.ArtistVotingID;
import com.base245.apeevent.repository.CrudRepositoryImpl;
import com.base245.apeevent.repository.custom.ArtistVotingRepository;
import com.base245.apeevent.repository.session.SessionManager;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ArtistVotingRepositoryImpl extends CrudRepositoryImpl<ArtistVoting, ArtistVotingID> implements ArtistVotingRepository {

    @Autowired
    private SessionManager sessionManager;

    @Override
    public ArtistVoting getArtistVotingByID(int eventID, int artistID, String username) {
        NativeQuery<ArtistVoting> nativeQuery = sessionManager.getSession().createNativeQuery("select * from artist_voting where eventID ='" + eventID + "' AND artistID ='" + artistID +"' AND username='" + username  +"' ;", ArtistVoting.class);

        List<ArtistVoting> data = nativeQuery.getResultList();

        if ((null != data) && !data.isEmpty()) {
            return data.get(0);
        }
        return null;
    }

}
