package com.base245.apeevent.service.util.convertor;

import com.base245.apeevent.dto.*;
import com.base245.apeevent.entity.*;
import com.base245.apeevent.service.util.enum_types.EventFacilitator_Status;
import com.base245.apeevent.service.util.enum_types.Event_Status;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static com.base245.apeevent.service.util.enum_types.UserTypes.*;


/**
 * @author Dilini Peiris
 * edited by Supun Rangana
 */

@Component
public class DtoConvertor {

    public SuperDTO convertToDTO(SuperEntity entity) {
        System.out.println("inside convert to dto");
        if (entity instanceof Login) {
            Login login = (Login) entity;
            return new LoginDTO(login.getUsername(), login.getPassword());
        } else if (entity instanceof Fan) {
            Fan fan = (Fan) entity;
            return new UserDTO(fan.getName(), fan.getEmail(), FAN, fan.getId(), fan.getDistrict(), fan.getProfileImage());
        } else if (entity instanceof Facilitator) {
            Facilitator facilitator = (Facilitator) entity;
            return new UserDTO(facilitator.getName(), facilitator.getEmail(), FACILITATOR, facilitator.getId(), facilitator.getOccupation(), facilitator.getOrganization(), facilitator.getContactNo(), facilitator.getProfileImage());
        } else if (entity instanceof EventOrganizer) {
            EventOrganizer eventOrganizer = (EventOrganizer) entity;
            return new UserDTO(eventOrganizer.getName(), eventOrganizer.getEmail(), EVENT_ORGANIZER, eventOrganizer.getId(), eventOrganizer.getOccupation(), eventOrganizer.getOrganization(), eventOrganizer.getContact_no(), eventOrganizer.getProfileImage());
        } else if (entity instanceof TicketPackage) {
            TicketPackage ticketPackage = (TicketPackage) entity;
            return new TicketPackageDTO(ticketPackage.getPackId(), ticketPackage.getPrice(), ticketPackage.getName(), ticketPackage.getDiscount(), ticketPackage.getDescription());
        } else if (entity instanceof Transaction) {
            Transaction transaction = (Transaction) entity;
            return new TransactionDTO(transaction.getTransId(), transaction.getFrom(), transaction.getFrom_amount(), transaction.getTo(), transaction.getTo_amount(), transaction.getDate());
        } else if (entity instanceof Card) {
            Card card = (Card) entity;
            return new CardDTO(card.getCard_no(), card.getCard_type(), card.getCvv(), card.getExpiry(), card.getName_on_card());
        } else if (entity instanceof BankAccount) {
            BankAccount bankAccount = (BankAccount) entity;
            return new BankAccountDTO(bankAccount.getAccountNo(), bankAccount.getBankName(), bankAccount.getBranch(), bankAccount.getSwiftCode());
        } else if (entity instanceof Event) {
            Event event = (Event) entity;
            return new EventDTO(event.getId(), event.getName(), Event_Status.valueOf(event.getStatus()), event.getPreOrderClosingDate(), event.getCreatedDateTime(), event.getTarget_budget(), ((Event) entity).getEventOrganizer().getName(), event.getImage(), ((Event) entity).getNoOfVotes(), event.getEventDueDate(), event.getPublishedDateTime());
            //            conversions of associate entities
        } else if (entity instanceof Ticket) {
            Ticket ticket = (Ticket) entity;
            return new TicketDTO(ticket.getTicketNo(), ticket.getPurchaseDate());
        } else if (entity instanceof FanLocation) {
            FanLocation fanLocation = (FanLocation) entity;
            return new FanLocationDTO(fanLocation.getPrimaryKey().getFan().getId(), fanLocation.getPrimaryKey().getLocation().getId(), fanLocation.getVotedDate());

//            associations to be
        } else if (entity instanceof Location) {
            Location location = (Location) entity;
            return new LocationDTO(location.getId(), location.getName(), location.getLatitude(), location.getLongitude());
        } else if (entity instanceof Artist) {
            Artist artist = (Artist) entity;
            ArtistDTO artistDTO = new ArtistDTO(artist.getName(), artist.getId(), artist.getImage());
            if (artist.getStatus() != null)
                artistDTO.setstate(EventFacilitator_Status.valueOf(artist.getStatus()));
            return artistDTO;
//            check the details correctly and rewrite the conversion code if necessary
        } else if (entity instanceof EventArtist) {
            System.out.println("conversion event artist");
            EventArtist eventArtist = (EventArtist) entity;
            Artist artist = eventArtist.getPrimaryKey().getArtist();
            ArtistDTO artistDTO = new ArtistDTO();
            artistDTO.setName(artist.getName());
            artistDTO.setId(artist.getId());
            artistDTO.setstate(EventFacilitator_Status.valueOf(artist.getStatus()));
            artistDTO.setArtistImage(artist.getImage());
            artistDTO.setImageFile(artist.getImage());

            return artistDTO;

        } else if (entity instanceof EventLocation) {
            EventLocation eventLocation = (EventLocation) entity;
            Location location = eventLocation.getPrimaryKey().getLocation();
            LocationDTO locationDTO = new LocationDTO(location.getId(), location.getName(), location.getLatitude(), location.getLongitude());
            System.out.println("event location status: " + eventLocation.getStatus());
            locationDTO.setStatus(EventFacilitator_Status.valueOf(eventLocation.getStatus()));
            return locationDTO;
        } else if (entity instanceof LocationVoteCount) {
            LocationVoteCount voteCount = (LocationVoteCount) entity;
            return new VoteCountDTO(voteCount.getLocationName(), voteCount.getVoteCount());
        } else if (entity instanceof ArtistVoteCount) {
            ArtistVoteCount voteCount = (ArtistVoteCount) entity;
            return new VoteCountDTO(voteCount.getArtistName(), voteCount.getVoteCount());
        } else if (entity instanceof TicketPurchaseCount) {
            TicketPurchaseCount voteCount = (TicketPurchaseCount) entity;
            return new VoteCountDTO(voteCount.getTicketPackageName(), voteCount.getPurchaseCount());
        }


        return null;
    }

    public List convertToDTOList(List entities) {
        System.out.println("inside convert to dto list");
        if (entities.get(0) instanceof Login) {
            List<LoginDTO> ll = new ArrayList<>();
            for (Object e : entities) {
                LoginDTO f = (LoginDTO) convertToDTO((SuperEntity) e);
                ll.add(f);
            }
            return ll;
        } else if (entities.get(0) instanceof Artist) {
            List<ArtistDTO> ll = new ArrayList<>();
            for (Object e : entities) {
                ArtistDTO f = (ArtistDTO) convertToDTO((SuperEntity) e);
                ll.add(f);
            }
            return ll;
        } else if (entities.get(0) instanceof Location) {
            List<LocationDTO> ll = new ArrayList<>();
            for (Object e : entities) {
                LocationDTO f = (LocationDTO) convertToDTO((SuperEntity) e);
                ll.add(f);
            }
            return ll;
        } else if (entities.get(0) instanceof Event) {
            List<EventDTO> eventDTOS = new ArrayList<>();
            for (Object e : entities) {
                EventDTO eventDTO = (EventDTO) convertToDTO((SuperEntity) e);
                eventDTOS.add(eventDTO);
            }
            return eventDTOS;
        } else if (entities.get(0) instanceof TicketPackage) {
            List<TicketPackageDTO> ticketPackageDTOS = new ArrayList<>();
            for (Object e : entities) {
                TicketPackageDTO ticketPackageDTO = (TicketPackageDTO) convertToDTO((SuperEntity) e);
                ticketPackageDTOS.add(ticketPackageDTO);
            }
            return ticketPackageDTOS;
        } else if (entities.get(0) instanceof EventLocation) {
            List<LocationDTO> ll = new ArrayList<>();
            for (Object e : entities) {
                LocationDTO f = (LocationDTO) convertToDTO((EventLocation) e);
                ll.add(f);
            }
            return ll;
        } else if (entities.get(0) instanceof EventArtist) {
            System.out.println("conversion event artist list");
            List<ArtistDTO> ll = new ArrayList<>();
            for (Object e : entities) {
                ArtistDTO f = (ArtistDTO) convertToDTO((EventArtist) e);
                ll.add(f);
            }
            return ll;
        } else if (entities.get(0) instanceof LocationVoteCount) {
            System.out.println("conversion location votes");
            List<VoteCountDTO> ll = new ArrayList<>();
            for (Object e : entities) {
                VoteCountDTO f = (VoteCountDTO) convertToDTO((LocationVoteCount) e);
                ll.add(f);
            }
            return ll;
        } else if (entities.get(0) instanceof ArtistVoteCount) {
            System.out.println("conversion artist votes");
            List<VoteCountDTO> ll = new ArrayList<>();
            for (Object e : entities) {
                VoteCountDTO f = (VoteCountDTO) convertToDTO((ArtistVoteCount) e);
                ll.add(f);
            }
            return ll;
        } else if (entities.get(0) instanceof TicketPurchaseCount) {
            System.out.println("conversion ticket purchases list by event");
            List<VoteCountDTO> ll = new ArrayList<>();
            for (Object e : entities) {
                VoteCountDTO f = (VoteCountDTO) convertToDTO((TicketPurchaseCount) e);
                ll.add(f);
            }
            return ll;
        }
        return null;

    }
}
