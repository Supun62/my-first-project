package com.base245.apeevent.dto;

/**
 * @author Dilini Peiris on 6/16/2019
 */
public class CartItemDTO {
    TicketPackageDTO ticketPackageDTO;
    int qty;

    public CartItemDTO(TicketPackageDTO ticketPackageDTO, int qty) {
        this.ticketPackageDTO = ticketPackageDTO;
        this.qty = qty;
    }

    public CartItemDTO() {
    }

    public TicketPackageDTO getTicketPackageDTO() {
        return ticketPackageDTO;
    }

    public void setTicketPackageDTO(TicketPackageDTO ticketPackageDTO) {
        this.ticketPackageDTO = ticketPackageDTO;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}
