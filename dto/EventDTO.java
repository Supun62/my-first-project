package com.base245.apeevent.dto;

import com.base245.apeevent.service.util.enum_types.Event_Status;

import java.util.ArrayList;
import java.util.List;

/**
 *  @author Supun Rangana as 1/23/2019
 */
public class EventDTO extends SuperDTO{

    private int id;
    private String name;
    private Event_Status status;
    private String preOrderClosingDate;
    private String createdDateTime;
    private double targetBudget;
    private List<TicketPackageDTO> ticketPackageDTOS;
    private List<ArtistDTO> artistDTOS;
    private List<LocationDTO> locationDTOS;
    private String organizerName;
    private String image;
    private int noOfVotes;
    private String imageFile;
    private String eventDueDate;
    private String publishedDateTime;

    public EventDTO(int id, String name, Event_Status status, String preOrderClosingDate, String createdDateTime, double targetBudget, ArrayList<ArtistDTO> artistDTOS, ArrayList<LocationDTO> locationDTOS, String organizerName, String image, String imageFile, int noOfVotes, String eventDueDate, String publishedDateTime) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.preOrderClosingDate = preOrderClosingDate;
        this.createdDateTime = createdDateTime;
        this.targetBudget = targetBudget;
        this.artistDTOS = artistDTOS;
        this.locationDTOS = locationDTOS;
        this.organizerName = organizerName;
        this.image = image;
        this.imageFile = imageFile;
        this.noOfVotes = noOfVotes;
        this.eventDueDate = eventDueDate;
        this.publishedDateTime = publishedDateTime;
    }


    public EventDTO(int id, String name, Event_Status status, String preOrderClosingDate, String createdDateTime, double targetBudget, List<TicketPackageDTO> ticketPackageDTOS,int noOfVotes) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.preOrderClosingDate = preOrderClosingDate;
        this.createdDateTime = createdDateTime;
        this.targetBudget = targetBudget;
        this.ticketPackageDTOS = ticketPackageDTOS;
        this.noOfVotes = noOfVotes;
    }

    public EventDTO(int id, String name, Event_Status status, String preOrderClosingDate, String createdDateTime, double targetBudget, int noOfVotes, String eventDueDate, String publishedDateTime) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.preOrderClosingDate = preOrderClosingDate;
        this.createdDateTime = createdDateTime;
        this.targetBudget = targetBudget;
        this.noOfVotes = noOfVotes;
        this.eventDueDate = eventDueDate;
        this.publishedDateTime = publishedDateTime;
    }

    public EventDTO(int id, String name, Event_Status status, String preOrderClosingDate, String createdDateTime, double targetBudget, String organizerName, int noOfVotes, String eventDueDate, String publishedDateTime) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.preOrderClosingDate = preOrderClosingDate;
        this.createdDateTime = createdDateTime;
        this.targetBudget = targetBudget;
        this.organizerName = organizerName;
        this.noOfVotes = noOfVotes;
        this.eventDueDate = eventDueDate;
        this.publishedDateTime = publishedDateTime;
    }


    public EventDTO(int id, String name, Event_Status status, String preOrderClosingDate, String createdDateTime, double targetBudget, String organizerName, String image, int noOfVotes, String eventDueDate, String publishedDateTime) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.preOrderClosingDate = preOrderClosingDate;
        this.createdDateTime = createdDateTime;
        this.targetBudget = targetBudget;
        this.organizerName = organizerName;
        this.image = image;
        this.noOfVotes = noOfVotes;
        this.eventDueDate = eventDueDate;
        this.publishedDateTime = publishedDateTime;
    }

    public EventDTO(){

    }


    public int getNoOfVotes() {
        return noOfVotes;
    }

    public void setNoOfVotes(int noOfVotes) {
        this.noOfVotes = noOfVotes;
    }

    public String getImageFile() {
        return imageFile;
    }

    public void setImageFile(String imageFile) {
        this.imageFile = imageFile;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Event_Status getStatus() {
        return status;
    }

    public void setStatus(Event_Status status) {
        this.status = status;
    }

    public double getTargetBudget() {
        return targetBudget;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPreOrderClosingDate() {
        return preOrderClosingDate;
    }

    public void setPreOrderClosingDate(String preOrderClosingDate) {
        this.preOrderClosingDate = preOrderClosingDate;
    }

    public void setTargetBudget(double targetBudget) {
        this.targetBudget = targetBudget;
    }

    public List<TicketPackageDTO> getTicketPackageDTOS() {
        return ticketPackageDTOS;
    }

    public void setTicketPackageDTOS(List<TicketPackageDTO> ticketPackageDTOS) {
        this.ticketPackageDTOS = ticketPackageDTOS;
    }

    public String getOrganizerName() {
        return organizerName;
    }

    public void setOrganizerName(String organizerName) {
        this.organizerName = organizerName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<ArtistDTO> getArtistDTOS() {
        return artistDTOS;
    }

    public void setArtistDTOS(List<ArtistDTO> artistDTOS) {
        this.artistDTOS = artistDTOS;
    }

    public List<LocationDTO> getLocationDTOS() {
        return locationDTOS;
    }

    public void setLocationDTOS(List<LocationDTO> locationDTOS) {
        this.locationDTOS = locationDTOS;
    }

    public String getEventDueDate() {
        return eventDueDate;
    }

    public void setEventDueDate(String eventDueDate) {
        this.eventDueDate = eventDueDate;
    }

    public String getPublishedDateTime() {
        return publishedDateTime;
    }

    public void setPublishedDateTime(String publishedDateTime) {
        this.publishedDateTime = publishedDateTime;
    }
}
