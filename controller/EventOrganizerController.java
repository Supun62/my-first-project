package com.base245.apeevent.controller;

import com.base245.apeevent.dto.*;
import com.base245.apeevent.service.custom.*;
import com.base245.apeevent.service.util.convertor.EntityConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Dilini Peiris
 */

@RequestMapping("api/v1/eventOrganizer")
@CrossOrigin(origins = "*")
@RestController
public class EventOrganizerController {

    @Autowired
    EventOrganizerService eventOrganizerService;

    @Autowired
    EntityConvertor entityConvertor;

    @Autowired
    EventService eventService;

    @Autowired
    BankAccountService bankAccountService;

    @Autowired
    ArtistService artistService;

    @Autowired
    VenueService venueService;

    @Autowired
    TicketService ticketService;

    @PostMapping("/event")
    public boolean createEvent(@RequestBody EventDTO eventDTO) {
        System.out.println(" =========== EventDTO" + eventDTO);
        System.out.println(" =========== " + eventDTO.getName() + eventDTO.getPreOrderClosingDate() + eventDTO.getTicketPackageDTOS());
        return eventService.save(eventDTO);
    }

    @PostMapping("/bank")
    public boolean addBankDetails(@RequestBody BankAccountDTO bankAccountDTO) {
        return bankAccountService.save(bankAccountDTO);
    }

    @PostMapping("/setBankAccountForeignKey")
    public void setBankAccountForeignKey(@RequestParam("id") int id, @RequestParam("accountNo") String accountNo) {
        System.out.println("  ================ Mapping");
        eventOrganizerService.setBankAccountForeignKey(id, accountNo);
    }

    @PostMapping("/venue")
    public boolean addVenue(@RequestBody LocationDTO locationDTO) {
        return venueService.save(locationDTO);
    }

    @GetMapping("/artists-view")
    public List<ArtistDTO> getAllArtists() {
        return artistService.findAll();
    }

    @GetMapping("/venues-view")
    public List<LocationDTO> getAllVenues() {
        return venueService.findAll();
    }

    @GetMapping("/event/purchases")
    public List<VoteCountDTO> getPurchasesByEvent(@RequestParam("eventID") int eventID) {
        System.out.println("inside get ticket purchases method");
        List<VoteCountDTO> ticketPurchases = ticketService.getTicketPurchases(eventID);
        System.out.println("ticket purchases to be sent: " + ticketPurchases);
        return ticketPurchases;
    }

    @GetMapping("/getEventOrganizerByName")
    public UserDTO getEventOrganizerByName(@RequestParam("name") String name) {
        return eventOrganizerService.getEventOrganizerByName(name);
    }
}
