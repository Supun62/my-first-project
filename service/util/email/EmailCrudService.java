package com.base245.apeevent.service.util.email;

import com.base245.apeevent.dto.LoginDTO;

import java.util.List;

/**
 * @author Dilini Peiris on 5/21/2019
 */
public interface EmailCrudService {

    boolean saveVerificationToken(LoginDTO loginDTO, String token);

    boolean saveGeneratedPassword(String username, String password);

    List<String> getEvents(String username);

}
